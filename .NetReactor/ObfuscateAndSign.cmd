@echo off
:: change path to script directory
cd /d %~dp0
setlocal enableextensions enabledelayedexpansion

SET "dotNetReactor=c:\Program Files (x86)\Eziriz\.NET Reactor\dotNET_Reactor.Console.exe"
SET "file=%1"
SET "solutionDir=%2"
SET "project=BatTerminal.nrproj"

::run .NET Reactor
"%dotNetReactor%" -file "%file%" -project "%project%"


:exit
@echo on