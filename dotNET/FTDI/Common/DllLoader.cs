﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Ftdi.Common
{
    /// <summary>
    /// Add any dll to Project Properties -> Resources
    /// Mark it as "Embedded resource" in Solution Explorer
    /// Call DllLoader.Load(DllName)
    /// </summary>
    public static class DllLoader
    {
        /// <summary>
        /// Loads and dll specified by name from Embedded resources
        /// </summary>
        /// <param name="dll">name of dll to load</param>
        public static void Load(string dll)
        {
            // Load dll from resources
            var asm = Assembly.GetCallingAssembly();
            var name = asm.GetManifestResourceNames().FirstOrDefault(n => n.EndsWith(dll));
            Stream stream;
            if (!string.IsNullOrEmpty(name) && (stream = asm.GetManifestResourceStream(name)) != null)
            {
                var dllData = new byte[stream.Length];
                stream.Read(dllData, 0, (int)stream.Length);

                // Write to temporary files
                var dllPath = Path.GetTempPath() + dll;
                File.Delete(dllPath);
                var fs = new FileStream(dllPath, FileMode.CreateNew);
                var w = new BinaryWriter(fs);
                w.Write(dllData);
                w.Close();
                fs.Close();
            }

            // Set dll path
            SetDllDirectory(Path.GetTempPath());
        }

        #region Private

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern void SetDllDirectory(string lpPathName);

        #endregion

    }
}
