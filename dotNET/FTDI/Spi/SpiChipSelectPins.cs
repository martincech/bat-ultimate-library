﻿using System.Runtime.InteropServices;

namespace Ftdi.Spi
{
   /// <summary>
   /// Specifiy initial state of each pin at beginning of transfering
   /// 
   /// Caution. The original FTCSPI.h had bug on the name of each member function.
   /// The name bADBUS4GPIOL1PinState, is wrong name. It should be bADBUS4GPIOL0PinState.
   /// (Tha GPIOLxPinState where x must be zero origin, instead of one origin ).
   /// Below definition corrected this mistake. 
   /// </summary>
   [StructLayout(LayoutKind.Sequential)]
   public struct SpiChipSelectPins
   {
      ///trueH, falseL
      public LogicLevel bADBUS3ChipSelectPinState;

      ///trueH, falseL
      public LogicLevel bADBUS4GPIOL0PinState;

      ///trueH, falseL
      public LogicLevel bADBUS5GPIOL1PinState;

      ///trueH, falseL
      public LogicLevel bADBUS6GPIOL2PinState;

      ///trueH, falseL
      public LogicLevel bADBUS7GPIOL3PinState;
   };
}