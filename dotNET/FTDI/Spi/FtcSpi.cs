﻿/**
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/

* \file spidll.cs
* \Author Suikan
* \brief Basic interface for FTDI's DLL.
* 
* Contains basic interface for FTDI's DLL. This file is generated from the FTCSPI.h 
* file which is delivered at http://www.ftdichip.com/Projects/MPSSE/FTCSPI.htm. 
* To use this interface file, you should also plase FTCSPI.DLL into /windows/system32 
* directory. that DLL is distributed together with FTCSPI.h. 
* 
* Note that original FTCSPI.h has several typo about the pin name. After few minutes
* concern, I decided to correct these bugs. So, some name is not compatible with original
* name. 
* 
* Also, functionames of each DLL function is changed. I just thought FTDI.SPI.Open() is 
* much better than FTDI.SPI.SPI_Open(). 
* 
*/

using System.Runtime.InteropServices;
using System.Text;
using Ftdi.Common;

namespace Ftdi.Spi
{
   /// <summary> 
   /// \brief interface class for FTCSPI.DLL.
   /// 
   /// This class works as wrapper of FTCSPI.DLL for C#. All interface is delivered from
   /// FTCSPI.h but re-written in C# manner. Note that SPI.ChipSelectPins, SPI.InputOutputPins,
   /// and SPI.LowHighPins structs members are rename to fix the typo about pin number
   /// origin which is found in FTCSPI.h.
   /// </summary>
   public static class FtcSpi
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      static FtcSpi()
      {
         DllLoader.Load("FTCSPI.dll");
      }

      /// <summary>
      /// \brief Count up FT2232 devices.
      /// \param lpdwNumDevice  Returns how many devices are in the system
      /// 
      /// This function provides the information how many FT2232 devices are in the system.
      /// After you know the number of device, you will scan all devices's name and its unique
      /// location ID by SPI.GetDeviceNameLocID() function.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetNumDevices")]
      public static extern SpiStatus
         GetNumDevices(
         out int lpdwNumDevice // return how many devices are in system
         );


      /// <summary>
      /// \brief Detect name and location ID of specified devices. 
      /// \param dwDeviceNameIndex  0 origin device number. Must be smaller than value which is 
      /// given by SPI.GetNumDevices() function.
      /// \param lpDeviceNameBuffer  Returns name of specified device.
      /// \param dwBufferSize  Specify the length of lpDevicesNameBuffer.
      /// \param lpdwLocationID  Returns unique location ID which is used by SPI.OpenEx() function.
      /// 
      /// This functions is useful to enumulate all FT2232 devices in your system. the returned value
      /// for lpdwLocationID is used to open the device. Before calling this function, you should check
      /// how many devices are in system, by calling SPI.GetNumDevices() function.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetDeviceNameLocID")]
      public static extern SpiStatus
         GetDeviceNameLocID(
         int dwDeviceNameIndex, // specify device index number. 0 origin.
         StringBuilder lpDeviceNameBuffer, // name of speicified device
         int dwBufferSize, // length of name buffer.
         out int lpdwLocationId); // location ID of specified devices.

      /// <summary>
      /// \brief Open the device by name and location id.
      /// \param lpDeviceName  device Name given by GetDeviceNameLocID
      /// \param dwLocationID  location ID given by GetDeviceNameLocID
      /// \param pftHandle  handle to opened device
      /// 
      /// open a device specified by name and location ID. The name and Location ID must be the
      /// one which is obtained by SPI.GetDeviceNameLocID() funciton.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_OpenEx")]
      public static extern SpiStatus
         OpenEx(
         string lpDeviceName, // device Name given by GetDeviceNameLocID
         int dwLocationId, // location ID given by GetDeviceNameLocID
         out int pftHandle // handle to opened device
         );

      /// <summary>
      /// \brief Open a device when that is only one in the system.
      /// \param pftHandle  handle to opened device
      /// 
      /// If you have only one FT2232 device in your system, you can open it by this function.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_Open")]
      public static extern SpiStatus
         Open(
         out int pftHandle // handle to opened device
         );

      /// <summary>
      /// \brief Close the opened device
      /// \param pftHandle  specify device
      /// 
      /// Close the specified device.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_Close")]
      public static extern SpiStatus
         Close(
         int pftHandle // specify device
         );

      /// <summary>
      /// \brief Initialize SPI clock by given devisor
      /// \param pftHandle  specify device
      /// \param dwClockDivisor  specify devisor
      /// 
      /// Set the clock frequency of SPICLK. The clock frequency[MHz] is 6/(1+dwClockDivisor).
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_InitDevice")]
      public static extern SpiStatus
         InitDevice(
         int pftHandle, // specify device
         int dwClockDivisor // specify devisor
         );

      /// <summary>
      /// \brief calc actual clock from devisor
      /// \param dwClockDivisor  specify devisor
      /// \param lpdwClockFrequencyHz  obtain freq from devisor
      /// 
      /// Calcurate SPI clock frequency from devisor.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetClock")]
      public static extern SpiStatus
         GetClock(
         int dwClockDivisor, // specify devisor
         ref int lpdwClockFrequencyHz // obtain freq from devisor
         );

      /// <summary>
      /// \brief set SPI clock by given devisor
      /// \param pftHandle  specify device
      /// \param dwClockDivisor  specify devisor
      /// \param lpdwClockFrequencyHz  obtain freq from devisor
      /// 
      /// Set clock divisor and obtain result frequency.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_SetClock")]
      public static extern SpiStatus
         SetClock(
         int pftHandle, // specify device
         int dwClockDivisor, // specify devisor
         ref int lpdwClockFrequencyHz // obtain freq from devisor
         );

      /// <summary>
      /// \brief set SPI Loopback mode
      /// \param pftHandle  specify device
      /// \param bLoopbackState  true to set loopback mode
      /// 
      /// Is this internal test of FTCSPI.DLL?
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_SetLoopback")]
      public static extern SpiStatus
         SetLoopback(
         int pftHandle, // specify device
         bool bLoopbackState // true to set loopback mode
         );

      /// <summary>
      /// \brief Set GPIO pin
      /// \param ftHandle Specify device.
      /// \param pChipSelectsDisableStates
      /// \param pHighInputOutputPinsData
      /// 
      /// This function sets the GPIO pin state based on the given parameter.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_SetGPIOs")]
      public static extern SpiStatus
         SetGPIOs(
         int ftHandle,
         ref SpiChipSelectPins pChipSelectsDisableStates,
         ref InputOutputPins pHighInputOutputPinsData
         );


      /// <summary>
      /// \brief Get GPIO pin status.
      /// \param ftHandle  specify device
      /// \param pHighPinsInputData  return GPIO status.
      /// 
      /// Return the GPIO pin status into parameter.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetGPIOs")]
      public static extern SpiStatus
         GetGPIOs(
         int ftHandle,
         out InputPins pHighPinsInputData
         );


      /// <summary>
      /// \brief Write data stream to SPI
      /// \param ftHandle  handle given by Open or OpenEx
      /// \param pWriteStartCondition  Pins state before transfer to be set.
      /// \param bClockOutDataBitsMSBFirst  Data is sent by trueMSB, falseLSB first.
      /// \param bClockOutDataBitsPosEdge  Data transit by trueRising falseFalling edge. 
      /// This setting should be same with initCondition.bClockPinState.
      /// \param dwNumControlBitsToWrite  The numbers of data to be written [bits]
      /// \param pWriteControlBuffer  buffer which contains command to be sent
      /// \param dwNumControlBytesToWrite  Length of Control Buffer? [BYTE]
      /// \param bWriteDataBits  trueSend, falseDon't send data bits
      /// \param dwNumDataBitsToWrite  The numbers of data to be read [bits]
      /// \param pWriteDataBuffer  buffer which contains data to be sent
      /// \param dwNumDataBytesToWrite  Length of data buffer? [BYTE
      /// \param pWaitDataWriteComplete
      /// \param pHighPinsWriteActiveStates
      /// 
      /// A write command.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_Write")]
      public static extern SpiStatus
         Write(
         int ftHandle,
         ref SpiInitCondition pWriteStartCondition, // Pins state before transfer to be set.
         bool bClockOutDataBitsMsbFirst, // Data is sent by trueMSB, falseLSB first.
         bool bClockOutDataBitsPosEdge, // Data transit by trueRising falseFalling edge.
         int dwNumControlBitsToWrite, // The numbers of data to be written [bits]
         byte[] pWriteControlBuffer,
         int dwNumControlBytesToWrite, // Length of Control Buffer? [BYTE]
         bool bWriteDataBits, // trueSend, falseDon't send data bits
         int dwNumDataBitsToWrite, // The numbers of data to be read [bits]
         byte[] pWriteDataBuffer,
         int dwNumDataBytesToWrite, // Length of data buffer? [BYTE]
         ref SpiWaitDataWrite pWaitDataWriteComplete,
         ref InputOutputPins pHighPinsWriteActiveStates
         );


      /// <summary>
      /// \brief Read data stream form SPI
      /// \param ftHandle  handle given by Open or OpenEx
      /// \param pReadStartCondition  Pins state before transfer.
      /// \param bClockOutControlBitsMSBFirst  Data is sent by trueMSB, falseLSB first.
      /// \param bClockOutControlBitsPosEdge  Data transit by trueRising falseFalling edge. 
      /// This setting should be same with initCondition.bClockPinState. 
      /// \param dwNumControlBitsToWrite  The numbers of data to be written [bits]
      /// \param pWriteControlBuffer  command buffer to be sent
      /// \param dwNumControlBytesToWrite  Length of Control Buffer [BYTE]
      /// \param bClockInDataBitsMSBFirst  Data is read by trueMSB, falseLSB first.
      /// \param bClockInDataBitsPosEdge  Data transit by trueRising falseFalling edge.
      /// \param dwNumDataBitsToRead  The numbers of data to be read [bits]
      /// \param pReadDataBuffer  Buffer to receive the data
      /// \param lpdwNumDataBytesReturned  The numbers of data transfered [BYTE]
      /// \param pHighPinsReadActiveStates
      /// 
      /// A read command
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_Read")]
      public static extern SpiStatus
         Read(
         int ftHandle,
         ref SpiInitCondition pReadStartCondition, // Pins state before transfer to be set.
         bool bClockOutControlBitsMsbFirst, // Data is sent by trueMSB, falseLSB first.
         bool bClockOutControlBitsPosEdge, // Data transit by trueRising falseFalling edge.
         int dwNumControlBitsToWrite, // The numbers of data to be written [bits]
         byte[] pWriteControlBuffer,
         int dwNumControlBytesToWrite, // Length of Control Buffer [BYTE]
         bool bClockInDataBitsMsbFirst, // Data is read by trueMSB, falseLSB first.
         bool bClockInDataBitsPosEdge, // Data transit by trueRising falseFalling edge.
         int dwNumDataBitsToRead, // The numbers of data to be read [bits]
         byte[] pReadDataBuffer,
         ref int lpdwNumDataBytesReturned, // The numbers of data transfered [BYTE]
         ref InputOutputPins pHighPinsReadActiveStates
         );


      /// <summary>
      /// \brief Clear command sequence
      /// \param ftHandle  handle given by Open or OpenEx
      /// 
      /// Clear the commands assembled by AddDeviceWriteCmd and AddDeviceReadCmd
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_ClearDeviceCmdSequence")]
      public static extern SpiStatus
         ClearDeviceCmdSequence(
         int ftHandle
         );


      /// <summary>
      /// \brief Add write command to command sequence
      /// \param ftHandle  handle given by Open or OpenEx
      /// \param pWriteStartCondition  Pins state before transfer to be set.
      /// \param bClockOutDataBitsMSBFirst  Data is sent by trueMSB, falseLSB first.
      /// \param bClockOutDataBitsPosEdge  Data transit by trueRising falseFalling edge. 
      /// This setting should be same with initCondition.bClockPinState.
      /// \param dwNumControlBitsToWrite  The numbers of data to be written [bits]
      /// \param pWriteControlBuffer  buffer which contains command to be sent
      /// \param dwNumControlBytesToWrite  Length of Control Buffer? [BYTE]
      /// \param bWriteDataBits  trueSend, falseDon't send data bits
      /// \param dwNumDataBitsToWrite The numbers of data to be read [bits]
      /// \param pWriteDataBuffer  buffer which contains data to be sent
      /// \param dwNumDataBytesToWrite  Length of data buffer? [BYTE]
      /// \param pHighPinsWriteActiveStates
      ///
      /// Same with Write function, but it is executed when ExcecuteDeviceMcdSequence
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_AddDeviceWriteCmd")]
      public static extern SpiStatus
         AddDeviceWriteCmd(
         int ftHandle,
         ref SpiInitCondition pWriteStartCondition,
         bool bClockOutDataBitsMsbFirst,
         bool bClockOutDataBitsPosEdge,
         int dwNumControlBitsToWrite,
         byte[] pWriteControlBuffer,
         int dwNumControlBytesToWrite,
         bool bWriteDataBits,
         int dwNumDataBitsToWrite,
         byte[] pWriteDataBuffer,
         int dwNumDataBytesToWrite,
         ref InputOutputPins pHighPinsWriteActiveStates
         );


      /// <summary>
      /// \brief Add read command to command sequence
      /// \param ftHandle  handle given by Open or OpenEx
      /// \param pReadStartCondition  Pins state before transfer.
      /// \param bClockOutControlBitsMSBFirst  Data is sent by trueMSB, falseLSB first.
      /// \param bClockOutControlBitsPosEdge  Data transit by trueRising falseFalling edge. 
      /// This setting should be same with initCondition.bClockPinState. 
      /// \param dwNumControlBitsToWrite  The numbers of data to be written [bits]
      /// \param pWriteControlBuffer  command buffer to be sent
      /// \param dwNumControlBytesToWrite  Length of Control Buffer [BYTE]
      /// \param bClockInDataBitsMSBFirst  Data is read by trueMSB, falseLSB first.
      /// \param bClockInDataBitsPosEdge  Data transit by trueRising falseFalling edge.
      /// \param dwNumDataBitsToRead  The numbers of data to be read [bits]
      /// \param pHighPinsReadActiveStates
      /// 
      /// Same with Read function, but it is executed when ExcecuteDeviceMcdSequence.
      /// The received data will be passed to the param of ExecuteDeviceCmdSequence.
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_AddDeviceReadCmd")]
      public static extern SpiStatus
         AddDeviceReadCmd(
         int ftHandle,
         ref SpiInitCondition pReadStartCondition,
         bool bClockOutControlBitsMsbFirst,
         bool bClockOutControlBitsPosEdge,
         int dwNumControlBitsToWrite,
         byte[] pWriteControlBuffer,
         int dwNumControlBytesToWrite,
         bool bClockInDataBitsMsbFirst,
         bool bClockInDataBitsPosEdge,
         int dwNumDataBitsToRead,
         byte[] pHighPinsReadActiveStates
         );


      /// <summary>
      /// \brief Start to execute the command sequence by AddDeviceReadCmd and AddDeviceWriteCmd
      /// \param ftHandle  handle given by Open or OpenEx
      /// \param pReadCmdSequenceDataBuffer  Buffer to receive the data
      /// \param lpdwNumBytesReturned  The numbers of data transfered [BYTE]
      ///
      /// This command starts to execute the command sequence in a command buffer.  
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_ExecuteDeviceCmdSequence")]
      public static extern SpiStatus
         ExecuteDeviceCmdSequence(
         int ftHandle,
         byte[] pReadCmdSequenceDataBuffer,
         ref int lpdwNumBytesReturned
         );


      /// <summary>
      /// \brief convert error number to massage string
      /// \param lpLanguage  Language. Use "en"
      /// \param StatusCode  given error code
      /// \param lpErrorMessageBuffer  Obtain error message 
      /// \param dwBufferSize  length of message buffer
      /// 
      /// generate the message string from given Status Code
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetErrorCodeString")]
      public static extern SpiStatus
         GetErrorCodeString(
         string lpLanguage, // Language. Use "en"
         SpiStatus statusCode, // given error code
         StringBuilder lpErrorMessageBuffer, // Obtain error message 
         int dwBufferSize // length of message buffer
         );

      /// <summary>
      /// \brief get DLL Version
      /// \param lpDllVersionBuffer  Obtain version string
      /// \param dwBufferSize  length of string
      /// </summary>
      [DllImport("FTCSPI.dll", EntryPoint = "SPI_GetDllVersion")]
      public static extern SpiStatus
         GetDllVersion(
         StringBuilder lpDllVersionBuffer, // Obtain version string 
         int dwBufferSize // length of string
         );
   }
} // namespace FT2232
