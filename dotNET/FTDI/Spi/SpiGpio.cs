﻿namespace Ftdi.Spi
{
   /// <summary>
   /// pins for GPIO
   /// 
   /// These identifier represent the pins which can be used as GPIO signal.
   /// </summary>
   public enum SpiGpio
   {
// ReSharper disable InconsistentNaming
      ADBUS2DataIn = 0,
      ACBUS0GPIOH0,
      ACBUS1GPIOH1,
      ACBUS2GPIOH2,
      ACBUS3GPIOH3
   };
}