﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using MvvmFramework.Observable;
using NUnit.Framework;

namespace MvvmFramework.Tests.UnitTests
{
   [TestFixture]
   public class RecursiveValidatableObservableObjectTests
   {
      private StubRecursiveValidatableObservableObject obj;
      private bool raised;

      [SetUp]
      public void TestInit()
      {
         obj = new StubRecursiveValidatableObservableObject();
         raised = false;
      }

      [Test]
      public void InnerObservablePropertyChangedEventHandlerIsRaised()
      {
         obj.ObservableProperty = new StubObservableObject();

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.AreEqual("ObservableProperty", e.PropertyName);
            raised = true;
         };

         obj.ObservableProperty.ChangedProperty = "some value";
         Assert.IsTrue(raised, "PropertyChanged some value never raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = "some value";
         Assert.IsFalse(raised, "PropertyChanged same some value raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = null;
         Assert.IsTrue(raised, "PropertyChanged null value never raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = "";
         Assert.IsTrue(raised, "PropertyChanged empty value never raised");
         raised = false;
      }

      [Test]
      public void InnerValidatableObservablePropertyDataErrorIsRaised()
      {
         obj.ValidatableObservableProperty = new StubValidatableObservableObject();

         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.AreEqual("ValidatableObservableProperty", e.PropertyName);
            raised = true;
         };

         // check if errors raised
         obj.ValidatableObservableProperty.MaxLengthProperty = "lasjfůlkasjflkajflkjaslkfd";
         Assert.IsTrue(raised, "ErrorsChanged never invoked");
         Assert.IsTrue(obj.HasErrors, "Parent has no errors");
         var errors = obj.GetErrors("ValidatableObservableProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.IsTrue(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.IsTrue(validationResults.Any(p => p.ErrorMessage == "Too lenghty error"));


         // check if errors cleared
         raised = false;
         obj.ValidatableObservableProperty.MaxLengthProperty = "123";
         Assert.IsTrue(raised, "ErrorsChanged after valid property never invoked");
         Assert.IsFalse(obj.HasErrors, "Parent has errors");
         errors = obj.GetErrors("ValidatableObservableProperty");
         validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.IsTrue(!validationResults.Any(), "More errors returned where none should be");
      }
   }

   internal class StubRecursiveValidatableObservableObject : RecursiveValidatableObservableObject
   {
      private StubObservableObject observableProperty;
      private StubValidatableObservableObject validatableObservableProperty;

      public StubObservableObject ObservableProperty
      {
         get { return observableProperty; }
         set { SetProperty(ref observableProperty, value); }
      }

      public StubValidatableObservableObject ValidatableObservableProperty
      {
         get { return validatableObservableProperty; }
         set { SetPropertyAndValidate(ref validatableObservableProperty, value); }
      }
   };
}
