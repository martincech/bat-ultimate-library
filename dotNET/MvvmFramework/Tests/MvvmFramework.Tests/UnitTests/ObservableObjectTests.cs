﻿using MvvmFramework.Observable;
using NUnit.Framework;

namespace MvvmFramework.Tests.UnitTests
{
   [TestFixture]
   public class ObservableObjectTests
   {
      [Test]
      public void PropertyChangedEventHandlerIsRaised_WhenCalledByName()
      {
         var obj = new StubObservableObject();
         var raised = false;

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.IsTrue(e.PropertyName == "ChangedProperty");
            raised = true;
         };

         obj.ChangedProperty = "Some Value";

         Assert.IsTrue(raised, "PropertyChanged never invoked");
      }

      [Test]
      public void PropertyChangedEventHandlerIsRaised_WhenCalledByExpression()
      {
         var obj = new StubObservableObject();
         var raised = false;

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.IsTrue(e.PropertyName == "ChangedPropertyExpr");
            raised = true;
         };

         obj.ChangedPropertyExpr = "Some Value";

         Assert.IsTrue(raised, "PropertyChanged never invoked");
      }
   }

   internal class StubObservableObject : ObservableObject
   {
      private string changedProperty;
      private string changedPropertyExpr;

      public string ChangedProperty
      {
         get { return changedProperty; }
         set { SetProperty(ref changedProperty, value); }
      }

      public string ChangedPropertyExpr
      {
         get { return changedPropertyExpr; }
         set
         {
            if (value == changedPropertyExpr) return;
            changedPropertyExpr = value;
            RaisePropertyChanged(() => ChangedPropertyExpr);
         }
      }
   }
}
