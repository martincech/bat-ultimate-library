﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace MvvmFramework.Observable
{
   /// <summary>
   /// Defines a base class for validatable observable object that supports recursive
   /// property/errors changed invocation
   /// </summary>
   public abstract class RecursiveValidatableObservableObject : ValidatableObservableObject
   {
      [NonSerialized]
      private readonly Dictionary<string, List<ValidationResult>> childErrors;

      protected RecursiveValidatableObservableObject()
      {
         childErrors = new Dictionary<string, List<ValidationResult>>();
      }

      /// <summary>
      /// Add property changed event listener for nested property
      /// </summary>
      /// <param name="value">nested property object</param>
      private void AddNestedPropertyListener(object value)
      {
         var collection = value as INotifyCollectionChanged;
         if (collection != null)
         {
            collection.CollectionChanged += NestedCollectionChanged;
            return;
         }

         var prop = value as INotifyPropertyChanged;
         if (prop != null)
         {
            prop.PropertyChanged += NestedPropertyChanged;
         }
         var indei = value as INotifyDataErrorInfo;
         if (indei != null)
         {
            indei.ErrorsChanged += NestedErrorsChanged;
         }
      }

      /// <summary>
      /// Removes property changed event listener for nested property
      /// </summary>
      /// <param name="value">nested property object</param>
      private void RemoveNestedPropertyListener(object value)
      {
         var collection = value as INotifyCollectionChanged;
         if (collection != null)
         {
            collection.CollectionChanged -= NestedCollectionChanged;
         }
         else
         {
            var prop = value as INotifyPropertyChanged;
            if (prop != null)
            {
               prop.PropertyChanged -= NestedPropertyChanged;
            }
         }

         var indei = value as INotifyDataErrorInfo;
         if (indei != null)
         {
            indei.ErrorsChanged -= NestedErrorsChanged;
         }
      }

      /// <summary>
      /// Set the property with the specified value. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="propertyName">The property name. This optional parameter can be skipped
      /// because the compiler is able to create it automatically.</param>
      /// <returns>True if the value has changed, false if the old and new value were equal.</returns>
      protected override bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
      {
         RemoveNestedPropertyListener(field);
         if (!base.SetProperty(ref field, value, propertyName)) return false;
         AddNestedPropertyListener(value);
         return true;
      }

      /// <summary>
      /// Set the property with the specified value and validate the property. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised, the property is validated and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="propertyName">The property name. This optional parameter can be skipped
      /// because the compiler is able to create it automatically.</param>
      /// <returns>True if the value has changed, false if the old and new value were equal.</returns>
      /// <exception cref="ArgumentException">The argument propertyName must not be null or empty.</exception>
      protected override bool SetPropertyAndValidate<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
      {
         RemoveNestedPropertyListener(field);
         if (!base.SetPropertyAndValidate(ref field, value, propertyName)) return false;
         AddNestedPropertyListener(value);
         return true;
      }


      /// <summary>
      /// For each <see cref="INotifyPropertyChanged"/> property 'X' of this object raise PropertyChanged event when 
      /// X's PropertyChanged event is raised.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void NestedPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         //TODO: remove properties from inherited class (ValidatableObservableObject)
         var properties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance /*| BindingFlags.DeclaredOnly*/);

         var property = properties.Where(pi =>
         {
            if (pi.GetIndexParameters().Length > 0) // indexed property
            {
               return false;
            }
            else if (pi.GetValue(this) == sender)
            {
               return true;
            }
            else
            {
               return false;
            }
         }).FirstOrDefault();

         if (property != null)
         {
            RaisePropertyChanged(property.Name);
         }
         else
         {  // property is collection ?
            foreach (var prop in properties)
            {
               var propInfo = GetType().GetProperty(prop.Name, BindingFlags.Public | BindingFlags.Instance);
               Debug.Assert(propInfo != null);
               var typeArg = propInfo.PropertyType.GetGenericArguments().FirstOrDefault();

               var type = sender.GetType();
               if (typeArg == null || !typeArg.FullName.Equals(type.ToString())) continue;
               var collection = (IEnumerable)propInfo.GetGetMethod().Invoke(this, null);
               if (collection.Cast<object>().All(item => item != sender)) continue;
               RaisePropertyChanged(prop.Name);
               return;
            }
         }

      }

      private void NestedCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               foreach (var item in e.NewItems)
               {
                  AddNestedPropertyListener(item);
               }
               break;
            case NotifyCollectionChangedAction.Remove:
               foreach (var item in e.OldItems)
               {
                  RemoveNestedPropertyListener(item);
               }
               break;
            case NotifyCollectionChangedAction.Reset:
               foreach (var item in e.OldItems)
               {
                  RemoveNestedPropertyListener(item);
               }
               break;
         }

         NestedPropertyChanged(sender, null);
      }

      /// <summary>
      /// For each <see cref="INotifyDataErrorInfo"/> property 'X' of this object raise ErrorsChanged event when 
      /// X's ErrorsChanged event is raised.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void NestedErrorsChanged(object sender, DataErrorsChangedEventArgs e)
      {
         Debug.Assert(sender is INotifyDataErrorInfo);
         var prop = GetType().GetProperties().FirstOrDefault(pi => pi.GetValue(this) == sender);
         if (prop == null) return;

         var errors = ((INotifyDataErrorInfo)sender).GetErrors(e.PropertyName).OfType<ValidationResult>();
         var errorsList = errors as IList<ValidationResult> ?? errors.ToList();
         childErrors[prop.Name] = errorsList.ToList();
         ValidateProperty(sender, prop.Name);
      }

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         if (!childErrors.ContainsKey(propertyName)) return base.AdditionalValidationRules(value, propertyName);

         return childErrors[propertyName];
      }
   }
}
