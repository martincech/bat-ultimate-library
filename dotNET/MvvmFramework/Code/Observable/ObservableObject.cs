﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using MvvmFramework.Annotations;

namespace MvvmFramework.Observable
{
   /// <summary>
   /// Defines the base class for a ObservableObject object (implementation of <see cref="INotifyPropertyChanged"/> interface)
   /// </summary>
   [Serializable]
   public abstract class ObservableObject : INotifyPropertyChanged
   {
      protected static string GetCallerName()
      {
         var stackTrace = new StackTrace();
         var frame = stackTrace.GetFrame(2);
         var method = frame.GetMethod();
         return method.Name.Replace("set_", "");
      }
      
      /// <summary>
      /// Occurs when a property value changes.
      /// </summary>
      [field: NonSerialized]
      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Set the property with the specified value. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="propertyName">The property name. This optional parameter can be skipped
      /// because the compiler is able to create it automatically.</param>
      /// <returns>True if the value has changed, false if the old and new value were equal.</returns>
      [NotifyPropertyChangedInvocator]
      protected virtual bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
      {
         if (Equals(field, value))
         {
            return false;
         }

         field = value;
         if (propertyName == null)
         {
            propertyName = GetCallerName();
         }
         Debug.Assert(propertyName != null, "ObservableObject: SetProperty - propertyName is null");
         RaisePropertyChanged(propertyName);
         return true;
      }



      /// <summary>
      /// Set the property with the specified value. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="expression">property expression - () => PropertyName </param>
      /// <exception cref="ArgumentException">The argument expression must not be null.</exception>
      [NotifyPropertyChangedInvocator]
      protected virtual bool SetProperty<T>(ref T field, T value,
         Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null, "expression", "The expression argument must not be null!");
         Debug.Assert(expression.Body is MemberExpression, "expression",
            "The expression argument must be of type MemberExpression!");
         var memberExpression = (MemberExpression) expression.Body;
         return SetProperty(ref field, value, memberExpression.Member.Name);
      }

      /// <summary>
      /// Raises the <see cref="E:PropertyChanged"/> event.
      /// </summary>
      /// <param name="expression">property expression - () => PropertyName </param>
      [NotifyPropertyChangedInvocator]
      protected void RaisePropertyChanged<T>(Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null, "expression", "The expression argument must not be null!");
         Debug.Assert(expression.Body is MemberExpression, "expression", 
            "The expression argument must be of type MemberExpression!");
         var memberExpression = (MemberExpression) expression.Body;
         RaisePropertyChanged(memberExpression.Member.Name);
      }

      /// <summary>
      /// Raises the <see cref="E:PropertyChanged"/> event.
      /// </summary>
      /// <param name="propertyName">The property name of the property that has changed.
      /// This optional parameter can be skipped because the compiler is able to create it automatically.</param>
      [NotifyPropertyChangedInvocator]
      protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
      {
         if (propertyName == null)
         {
            propertyName = GetCallerName();
         }
         RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));
      }   

      /// <summary>
      /// Raises the <see cref="E:PropertyChanged"/> event.
      /// </summary>
      /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
      protected virtual void RaisePropertyChanged(PropertyChangedEventArgs e)
      {
         var handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, e);
         }
      }
   }
}
