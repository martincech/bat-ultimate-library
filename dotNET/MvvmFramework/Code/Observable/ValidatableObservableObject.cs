﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using MvvmFramework.Annotations;

namespace MvvmFramework.Observable
{
   /// <summary>
   /// Defines a base class for a observable object that supports validation.
   /// </summary>
   [Serializable]
   public abstract class ValidatableObservableObject : ObservableObject, INotifyDataErrorInfo, IDataErrorInfo
   {
      private static readonly ValidationResult[] NO_ERRORS = new ValidationResult[0];

      [NonSerialized]
      private readonly Dictionary<string, List<ValidationResult>> errors;
      [NonSerialized]
      private bool hasErrors;

      /// <summary>
      /// Initializes a new instance of the <see cref="ValidatableObservableObject"/> class.
      /// </summary>
      protected ValidatableObservableObject()
      {
         errors = new Dictionary<string, List<ValidationResult>>();
      }


      /// <summary>
      /// Gets a value that indicates whether the entity has validation errs.
      /// </summary>
      public bool HasErrors
      {
         get { return hasErrors; }
         protected set { SetProperty(ref hasErrors, value); }
      }


      /// <summary>
      /// Occurs when the validation errs have changed for a property or for the entire entity.
      /// </summary>
      [field: NonSerialized]
      public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;


      /// <summary>
      /// Gets the validation errs for the entire entity.
      /// </summary>
      /// <returns>The validation errs for the entity.</returns>
      public IEnumerable<ValidationResult> GetErrors()
      {
         return GetErrors(null);
      }

      /// <summary>
      /// Gets the validation errs for a specified property or for the entire entity.
      /// </summary>
      /// <param name="propertyName">The name of the property to retrieve validation errs for; 
      /// or null or String.Empty, to retrieve entity-level errs.</param>
      /// <returns>The validation errs for the property or entity.</returns>
      public IEnumerable<ValidationResult> GetErrors(string propertyName)
      {
         if (string.IsNullOrEmpty(propertyName)) return errors.Values.SelectMany(x => x).Distinct().ToArray();

         List<ValidationResult> result;
         if (errors.TryGetValue(propertyName, out result))
         {
            return result;
         }
         return NO_ERRORS;
      }

      IEnumerable INotifyDataErrorInfo.GetErrors(string propertyName)
      {
         return GetErrors(propertyName);
      }

      /// <summary>
      /// Validates the object and all its properties. The validation results are stored and can be retrieved by the 
      /// GetErrors method. If the validation results are changing then the ErrorsChanged event will be raised.
      /// </summary>
      /// <returns>True if the object is valid, otherwise false.</returns>
      public virtual bool Validate()
      {
         var validationResults = new List<ValidationResult>();
         Validator.TryValidateObject(this, new ValidationContext(this), validationResults, true);
         if (validationResults.Any())
         {
            errors.Clear();
            foreach (var validationResult in validationResults)
            {
               var propertyNames = validationResult.MemberNames.Any() ? validationResult.MemberNames : new[] { "" };
               foreach (var propertyName in propertyNames)
               {
                  if (!errors.ContainsKey(propertyName))
                  {
                     errors.Add(propertyName, new List<ValidationResult> { validationResult });
                  }
                  else
                  {
                     errors[propertyName].Add(validationResult);
                  }
               }
            }
            RaiseErrorsChanged();
            return false;
         }
         if (!errors.Any()) return true;
         errors.Clear();
         RaiseErrorsChanged();
         return true;
      }

      /// <summary>
      /// Set the property with the specified value and validate the property. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised, the property is validated and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="propertyName">The property name. This optional parameter can be skipped
      /// because the compiler is able to create it automatically.</param>
      /// <returns>True if the value has changed, false if the old and new value were equal.</returns>
      /// <exception cref="ArgumentException">The argument propertyName must not be null or empty.</exception>
      [NotifyPropertyChangedInvocator]
      protected virtual bool SetPropertyAndValidate<T>(ref T field, T value,
         [CallerMemberName] string propertyName = null)
      {
         if (string.IsNullOrEmpty(propertyName))
         {
            throw new ArgumentException("The argument propertyName must not be null or empty.");
         }

         if (!SetProperty(ref field, value, propertyName)) return false;
         return ValidateProperty(value, propertyName);
      }

      /// <summary>
      /// Set the property with the specified value and validate the property. If the value is not equal with the field then the field is
      /// set, a PropertyChanged event is raised, the property is validated and it returns true.
      /// </summary>
      /// <typeparam name="T">Type of the property.</typeparam>
      /// <param name="field">Reference to the backing field of the property.</param>
      /// <param name="value">The new value for the property.</param>
      /// <param name="expression">propertz expression - () => PropertyName </param>
      /// <returns>True if the value has changed, false if the old and new value were equal.</returns>
      /// <exception cref="ArgumentException">The argument expression must not be null.</exception>
      [NotifyPropertyChangedInvocator]
      protected virtual bool SetPropertyAndValidate<T>(ref T field, T value,
         Expression<Func<T>> expression)
      {
         if (expression == null)
         {
            throw new ArgumentNullException("expression", "The expression argument must not be null!");
         }
         var memberExpression = expression.Body as MemberExpression;
         if (memberExpression != null)
         {
            return SetPropertyAndValidate(ref field, value, memberExpression.Member.Name);
         }
         return false;
      }

      /// <summary>
      /// Validates the property with the specified value. The validation results are stored and can be retrieved by the 
      /// GetErrors method. If the validation results are changing then the ErrorsChanged event will be raised.
      /// </summary>
      /// <param name="value">The value of the property.</param>
      /// <param name="propertyName">The property name. This optional parameter can be skipped
      /// because the compiler is able to create it automatically.</param>
      /// <returns>True if the property value is valid, otherwise false.</returns>
      /// <exception cref="ArgumentException">The argument propertyName must not be null or empty.</exception>
      protected bool ValidateProperty(object value, [CallerMemberName] string propertyName = null)
      {
         if (string.IsNullOrEmpty(propertyName))
         {
            throw new ArgumentException("The argument propertyName must not be null or empty.");
         }

         var validationResults = new List<ValidationResult>();
         Validator.TryValidateProperty(value, new ValidationContext(this) { MemberName = propertyName }, validationResults);
         validationResults.AddRange(AdditionalValidationRules(value, propertyName));
         if (validationResults.Any())
         {
            errors[propertyName] = validationResults;
            RaiseErrorsChanged(propertyName);
            return false;
         }

         if (!errors.ContainsKey(propertyName) &&
             !errors.Values.Any(errs => errs.Any(item => item.MemberNames.Contains(propertyName)))) { return true; }

         errors.Remove(propertyName);
         var removeKeys = new List<String>();
         foreach (var error in errors)
         {
            if (error.Value.RemoveAll(item => item.MemberNames.Contains(propertyName)) == 0) continue;

            if (error.Value.Count == 0)
            {
               removeKeys.Add(error.Key);
            }
            RaiseErrorsChanged(error.Key);
         }
         foreach (var removeKey in removeKeys)
         {
            errors.Remove(removeKey);
         }

         RaiseErrorsChanged(propertyName);
         return true;
      }

      /// <summary>
      /// Raises the <see cref="E:ErrorsChanged"/> event.
      /// </summary>
      /// <param name="e">The <see cref="System.ComponentModel.DataErrorsChangedEventArgs"/> instance containing the event data.</param>
      protected virtual void OnErrorsChanged(DataErrorsChangedEventArgs e)
      {
         var handler = ErrorsChanged;
         if (handler != null)
         {
            handler(this, e);
         }
      }

      /// <summary>
      /// Raises the <see cref="ErrorsChanged"/> event
      /// </summary>
      /// <param name="propertyName"></param>
      protected void RaiseErrorsChanged([CallerMemberName] string propertyName = "")
      {
         HasErrors = errors.Any();
         OnErrorsChanged(new DataErrorsChangedEventArgs(propertyName));
      }

      /// <summary>
      /// Specify additional validation results based on child rules. Override this method in subclasses to get additional validation results
      /// which is not based on attributes
      /// </summary>
      /// <param name="value">value to be set on property</param>
      /// <param name="propertyName">property name to be validated</param>
      /// <returns></returns>
      protected virtual IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         return new ValidationResult[0];
      }

      #region Implementation of IDataErrorInfo

      /// <summary>
      /// Gets the error message for the property with the given name.
      /// </summary>
      /// <returns>
      /// The error message for the property. The default is an empty string ("").
      /// </returns>
      /// <param name="columnName">The name of the property whose error message to get. </param>
      public string this[string columnName]
      {
         get
         {
            var errs = GetErrors(columnName);
            return errs == NO_ERRORS
               ? ""
               : errs.Aggregate("", (current, validationResult) => current + validationResult.ErrorMessage + "\n");
         }
      }

      /// <summary>
      /// Gets an error message indicating what is wrong with this object.
      /// </summary>
      /// <returns>
      /// An error message indicating what is wrong with this object. The default is an empty string ("").
      /// </returns>
      public string Error
      {
         get
         {
            var errs = GetErrors(null);
            return errs == NO_ERRORS
               ? ""
               : errs.Aggregate("", (current, validationResult) => current + validationResult.ErrorMessage + "\n");
         }
      }

      #endregion
   }
}
