﻿using System.IO;

namespace SerialPort
{
   public interface ISerialPort
   {
      string PortName { get; }
      int BaudRate { get; set; }
      Parity Parity { get; set; }
      int RxTimeout { get; set; }
      Handshake Handshake { get; set; }
      int DataBits { get; set; }
      bool RtsEnable { get; set; }

      StopBit StopBits { get; set; }

      Stream Open();
   }
}