﻿namespace SerialPort
{
   public enum Handshake
   {
      None,
      XOnXOff,
      RequestToSend,
      RequestToSendXOnXOff,
      Hardware
   }
}
