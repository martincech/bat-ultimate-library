﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace SerialPort
{
   public class SerialPortAdapter : ISerialPort, IDisposable
   {
      private readonly System.IO.Ports.SerialPort port;

      #region Implementation of ISerialPort

      public SerialPortAdapter(System.IO.Ports.SerialPort port)
      {
         this.port = port;
      }

      public string PortName
      {
         get { return port.PortName; }
      }

      public int BaudRate
      {
         get { return port.BaudRate; }
         set { port.BaudRate = value; }
      }

      public Parity Parity
      {
         get { return port.GetParity(); }
         set { port.SetParity(value); }
      }

      public int RxTimeout
      {
         get { return port.ReadTimeout; }
         set { port.ReadTimeout = value; }
      }

      public Handshake Handshake
      {
         get { return port.GetHandshake(); }
         set { port.SetHandshake(value); }
      }

      public int DataBits
      {
         get { return port.DataBits; }
         set { port.DataBits = value; }
      }

      public bool RtsEnable
      {
         get { return port.RtsEnable; }
         set { port.RtsEnable = value; }
      }

      public StopBit StopBits
      {
         get { return port.GetStopBits(); }
         set { port.SetStopBits(value); }
      }

      public Stream Open()
      {
         if (port.IsOpen)
         {
            port.Close();
            Task.Delay(200);
         }

         port.Open();
         return port.BaseStream;
      }


      #endregion

      #region IDisposable

      public void Dispose()
      {
         if (port != null) port.Dispose();
      }

      #endregion
   }
}