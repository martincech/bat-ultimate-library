﻿namespace SerialPort
{
   public enum StopBit
   {
      One,
      OneHalf,
      Two
   }
}