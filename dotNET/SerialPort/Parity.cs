﻿namespace SerialPort
{
   public enum Parity
   {
      None,
      Odd,
      Even,
      Mark,
      Space
   }
}
