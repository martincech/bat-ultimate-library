﻿using System;
using System.IO.Ports;

namespace SerialPort
{
   public static class SerialPortExtensions
   {
      public static Parity GetParity(this System.IO.Ports.SerialPort port)
      {
         return (Parity) port.Parity;
      }

      public static void SetParity(this System.IO.Ports.SerialPort port, Parity parity)
      {
         port.Parity = (System.IO.Ports.Parity) parity;
      }

      public static Handshake GetHandshake(this System.IO.Ports.SerialPort port)
      {
         return (Handshake)port.Handshake;
      }

      public static void SetHandshake(this System.IO.Ports.SerialPort port, Handshake handshake)
      {
         port.Handshake = (System.IO.Ports.Handshake)handshake;
      }

        public static StopBit GetStopBits(this System.IO.Ports.SerialPort port)
        {
            switch (port.StopBits)
            {
                case StopBits.None:
                case StopBits.One:
                    return StopBit.One;
                case StopBits.Two:
                    return StopBit.Two;
                case StopBits.OnePointFive:
                    return StopBit.OneHalf;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static void SetStopBits(this System.IO.Ports.SerialPort port, StopBit stopBit)
        {
            switch (stopBit)
            {
                case StopBit.One:
                    port.StopBits = StopBits.One;
                    break;
                case StopBit.OneHalf:
                    port.StopBits = StopBits.OnePointFive;
                    break;
                case StopBit.Two:
                    port.StopBits = StopBits.Two;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("stopBit", stopBit, null);
            }
        }
    }
}