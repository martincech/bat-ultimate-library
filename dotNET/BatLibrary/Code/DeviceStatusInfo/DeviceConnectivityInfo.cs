﻿using System.Runtime.Serialization;
using BatLibrary.Sample;

namespace BatLibrary
{
   [DataContract]
   public class DeviceConnectivityInfo : TimeSample
   {
      [DataMember(Order = 3)]
      public string Type { get; set; }

      [DataMember(Order = 4)]
      public bool Connected { get; set; }
   }
}
