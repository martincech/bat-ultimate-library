﻿using System.Runtime.Serialization;

namespace BatLibrary
{
   [DataContract]
   public class SourceConnectivityInfo
   {
      [DataMember]
      public string Source { get; set; }
      [DataMember]
      public DeviceConnectivityInfo Device { get; set; }
   }
}
