﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class HumiditySample : TimeStampedSample
   {
      public HumiditySample(double value, DateTime timeStamp)
         : base(value, timeStamp)
      {
      }

      public HumiditySample(double value)
         : base(value)
      {
      }

      public HumiditySample(DateTime timeStamp)
         : base(timeStamp)
      {
      }

      public HumiditySample()
      {
      }
   }
}
