﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class TemperatureSample : TimeStampedSample
   {
      private const int DOUBLE_PRECISION = 5;

      private static double RoundToPrecision(double d)
      {
         return Math.Round(d, DOUBLE_PRECISION);
      }
      public TemperatureSample(double celsius, DateTime timeStamp)
         : base(RoundToPrecision(celsius), timeStamp)
      {
      }

      public TemperatureSample(double celsius)
         : this(celsius, DateTime.Now)
      {
      }

      public TemperatureSample(DateTime timeStamp)
         : this(0, timeStamp)
      {
      }

      public TemperatureSample()
      {
      }

      public double ToUnit(TemperatureUnits toUnit)
      {
         return RoundToPrecision(ConvertTemperature.Convert(Value, TemperatureUnits.Celsius, toUnit));
      }

      public void FromUnit(double value, TemperatureUnits unit)
      {
         Value = RoundToPrecision(ConvertTemperature.Convert(value, unit, TemperatureUnits.Celsius));
      }

   }
}
