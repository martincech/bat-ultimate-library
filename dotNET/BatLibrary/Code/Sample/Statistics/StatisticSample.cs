﻿using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class StatisticSample : TimeSample
   {
      /// <summary>
      /// Phone number of the scale
      /// </summary>
      [DataMember]
      public string PhoneNumber;

      /// <summary>
      /// Id. number of the scale
      /// </summary>
      [DataMember]
      public long ScaleNumber;

      /// <summary>
      /// Name of the scale
      /// </summary>
      [DataMember]
      public string ScaleName;

      /// <summary>
      /// Day number
      /// </summary>
      [DataMember]
      public int DayNumber;

      /// <summary>
      /// Male data
      /// </summary>
      [DataMember]
      public StatData MaleData;

      /// <summary>
      /// Female Data
      /// </summary>
      [DataMember]
      public StatData FemaleData;

      public override string ToString()
      {
         return $"Scale {ScaleName} id {ScaleNumber}(Phone: {PhoneNumber ?? ""}) day {DayNumber} date {TimeStamp} MALE:{(MaleData == null ? "NONE" : MaleData.ToString())} FEMALE:{(FemaleData == null ? "NONE" : FemaleData.ToString())}";
      }

   }
}
