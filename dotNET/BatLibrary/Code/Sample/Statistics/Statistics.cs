﻿using System;

namespace BatLibrary.Sample
{
   /// <summary>
   ///    Statistics of one day
   /// </summary>
   public class Statistics
   {
      public Statistics()
      {
         DateOfTheDay = default(DateTime);
      }

      /// <summary>
      ///    Date of the day
      /// </summary>
      public DateTime DateOfTheDay { get; set; }

      /// <summary>
      ///    Day number
      /// </summary>
      public int DayNumber { get; set; }

      /// <summary>
      ///    Target weight
      /// </summary>
      public float Target { get; set; }

      /// <summary>
      ///    Number of birds weighed
      /// </summary>
      public int Count { get; set; }

      /// <summary>
      ///    Average weight
      /// </summary>
      public float Average { get; set; }

      /// <summary>
      ///    Average from previous day
      /// </summary>
      public float LastAverage { get; set; }

      /// <summary>
      ///    Standard deviation
      /// </summary>
      public float Sigma { get; set; }

      /// <summary>
      ///    Uniformity in %
      /// </summary>
      public int Uniformity { get; set; }

      /// <summary>
      ///    Coefficient of variation
      /// </summary>
      public float Cv { get; set; }

      /// <inheritdoc />
      public override string ToString()
      {
         return string.Format("Day {0}({1}): " +
                              "Average {2} (Last {8}) " +
                              "Target {3}  " +
                              "Count {4}  " +
                              "Sigma {5}  " +
                              "Uniformity {6}  " +
                              "Cv {7}  ",
            DayNumber, DateOfTheDay, Average, Target, Count, Sigma, Uniformity, Cv, LastAverage);

      }
   }
}
