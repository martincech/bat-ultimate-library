﻿namespace BatLibrary.Sample
{
   public class StatData : Statistics
   {

      /// <summary>
      /// Daily gain
      /// </summary>
      public float Gain;

      /// <summary>
      /// Temperature
      /// </summary>
      public TemperatureSample Temperature;

      /// <summary>
      /// Carbon dioxide
      /// </summary>
      public Co2Sample CarbonDioxide;

      /// <summary>
      /// Ammonia
      /// </summary>
      public AmmoniaSample Ammonia;

      /// <summary>
      /// Humidity
      /// </summary>
      public HumiditySample Humidity;

      /// <inheritdoc />
      public override string ToString()
      {
         return string.Format("{0}" +
                              "Gain {1}",
                              base.ToString(), Gain);
      }
   }
}
