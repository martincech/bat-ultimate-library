﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class AmmoniaSample : TimeStampedSample
   {
      public AmmoniaSample(double value, DateTime timeStamp)
         : base(value, timeStamp)
      {
      }

      public AmmoniaSample(double value)
         : base(value)
      {
      }

      public AmmoniaSample(DateTime timeStamp)
         : base(timeStamp)
      {
      }

      public AmmoniaSample()
      {
      }
   }
}
