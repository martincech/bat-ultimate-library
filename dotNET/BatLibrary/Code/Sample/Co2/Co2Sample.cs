﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class Co2Sample : TimeStampedSample
   {
      public Co2Sample(double value, DateTime timeStamp)
         : base(value, timeStamp)
      {
      }

      public Co2Sample(double value)
         : base(value)
      {
      }

      public Co2Sample(DateTime timeStamp)
         : base(timeStamp)
      {
      }

      public Co2Sample()
      {
      }
   }
}
