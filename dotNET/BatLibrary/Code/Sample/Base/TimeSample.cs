﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public abstract class TimeSample
   {
      [DataMember(Order = 1)]
      public DateTime TimeStamp { get; set; }

      [DataMember(Order = 2)]
      public string SensorUid { get; set; }

      #region Overrides of Object

      /// <summary>
      /// Returns a string that represents the current object.
      /// </summary>
      /// <returns>
      /// A string that represents the current object.
      /// </returns>
      public override string ToString()
      {
         return $"{TimeStamp.ToString("yyyy-MM-ddTHH:mm:ss.fff")} D:{SensorUid}";
      }

      #endregion
   }
}
