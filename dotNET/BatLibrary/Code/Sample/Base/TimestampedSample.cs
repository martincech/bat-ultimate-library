using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class TimeStampedSample : TimeSample
   {
      public TimeStampedSample(double value, DateTime timeStamp)
      {
         TimeStamp = timeStamp;
         Value = value;
      }

      protected internal TimeStampedSample(double value)
         : this(value, DateTime.Now)
      {
      }

      protected internal TimeStampedSample(DateTime timeStamp)
         : this(new double(), timeStamp)
      {
      }

      public TimeStampedSample()
         : this(new double())
      {
      }

      [DataMember(Order = 2)]
      public double Value { get; set; }

      #region Overrides of Object

      /// <summary>
      /// Returns a string that represents the current object.
      /// </summary>
      /// <returns>
      /// A string that represents the current object.
      /// </returns>
      public override string ToString()
      {
         return $"{base.ToString()} {Value}";
      }

      #endregion

      #region Equality members

      protected bool Equals(TimeStampedSample other)
      {
         return Value.Equals(other.Value) &&
                TimeStamp.Equals(other.TimeStamp);
      }

      /// <summary>
      /// Determines whether the specified object is equal to the current object.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (ReferenceEquals(null, obj)) return false;
         if (ReferenceEquals(this, obj)) return true;
         if (obj.GetType() != GetType()) return false;
         return Equals((TimeStampedSample)obj);
      }

      /// <summary>
      /// Serves as the default hash function.
      /// </summary>
      /// <returns>
      /// A hash code for the current object.
      /// </returns>
      public override int GetHashCode()
      {
         return TimeStamp.GetHashCode();
      }

      public static bool operator ==(TimeStampedSample left, TimeStampedSample right)
      {
         return Equals(left, right);
      }

      public static bool operator !=(TimeStampedSample left, TimeStampedSample right)
      {
         return !Equals(left, right);
      }

      #endregion
   }
}