﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class DifferenceWeight : Weight
   {
      public DifferenceWeight(double grams = 0) :
         this(grams, WeightUnits.G)
      {
      }

      public DifferenceWeight(Weight weight)
         : this(weight.AsG)
      {
      }

      public DifferenceWeight(double? grams)
         : this(grams ?? 0)
      {
      }

      public DifferenceWeight(double weight, DateTime timeStamp)
         : base(weight, timeStamp)
      {
      }

      public DifferenceWeight(double weight, WeightUnits units)
         : base(Math.Abs(weight), units)
      {
         Difference = weight >= 0 ? DifferenceMode.Increased : DifferenceMode.Decreased;
      }
      [DataMember]
      public DifferenceMode Difference { get; set; }
   }
}
