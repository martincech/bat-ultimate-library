﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class RawWeight : Weight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public RawWeight(double grams = 0) : base(grams)
      {
      }

      public RawWeight()
         : base(0)
      {
      }

      public RawWeight(double grams, DateTime timeStamp)
         : base(grams, timeStamp)
      {
      }

      public RawWeight(Weight weight) : base(weight)
      {
      }

      public RawWeight(double? grams) : base(grams)
      {
      }

      public RawWeight(double weight, WeightUnits units) : base(weight, units)
      {
      }
   }
}
