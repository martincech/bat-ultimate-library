﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class BirdWeight : Weight
   {
      public BirdWeight()
         : this(0)
      {
      }

      public BirdWeight(double grams = 0)
         : this(grams, WeightUnits.G)
      {
      }

      public BirdWeight(double weight, DateTime timeStamp)
         : base(weight, timeStamp)
      {
      }

      public BirdWeight(Weight weight)
         : this(weight.AsG)
      {
      }
      public BirdWeight(DifferenceWeight weight) :
         base(weight.Difference == DifferenceMode.Decreased ? weight * -1 : weight)
      {
      }

      public BirdWeight(double? grams)
         : this(grams ?? 0)
      {
      }

      public BirdWeight(double weight, WeightUnits units) :
         base(weight, units)
      {
      }

      [DataMember]
      public Sex Sex { get; set; }

      #region Overrides of Weight

      /// <summary>
      /// Returns a string that represents the current object.
      /// </summary>
      /// <returns>
      /// A string that represents the current object.
      /// </returns>
      public override string ToString()
      {
         return string.Format("{0} {1}", base.ToString(), Sex);
      }

      #endregion
   }
}
