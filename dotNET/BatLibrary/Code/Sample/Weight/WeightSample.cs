﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class WeightSample : TimeStampedSample
   {
      private const double WEIGHT_RAW_CONSTANT = 10.0;

      public WeightSample(double rawWeight, DateTime timeStamp)
         : base(rawWeight, timeStamp)
      {
      }

      public WeightSample(double rawWeight)
         : base(rawWeight)
      {

      }

      public WeightSample(DateTime timeStamp)
         : base(timeStamp)
      {
      }

      public WeightSample()
      {
      }

      public double ToUnit(WeightUnits toWeightUnit)
      {
         return ConvertWeight.Convert(Value / WEIGHT_RAW_CONSTANT, WeightUnits.G, toWeightUnit);
      }

      public void FromUnit(double value, WeightUnits unit)
      {
         Value = (int)(ConvertWeight.Convert(value, unit, WeightUnits.G) * WEIGHT_RAW_CONSTANT);
      }
   }
}
