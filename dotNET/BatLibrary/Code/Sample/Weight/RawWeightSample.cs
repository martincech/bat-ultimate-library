﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary.Sample
{
   [DataContract]
   public class RawWeightSample : WeightSample
   {
      public RawWeightSample(double rawWeight, DateTime timeStamp)
         : base(rawWeight, timeStamp)
      {
      }

      public RawWeightSample(double rawWeight)
         : base(rawWeight)
      {
      }

      public RawWeightSample(DateTime timeStamp)
         : base(timeStamp)
      {
      }

      public RawWeightSample()
      {
      }
   }
}
