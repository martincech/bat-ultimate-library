﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary.Sample;

namespace BatLibrary
{
   [Flags]
   public enum DataType
   {
      None = 0,
      Temperature = 1,
      Humidity = 2,
      Co2 = 4,
      Ammonia = 8,
      Weight = 16,
      WeightStatistics = 32,
      BirdWeight = 64,

      Sensor = Temperature | Humidity | Co2 | Ammonia | Weight,
   }

   public static class DataTypeExtension
   {
      private static readonly Dictionary<DataType, Type> DATA_TYPE_TO_TYPE = new Dictionary<DataType, Type>
      {
         {DataType.Temperature, typeof(TemperatureSample)},
         {DataType.Humidity, typeof(HumiditySample)},
         {DataType.Co2, typeof(Co2Sample)},
         {DataType.Ammonia, typeof(AmmoniaSample)},
         {DataType.Weight, typeof(WeightSample)},
         {DataType.BirdWeight, typeof(BirdWeight)},
         {DataType.WeightStatistics, typeof(StatisticSample)},
      };

      public static DataType GetDataType(this TimeStampedSample sample)
      {
         return GetDataType(sample as TimeSample);
      }

      public static DataType GetDataType(this TimeSample sample)
      {
         if (sample == null || !DATA_TYPE_TO_TYPE.ContainsValue(sample.GetType())) return DataType.None;
         return DATA_TYPE_TO_TYPE.First(f => f.Value == sample.GetType()).Key;
      }

      public static string GetDataTypeName(this TimeStampedSample sample)
      {
         return GetDataTypeName(sample as TimeSample);
      }

      public static string GetDataTypeName(this TimeSample sample)
      {
         return GetDataType(sample).ToString();
      }

      public static Type GetSampleType(this DataType type)
      {
         if (!DATA_TYPE_TO_TYPE.ContainsKey(type)) return null;
         return DATA_TYPE_TO_TYPE[type];
      }

      public static TimeSample CreateSample(this DataType dataType, double value, DateTime timeStamp, Sex sex = Sex.Undefined)
      {
         switch (dataType)
         {
            case DataType.Temperature:
               return new TemperatureSample(value, timeStamp);
            case DataType.Humidity:
               return new HumiditySample(value, timeStamp);
            case DataType.Co2:
               return new Co2Sample(value, timeStamp);
            case DataType.Weight:
               return new WeightSample(value, timeStamp);
            case DataType.BirdWeight:
               return new BirdWeight(value, timeStamp) { Sex = sex };
            case DataType.Ammonia:
               return new AmmoniaSample(value, timeStamp);
         }
         return null;
      }
   }
}
