﻿namespace BatLibrary
{
   public enum TemperatureUnits
   {
      Celsius,
      Fahrenheit,
      Kelvin
   }
}
