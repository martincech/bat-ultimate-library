﻿using System;

namespace BatLibrary
{
   public class ConvertTemperature
   {
      public static double Convert(double value, TemperatureUnits oldUnits, TemperatureUnits newUnits)
      {
         switch (oldUnits)
         {
            case TemperatureUnits.Celsius:
               return CelsiusTo(value, newUnits);
            case TemperatureUnits.Fahrenheit:
               return FahrenheitTo(value, newUnits);
            case TemperatureUnits.Kelvin:
               return KelvinTo(value, newUnits);
            default:
               throw new ArgumentOutOfRangeException(nameof(oldUnits), oldUnits, null);
         }
      }

      public static double? Convert(double? value, TemperatureUnits oldUnits, TemperatureUnits newUnits)
      {
         if (value == null) return null;
         return Convert(value.Value, oldUnits, newUnits);
      }

      #region Private helpers

      private static double CelsiusTo(double value, TemperatureUnits units)
      {
         switch (units)
         {
            case TemperatureUnits.Celsius:
               return value;
            case TemperatureUnits.Fahrenheit:
               return value*9.0/5.0 + 32;
            case TemperatureUnits.Kelvin:
               return value + 273.15;
         }
         throw new ArgumentOutOfRangeException(nameof(units), units, null);
      }

      private static double FahrenheitTo(double value, TemperatureUnits units)
      {
         switch (units)
         {
            case TemperatureUnits.Celsius:
               return (value - 32)*5.0/9.0;
            case TemperatureUnits.Fahrenheit:
               return value;
            case TemperatureUnits.Kelvin:
               return (value + 459.67)*5.0/9.0;
         }
         throw new ArgumentOutOfRangeException(nameof(units), units, null);
      }

      private static double KelvinTo(double value, TemperatureUnits units)
      {
         switch (units)
         {
            case TemperatureUnits.Celsius:
               return value - 273.15;
            case TemperatureUnits.Fahrenheit:
               return 9.0/5.0*(value - 273.15) + 32;
            case TemperatureUnits.Kelvin:
               return value;
         }
         throw new ArgumentOutOfRangeException(nameof(units), units, null);
      }

      #endregion
   }
}
