﻿// ReSharper disable InconsistentNaming
namespace BatLibrary
{
   public enum WeightUnits
   {
      KG,
      G,
      LB,
   };
}
