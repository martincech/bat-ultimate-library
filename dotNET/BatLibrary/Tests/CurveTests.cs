﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatLibrary.Tests
{
   [TestClass]
   public class CurveTests
   {
      #region Private fields

      private static Curve _curve;
      private const int POINTS_START_X = 5;
      private const int POINTS_STOP_X = 50;
      private const int POINTS_INCREMENT = 5;
      private const int POINTS_VALUE_MULTIPLIER = 10;

      #endregion

      #region Init

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         _curve = new Curve();
      }

      [TestInitialize]
      public void CurveInitialize()
      {
         _curve.Points.Clear();
         for (var i = POINTS_START_X; i <= POINTS_STOP_X; i += POINTS_INCREMENT)
         {
            _curve.Points.Add(new CurvePoint(i, new Weight(i * POINTS_VALUE_MULTIPLIER)));
         }
      }

      #endregion

      [TestMethod]
      public void GetCurveValue_EmptyList()
      {
         _curve.Points.Clear();
         Assert.AreEqual(0, _curve.Points.Count);
         Assert.IsNull(_curve.GetCurveValue(10));
      }

      [TestMethod]
      public void GetCurveValue_ExistingPoint()
      {
         var valueX = POINTS_START_X + POINTS_INCREMENT;
         var expectedY = valueX * POINTS_VALUE_MULTIPLIER;

         Assert.AreEqual(expectedY, _curve.GetCurveValue(valueX).AsG);
      }

      [TestMethod]
      public void GetCurveValue_NoExistingPoint_Interpolation()
      {
         var day = (int)(POINTS_START_X + (POINTS_INCREMENT / 2.0));

         var point0 = _curve.Points.ElementAt(0);
         var point1 = _curve.Points.ElementAt(1);

         var interpolationResult = (int)CalcInterpolation(point0.X, point0.Y.AsG, point1.X, point1.Y.AsG, day);
         Assert.AreEqual(interpolationResult, _curve.GetCurveValue(day).AsG);
      }

      [TestMethod]
      public void GetCurveValue_NoExistingPoint_DaySmallerThanFirstPoint()
      {
         var value = _curve.GetCurveValue(POINTS_START_X - 1);
         Assert.IsNotNull(value);
         Assert.IsTrue(value < _curve.Points.First().Y);
      }

      [TestMethod]
      public void GetCurveValue_NoExistingPoint_DayGreaterThanLastPoint()
      {
         var value = _curve.GetCurveValue(POINTS_STOP_X + 1);
         Assert.IsNotNull(value);
         Assert.AreEqual(_curve.Points.Last().Y.AsG, value.AsG);
      }

      [TestMethod]
      public void GetCurveValue_DayInvalidValue()
      {
         Assert.IsNull(_curve.GetCurveValue(-1));
      }

      #region Private helpers

      private double CalcInterpolation(double x0, double y0, double x1, double y1, double x)
      {
         var y = y0 + ((y1 - y0)/(x1 - x0))*(x - x0);
         return y;
      }

      #endregion
   }
}
