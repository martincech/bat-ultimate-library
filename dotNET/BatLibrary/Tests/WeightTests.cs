﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatLibrary.Tests
{
   [TestClass]
   public class WeightTests
   {
      #region Private fields and Properties

      private const double REFERENCE_G = 125;
      private const double GRAMS_IN_ONE_LIBRA = 453.59237;
      private static int _originGDecimalPrecision;

      private const double OPERAND1 = 211.2;
      private const double OPERAND2 = 128.5;
      private const double NEGATIVE_OPERAND1 = OPERAND1*-1;
      private const double NEGATIVE_OPERAND2 = OPERAND2*-1;

      private double SmallerNumber
      {
         get { return OPERAND1; }
      }

      private double GreaterNumber
      {
         get { return OPERAND1 + 10; }
      }

      #endregion

      #region Init

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         _originGDecimalPrecision = Weight.GDecimalPrecision;
      }

      [TestInitialize]
      public void TestInit()
      {
         //Set origin decimal precision
         Weight.GDecimalPrecision = _originGDecimalPrecision;
      }

      #endregion

      [TestMethod]
      public void Weight_Conversion()
      {
         const double value = REFERENCE_G;
         var weight = new Weight(value);
         CompareValues(value, weight.AsG);
         CompareValues(GetKg(value), weight.AsKg, 4);
         CompareValues(GetLb(value), weight.AsLb, 4);
      }

      #region Operators

      [TestMethod]
      public void Operator_Plus()
      {
         CheckOperatorPlus(OPERAND1, OPERAND2);
         CheckOperatorPlus(NEGATIVE_OPERAND1, OPERAND2);
         CheckOperatorPlus(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
      }

      [TestMethod]
      public void Operator_Minus()
      {
         // switch operand due to minus value
         CheckOperatorMinus(OPERAND1, OPERAND2);
         CheckOperatorMinus(OPERAND2, OPERAND1);
         CheckOperatorMinus(NEGATIVE_OPERAND1, OPERAND2);
         CheckOperatorMinus(OPERAND2, NEGATIVE_OPERAND1);
         CheckOperatorMinus(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
      } 

      [TestMethod]
      public void Operator_Multiply()
      {
         CheckOperatorMultiply(OPERAND1, OPERAND2);
         CheckOperatorMultiply(NEGATIVE_OPERAND1, OPERAND2);
         CheckOperatorMultiply(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentException))]
      public void Operator_Divide()
      {
         CheckOperatorDivide(OPERAND1, OPERAND2);
         CheckOperatorDivide(OPERAND2, OPERAND1);
         CheckOperatorDivide(NEGATIVE_OPERAND1, OPERAND2);
         CheckOperatorDivide(NEGATIVE_OPERAND1, NEGATIVE_OPERAND2);
         CheckOperatorDivide(0, OPERAND2);
         CheckOperatorDivide(OPERAND1, 0);
      }

      [TestMethod]
      public void Operator_LessThan()
      {
         Assert.IsTrue(CheckOperatorLess(SmallerNumber, GreaterNumber));
         Assert.IsFalse(CheckOperatorLess(GreaterNumber, SmallerNumber));
         Assert.IsFalse(CheckOperatorLess(GreaterNumber, GreaterNumber));
      }

      [TestMethod]
      public void Operator_LessThanOrEqual()
      {
         Assert.IsTrue(CheckOperatorLessOrEqual(SmallerNumber, GreaterNumber));
         Assert.IsFalse(CheckOperatorLessOrEqual(GreaterNumber, SmallerNumber));
         Assert.IsTrue(CheckOperatorLessOrEqual(GreaterNumber, GreaterNumber));
      }

      [TestMethod]
      public void Operator_GreaterThan()
      {
         Assert.IsFalse(CheckOperatorGreater(SmallerNumber, GreaterNumber));
         Assert.IsTrue(CheckOperatorGreater(GreaterNumber, SmallerNumber));
         Assert.IsFalse(CheckOperatorGreater(GreaterNumber, GreaterNumber));
      }

      [TestMethod]
      public void Operator_GreaterThanOrEqual()
      {
         Assert.IsFalse(CheckOperatorGreaterOrEqual(SmallerNumber, GreaterNumber));
         Assert.IsTrue(CheckOperatorGreaterOrEqual(GreaterNumber, SmallerNumber));
         Assert.IsTrue(CheckOperatorGreaterOrEqual(GreaterNumber, GreaterNumber));
      }

      [TestMethod]
      public void Operator_Equal()
      {
         Assert.IsTrue(CheckOperatorEqual(SmallerNumber, SmallerNumber));
         Assert.IsTrue(CheckOperatorEqual(NEGATIVE_OPERAND1, NEGATIVE_OPERAND1));
         Assert.IsFalse(CheckOperatorEqual(SmallerNumber, GreaterNumber));
         Assert.IsFalse(CheckOperatorEqual(OPERAND1, NEGATIVE_OPERAND1));
      }

      [TestMethod]
      public void Operator_NotEqual()
      {
         Assert.IsFalse(CheckOperatorNotEqual(SmallerNumber, SmallerNumber));
         Assert.IsFalse(CheckOperatorNotEqual(NEGATIVE_OPERAND1, NEGATIVE_OPERAND1));
         Assert.IsTrue(CheckOperatorNotEqual(SmallerNumber, GreaterNumber));
         Assert.IsTrue(CheckOperatorNotEqual(OPERAND1, NEGATIVE_OPERAND1));
      }

      #region Private helpers

      private void CheckOperatorPlus(double op1, double op2)
      {
         var expectedResult = op1 + op2;
         CompareValues(expectedResult, (new Weight(op1) + new Weight(op2)).AsG);
         CompareValues(expectedResult, (new Weight(op1) + op2).AsG);
         CompareValues(expectedResult, (op1 + new Weight(op2)).AsG);
      }

      private void CheckOperatorMinus(double op1, double op2)
      {
         var expectedResult = op1 - op2;
         CompareValues(expectedResult, (new Weight(op1) - new Weight(op2)).AsG);
         CompareValues(expectedResult, (new Weight(op1) - op2).AsG);
         CompareValues(expectedResult, (op1 - new Weight(op2)).AsG);
      }

      private void CheckOperatorMultiply(double op1, double op2)
      {
         var expectedResult = op1 * op2;
         CompareValues(expectedResult, (new Weight(op1) * new Weight(op2)).AsG);
         CompareValues(expectedResult, (new Weight(op1) * op2).AsG);
         CompareValues(expectedResult, (op1 * new Weight(op2)).AsG);
      }

      private void CheckOperatorDivide(double op1, double op2)
      {
         var expectedResult = op1 / op2;
         CompareValues(expectedResult, (new Weight(op1) / new Weight(op2)).AsG);
         CompareValues(expectedResult, (new Weight(op1) / op2).AsG);
         CompareValues(expectedResult, (op1 / new Weight(op2)).AsG);
      }

      private bool CheckOperatorLess(double op1, double op2)
      {
         return new Weight(op1) < new Weight(op2) &&
                new Weight(op1) < op2 &&
                op1 < new Weight(op2);
      }

      private bool CheckOperatorLessOrEqual(double op1, double op2)
      {
         return new Weight(op1) <= new Weight(op2) &&
                new Weight(op1) <= op2 &&
                op1 <= new Weight(op2);
      }

      private bool CheckOperatorGreater(double op1, double op2)
      {
         return new Weight(op1) > new Weight(op2) &&
                new Weight(op1) > op2 &&
                op1 > new Weight(op2);
      }

      private bool CheckOperatorGreaterOrEqual(double op1, double op2)
      {
         return new Weight(op1) >= new Weight(op2) &&
                new Weight(op1) >= op2 &&
                op1 >= new Weight(op2);
      }

      private bool CheckOperatorEqual(double op1, double op2)
      {
         return new Weight(op1) == new Weight(op2) &&
                new Weight(op1) == op2 &&
                op1 == new Weight(op2);
      }

      private bool CheckOperatorNotEqual(double op1, double op2)
      {
         return new Weight(op1) != new Weight(op2) &&
                 new Weight(op1) != op2 &&
                 op1 != new Weight(op2);
      }

      //private void CheckOperatorResult(Func<object, object, Weight> op, double arg1, double arg2, double expectedResult)
      //{
      //   OperatorProcess(op, new Weight(arg1), new Weight(arg2), expectedResult);
      //   OperatorProcess(op, arg1, new Weight(arg2), expectedResult);
      //   OperatorProcess(op, new Weight(arg1), arg2, expectedResult);
      //}

      //private void OperatorProcess<T, U>(Func<T, U, Weight> op, T arg1, U arg2, double expectedResult)
      //{
      //   CompareValues(expectedResult, op(arg1, arg2).AsG);
      //}

      #endregion

      #endregion

      [TestMethod]
      public void DecimalPrecision()
      {
         const double c1 = 10.04;
         const double c2 = c1 + 0.0005;

         Weight.GDecimalPrecision = 2;
         Assert.IsTrue(CheckOperatorEqual(c1, c2));
         Assert.IsFalse(CheckOperatorNotEqual(c1, c2));
         Assert.IsTrue(CheckOperatorLessOrEqual(c2, c1));
         Assert.IsFalse(CheckOperatorLess(c1, c2));
         Assert.IsTrue(CheckOperatorGreaterOrEqual(c1, c2));
         Assert.IsFalse(CheckOperatorGreater(c2, c1));

         // change precision
         Weight.GDecimalPrecision = 5;
         Assert.IsFalse(CheckOperatorEqual(c1, c2));
         Assert.IsTrue(CheckOperatorNotEqual(c1, c2));
         Assert.IsFalse(CheckOperatorLessOrEqual(c2, c1));
         Assert.IsTrue(CheckOperatorLess(c1, c2));
         Assert.IsFalse(CheckOperatorGreaterOrEqual(c1, c2));
         Assert.IsTrue(CheckOperatorGreater(c2, c1));
      }

      [TestMethod]
      public void Equals()
      {
         var obj1 = GetWeightObject(100);
         var obj2 = GetWeightObject(100);
         var obj3 = GetWeightObject(250);

         Assert.IsTrue(obj1.Equals(obj2));
         Assert.IsFalse(obj1.Equals(obj3));
         Assert.IsFalse(obj1.Equals(null));
         object o = 5;
         Assert.IsFalse(obj1.Equals(o));
      }

      [TestMethod]
      public void CompareTo()
      {
         double baseValue = 100.0, lessValue = 50.0, greaterValue = 300.0;

         var obj1 = GetWeightObject(baseValue);
         var obj2 = GetWeightObject(baseValue);
         var obj3 = GetWeightObject(lessValue);
         var obj4 = GetWeightObject(greaterValue);
         Thread.Sleep(1);
         var obj5 = GetWeightObject(baseValue);
         //compare weight
         Assert.AreEqual(0, obj1.CompareTo(baseValue));
         Assert.AreEqual(1, obj1.CompareTo(lessValue));
         Assert.AreEqual(-1, obj1.CompareTo(greaterValue));
         Assert.AreEqual(1, obj1.CompareTo(obj3));
         Assert.AreEqual(-1, obj1.CompareTo(obj4));

         //compare TimeStamp
         Assert.AreEqual(0, obj1.CompareTo(obj2));
         Assert.AreEqual(-1, obj1.CompareTo(obj5));
      }

      #region Private helpers

      private Weight GetWeightObject(double weight)
      {
         return new Weight(weight);
      }

      private void CompareValues(double expected, double actual, int precision)
      {
         Assert.AreEqual(Math.Round(expected, precision), Math.Round(actual, precision));
      }

      private void CompareValues(double expected, double actual)
      {
         CompareValues(expected, actual, Weight.GDecimalPrecision);
      }

      private double GetLb(double grams)
      {
         return grams/GRAMS_IN_ONE_LIBRA;
      }

      private double GetKg(double grams)
      {
         return grams/1000;
      }

      #endregion
   }
}
