﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatLibrary.Tests
{
   [TestClass]
   public class PercentTests
   {
      private double number;
      private Percent uut;
      private Percent greaterUut;
      private Percent sameUut;
      private const double DEFAULT_NUMBER = 10;

      [TestInitialize]
      public void Init()
      {
         uut = new Percent(DEFAULT_NUMBER);
         greaterUut = new Percent(DEFAULT_NUMBER * 2);
         sameUut = new Percent(DEFAULT_NUMBER);
         number = 100.0;
      }

      [TestMethod]
      [ExpectedException(typeof (ArgumentOutOfRangeException))]
      public void OutOfRange_ForNegativePercent()
      {
         uut = new Percent(-5);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void OutOfRange_ForMoreThan100()
      {
         uut = new Percent(101);
      }

      [TestMethod]
      public void Valid_For_0_And_100()
      {
         uut = new Percent(0);
         Assert.AreEqual(0, (double)uut);
         uut = new Percent(100);
         Assert.AreEqual(100, (double)uut);

      }
      [TestMethod]
      public void Double_Plus_Percent()
      {
         Assert.AreEqual(110, number + uut);
      }

      [TestMethod]
      public void Percent_Plus_Double()
      {
         Assert.AreEqual(110, uut + number);
      }

      [TestMethod]
      public void Double_Minus_Percent()
      {
         Assert.AreEqual(90, number - uut);
      }

      [TestMethod]
      public void Percent_Minus_Double()
      {
         Assert.AreEqual(-90, uut - number);
      }

      [TestMethod]
      public void Double_Times_Percent()
      {
         Assert.AreEqual(1000, number * uut);
      }

      [TestMethod]
      public void Percent_Times_Double()
      {
         Assert.AreEqual(1000, uut * number);
      }

      [TestMethod]
      public void Double_Div_Percent()
      {
         Assert.AreEqual(10, number / uut);
      }

      [TestMethod]
      public void Percent_Div_Double()
      {
         Assert.AreEqual(0.1, uut / number);
      }


      //TODO testy na porovnání

      [TestMethod]
      public void Percent_Equals_Percent()
      {
         var percent = uut; 
         Assert.IsTrue(uut == sameUut);
         Assert.IsTrue(uut == percent);
      }

      [TestMethod]
      public void Percent_Not_Equals_Percent()
      {
         Assert.IsTrue(uut != greaterUut);
      }

      [TestMethod]
      public void Percent_Equals_Double()
      {
         number = DEFAULT_NUMBER;
         Assert.IsTrue(uut == number);
         Assert.IsTrue(number == uut);
      }

      [TestMethod]
      public void Percent_Not_Equals_Double()
      {
         Assert.IsTrue(uut != number);
         Assert.IsTrue(number != uut);
      }

      [TestMethod]
      public void Percent_CompareTo_Percent()
      {
         Assert.AreEqual(1, uut.CompareTo(new Percent(DEFAULT_NUMBER - 1)));
         Assert.AreEqual(0, uut.CompareTo(new Percent(DEFAULT_NUMBER)));
         Assert.AreEqual(-1, uut.CompareTo(new Percent(DEFAULT_NUMBER + 1)));
      }

      [TestMethod]
      public void Percent_CompareTo_Double()
      {
         Assert.AreEqual(1, uut.CompareTo(DEFAULT_NUMBER - 1));
         Assert.AreEqual(0, uut.CompareTo(DEFAULT_NUMBER));
         Assert.AreEqual(-1, uut.CompareTo(DEFAULT_NUMBER + 1));
      }
   }
}
