﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatLibrary.Tests
{
   [TestClass]
   public class ConvertTemperatureTests
   {
      #region Private fields

      private const int ROUND_PRECISION = 2;
      private const double LOW_VALUE = 0;
      private const double HIGH_VALUE = 20;

      private const double KELVIN_IN_0_CELSIUS = 273.15;
      private const double KELVIN_IN_0_FAHRENHEIT = 255.37;
      private const double KELVIN_IN_20_CELSIUS = 293.15;
      private const double KELVIN_IN_20_FAHRENHEIT = 266.48;

      private const double CELSIUS_IN_0_KELVIN = -273.15;
      private const double CELSIUS_IN_0_FAHRENHEIT = -17.78;
      private const double CELSIUS_IN_20_KELVIN = -253.15;
      private const double CELSIUS_IN_20_FAHRENHEIT = -6.67;

      private const double FAHRENHEIT_IN_0_KELVIN = -459.67;
      private const double FAHRENHEIT_IN_0_CELSIUS = 32;
      private const double FAHRENHEIT_IN_20_KELVIN = -423.67;
      private const double FAHRENHEIT_IN_20_CELSIUS = 68;

      #endregion

      [TestMethod]
      public void ConvertToKelvin()
      {
         const TemperatureUnits targetTemp = TemperatureUnits.Kelvin;
         Assert.AreEqual(KELVIN_IN_0_CELSIUS, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Celsius, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(KELVIN_IN_0_FAHRENHEIT, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Fahrenheit, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(0, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Kelvin, targetTemp), ROUND_PRECISION));

         Assert.AreEqual(KELVIN_IN_20_CELSIUS, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Celsius, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(KELVIN_IN_20_FAHRENHEIT, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Fahrenheit, targetTemp), ROUND_PRECISION));
      }

      [TestMethod]
      public void ConvertToCelsius()
      {
         const TemperatureUnits targetTemp = TemperatureUnits.Celsius;
         Assert.AreEqual(CELSIUS_IN_0_KELVIN, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Kelvin, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(CELSIUS_IN_0_FAHRENHEIT, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Fahrenheit, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(0, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Celsius, targetTemp), ROUND_PRECISION));

         Assert.AreEqual(CELSIUS_IN_20_KELVIN, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Kelvin, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(CELSIUS_IN_20_FAHRENHEIT, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Fahrenheit, targetTemp), ROUND_PRECISION));
      }

      [TestMethod]
      public void ConvertToFahrenheit()
      {
         const TemperatureUnits targetTemp = TemperatureUnits.Fahrenheit;
         Assert.AreEqual(FAHRENHEIT_IN_0_KELVIN, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Kelvin, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(FAHRENHEIT_IN_0_CELSIUS, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Celsius, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(0, Math.Round(ConvertTemperature.Convert(LOW_VALUE, TemperatureUnits.Fahrenheit, targetTemp), ROUND_PRECISION));

         Assert.AreEqual(FAHRENHEIT_IN_20_KELVIN, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Kelvin, targetTemp), ROUND_PRECISION));
         Assert.AreEqual(FAHRENHEIT_IN_20_CELSIUS, Math.Round(ConvertTemperature.Convert(HIGH_VALUE, TemperatureUnits.Celsius, targetTemp), ROUND_PRECISION));
      }
   }
}
