﻿using System.Collections.Generic;

namespace Repository
{
   public interface ICollectionRepository<T> : IRepository<T> where T : EntityBase
   {
      void Add(ICollection<T> entity);
      void Delete(long id, int type);
      IEnumerable<T> GetById(long id, int type);
   }
}
