﻿using System.Collections.Generic;

namespace Repository
{
    public interface IRepository<T> where T : EntityBase
    {
       IEnumerable<T> GetAll();
       long Add(T entity);
       void Delete(long id);
       void Update(T entity);
       T GetById(long id);
    }
}
