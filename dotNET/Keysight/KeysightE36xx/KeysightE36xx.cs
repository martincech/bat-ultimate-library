﻿//******************************************************************************
//
//   34410.cs     Keysight 34410A multimeter
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System.Threading;
using Agilent.AgilentE36xx.Interop;

namespace Keysight
{
   /// <summary>
   /// Keysight E36xx power supply
   /// </summary>
// ReSharper disable once InconsistentNaming
   public class KeysightE36xx
   {
      #region Private fields

      private readonly AgilentE36xx powerSupply;

      #endregion

      /// <summary>
      /// Represents an output of power supply
      /// </summary>
// ReSharper disable once InconsistentNaming
      public class KeysightE36xxOutput
      {
         #region Private properties

         private readonly IAgilentE36xxOutput output;

         #endregion

         /// <summary>
         /// Constructor
         /// </summary>
         public KeysightE36xxOutput(IAgilentE36xxOutput output)
         {
            this.output = output;
         }

         /// <summary>
         /// Current
         /// </summary>
         public double Current
         {
            get
            {
               return output.Measure(AgilentE36xxMeasurementTypeEnum.AgilentE36xxMeasurementCurrent);
            }
            set
            {
               output.CurrentLimit = value;
               Thread.Sleep(100);
            }
         }

         /// <summary>
         /// Voltage
         /// </summary>
         public double Voltage
         {
            get
            {
               return output.Measure(AgilentE36xxMeasurementTypeEnum.AgilentE36xxMeasurementVoltage);
            }
            set
            {
               output.VoltageLevel = value;
               Thread.Sleep(100);
            }
         }
      }

      /// <summary>
      /// Outputs of the power supply
      /// </summary>
// ReSharper disable once InconsistentNaming
      public class KeysightE36xxOutputs
      {
         #region Private fields

         private readonly AgilentE36xx powerSupply;
         private readonly KeysightE36xxOutput[] outputList;

         #endregion


         /// <summary>
         /// Constructor
         /// </summary>
         public KeysightE36xxOutputs(AgilentE36xx powerSupply)
         {
            this.powerSupply = powerSupply;
            outputList = new KeysightE36xxOutput[powerSupply.Outputs.Count];

            for (var i = 0; i < powerSupply.Outputs.Count; i++)
            {
               outputList[i] = new KeysightE36xxOutput(powerSupply.Outputs.Item[powerSupply.Outputs.Name[i + 1]]);
            }
         }

         /// <summary>
         /// Indexer of individual outputs of the power supply
         /// </summary>
         public KeysightE36xxOutput this[int i]
         {
            get
            {
               return outputList[i];
            }
         }

         /// <summary>
         /// Turn on power supply outputs
         /// </summary>
         public void TurnOn()
         {
            powerSupply.Outputs.Enabled = true;
            Thread.Sleep(100);
         }

         /// <summary>
         /// Turn off power supply outputs
         /// </summary>
         public void TurnOff()
         {
            powerSupply.Outputs.Enabled = false;
            Thread.Sleep(100);
         }
      }

      public readonly KeysightE36xxOutputs Outputs;

      /// <summary>
      /// Constructor
      /// </summary>
      public KeysightE36xx(string address)
      {
         powerSupply = new AgilentE36xx();
         powerSupply.Initialize(address, false, true);
         Outputs = new KeysightE36xxOutputs(powerSupply);
      }

      /// <summary>
      /// Finalize
      /// </summary>
      ~KeysightE36xx()
      {
         powerSupply.Close();
      }
   }
}