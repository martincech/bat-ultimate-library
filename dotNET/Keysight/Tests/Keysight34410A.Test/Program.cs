﻿using System;
using System.Threading;
using Keysight;

namespace Keysight34410ATest
{
   class Program
   {
      static void Main(string[] args)
      {
         try { 
         var multimeter = new Keysight34410A("USB0::0x0957::0x0607::MY47022753::0::INSTR");

         while (true) { 
            Console.WriteLine(multimeter.Current);
            Thread.Sleep(1000);
         }
         }
         catch { }
      }
   }
}
