﻿using System;
using System.Text;

namespace Utilities.RollingFile
{
   public class Parameters
   {
      #region Private fields

      private const string DEFAULT_LOG_FILE = @"c:\temp\logfile.txt";
      private const int DEFAULT_ROLLED_LOG_COUNT = 3;
      private static readonly Encoding DEFAULT_ENCODING = Encoding.UTF8;
      private string fileName;

      /// <summary>
      /// 1 * 1024 * 1024; small value for testing that it works, you can try yourself, and then use a reasonable size, like 1M-10M
      /// </summary>
      private const int DEFAULT_LOGSIZE = 1024*1024;

      #endregion

      public Parameters()
      {
         fileName = DEFAULT_LOG_FILE;
         MaxRolledFileCount = DEFAULT_ROLLED_LOG_COUNT;
         MaxFileSize = DEFAULT_LOGSIZE;
         Encoding = DEFAULT_ENCODING;
      }

      public event EventHandler FileNameChanged;

      public string FileName
      {
         get { return fileName; }
         set
         {
            fileName = value;
            Invoke();
         }
      }

      public int MaxRolledFileCount { get; set; }
      public int MaxFileSize { get; set; }
      public Encoding Encoding { get; set; }

      private void Invoke()
      {
         if (FileNameChanged != null)
         {
            FileNameChanged(this, new EventArgs());
         }
      }
   }
}
