﻿using System;

namespace Utilities.RollingFile
{
   public class RollingFile : Parameters, IRolling
   {
      private RollerBase roller;

      #region Constructors

      public RollingFile()
         : this(RollingMode.RollActualData)
      {
      }

      public RollingFile(RollingMode mode)
      {
         Mode = mode;
         RollerEngineInitialize();
      }

      #endregion

      public RollingMode Mode { get; private set; }

      public void LogMessage(string msg)
      {
         roller.LogMessage(msg);
      }

      #region Private helpers

      private void RollerEngineInitialize()
      {
         switch (Mode)
         {
            case RollingMode.RollActualData:
               roller = new ActualDataRoller(this);
               break;
            case RollingMode.RollHistoricdata:
               roller = new HistoricDataRoller(this);
               break;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion
   }
}
