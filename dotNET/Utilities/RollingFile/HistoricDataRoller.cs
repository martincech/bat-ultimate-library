﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Utilities.RollingFile
{
   public class HistoricDataRoller : RollerBase
   {
      public HistoricDataRoller(Parameters parameters)
         : base(parameters)
      {
      }

      #region Overrides of RollerBase

      protected override void Roll(string file)
      {
         try
         {
            var path = Path.GetDirectoryName(file);
            if (path == null) throw new ArgumentNullException();
            if (!Directory.Exists(path))
            {
               Directory.CreateDirectory(path);
            }

            var fileInfo = new FileInfo(file);
            if (!fileInfo.Exists) return;

            var length = fileInfo.Length;
            if (length <= Parameters.MaxFileSize) return;

            var wildLogName = Path.GetFileNameWithoutExtension(file) + "*" + Path.GetExtension(file);
            var barefile = Path.Combine(path, Path.GetFileNameWithoutExtension(file));
            var logFileList = Directory.GetFiles(path, wildLogName, SearchOption.TopDirectoryOnly);

            if (logFileList.Length <= 0) return;
            // only take files like logfilename.log and logfilename.0.log, so there also can be a maximum of 10 additional rolled files (0..9)
            var rolledLogFileList = logFileList.Where(fileName => fileName.Length == (file.Length + 2)).ToArray();
            Array.Sort(rolledLogFileList, 0, rolledLogFileList.Length);
            if (rolledLogFileList.Length >= Parameters.MaxRolledFileCount)
            {
               File.Delete(rolledLogFileList[Parameters.MaxRolledFileCount - 1]);
               var list = rolledLogFileList.ToList();
               list.RemoveAt(Parameters.MaxRolledFileCount - 1);
               rolledLogFileList = list.ToArray();
            }
            // move remaining rolled files
            for (var i = rolledLogFileList.Length; i > 0; --i)
            {
               File.Move(rolledLogFileList[i - 1], barefile + "." + i + Path.GetExtension(file));
            }
            var targetPath = barefile + ".0" + Path.GetExtension(file);
            // move original file
            File.Move(file, targetPath);
         }
         catch (Exception ex)
         {
            Debug.WriteLine(ex.ToString());
         }
      }

      protected override string GetWriteableFile()
      {
         return Parameters.FileName;
      }

      #endregion
   }
}
