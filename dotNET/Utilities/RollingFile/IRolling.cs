﻿namespace Utilities.RollingFile
{
   public interface IRolling
   {
      void LogMessage(string msg);
   }
}
