﻿using System;
using System.IO;

namespace Utilities.RollingFile
{
   public abstract class RollerBase : IRolling
   {
      protected RollerBase(Parameters parameters)
      {
         Parameters = parameters;
      }

      public Parameters Parameters;

      public void LogMessage(string msg)
      {
         lock (Parameters.FileName) // lock is optional, but.. should this ever be called by multiple threads, it is safer
         {
            try
            {
               Roll(GetWriteableFile());
               File.AppendAllText(GetWriteableFile(), msg + Environment.NewLine, Parameters.Encoding);
            }
            catch (IOException)
            {
            }
         }
      }

      protected abstract void Roll(string file);
      protected abstract string GetWriteableFile();
   }
}
