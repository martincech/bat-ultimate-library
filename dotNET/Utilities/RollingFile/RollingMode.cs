﻿namespace Utilities.RollingFile
{
   public enum RollingMode
   {
      RollActualData,
      RollHistoricdata
   }
}
