﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Utilities.RollingFile
{
   public class ActualDataRoller : RollerBase
   {
      #region Private fields

      private int fileIndex;
      private string fileWithoutExtension;
      private string fileExtension;
      private string path;
      private string indexedFileName;

      #endregion

      public ActualDataRoller(Parameters parameters)
         : base(parameters)
      {
         fileIndex = 0;
         indexedFileName = Parameters.FileName;
         SetLastWrittenFile();
         Parameters.FileNameChanged += ParametersOnFileNameChanged;
      }

      #region Overrides of RollerBase

      protected override void Roll(string file)
      {
         try
         {
            CreateFolderIfNotexist(Path.GetDirectoryName(file));
            var fileInfo = new FileInfo(file);
            if (!fileInfo.Exists) return;

            var length = fileInfo.Length;
            if (length <= Parameters.MaxFileSize) return;

            fileIndex = (fileIndex + 1) % Parameters.MaxRolledFileCount;
            indexedFileName = GetFileFullName();

            if (File.Exists(indexedFileName))
            {
               File.Delete(indexedFileName);
            }
         }
         catch (Exception ex)
         {
            Debug.WriteLine(ex.ToString());
         }
      }

      protected override string GetWriteableFile()
      {
         return indexedFileName;
      }

      #endregion

      #region Private helpers

      private void ParametersOnFileNameChanged(object sender, EventArgs eventArgs)
      {
         SetLastWrittenFile();
      }

      private void SetLastWrittenFile()
      {
         path = Path.GetDirectoryName(Parameters.FileName);
         fileWithoutExtension = Path.GetFileNameWithoutExtension(Parameters.FileName);
         fileExtension = Path.GetExtension(Parameters.FileName);

         var wildLogName = fileWithoutExtension + "*" + fileExtension;
         if (path == null) throw new ArgumentNullException();
         if (path.Equals(string.Empty)) path = Path.DirectorySeparatorChar.ToString();
         CreateFolderIfNotexist(path);
         var logFileList = Directory.GetFiles(path, wildLogName, SearchOption.TopDirectoryOnly);

         if (logFileList.Length <= 0)
         {
            indexedFileName = GetFileFullName();
            return;
         }
         var rolledLogFileList = new Dictionary<string, int>();
         foreach (var f in logFileList)
         {
            var index = GetFileIndex(f, Parameters.MaxRolledFileCount);
            if (index >= 0)
            {
               rolledLogFileList.Add(f, index);
            }
         }

         fileIndex = GetLastWriteFileIndex(rolledLogFileList);
         indexedFileName = GetFileFullName();
      }

      private int GetLastWriteFileIndex(Dictionary<string, int> rolledLogFileList)
      {
         DateTime? lastTimeStamp = null;
         var lastIndex = 0;
         foreach (var record in rolledLogFileList)
         {
            //get last write file
            var timeStamp = File.GetLastWriteTime(record.Key);
            if (lastTimeStamp != null && DateTime.Compare(timeStamp, lastTimeStamp.Value) <= 0) continue;
            lastIndex = record.Value;
            lastTimeStamp = timeStamp;
         }
         return lastIndex;
      }

      private int GetFileIndex(string name, int maxFileCount)
      {
         var nameWithoutExtension = Path.GetFileNameWithoutExtension(name);
         if (nameWithoutExtension == null) return -1;
         var indexStr = nameWithoutExtension.Replace(fileWithoutExtension, "");

         int indexNum;
         if (!int.TryParse(indexStr, out indexNum) || indexNum < maxFileCount)
         {
            indexNum = -1;
         }
         return indexNum;
      }

      private string GetFileFullName()
      {
         return Path.Combine(path, fileWithoutExtension + fileIndex + fileExtension);
      }

      private void CreateFolderIfNotexist(string folder)
      {
         if (!Directory.Exists(folder))
         {
            Directory.CreateDirectory(folder);
         }
      }

      #endregion
   }
}
