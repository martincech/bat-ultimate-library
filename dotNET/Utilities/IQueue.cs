using System.Collections.Generic;

namespace Utilities
{
   public interface IQueue
   {
      /// <summary>
      /// Adds an object to the end of the Queue for furthure processing
      /// </summary>
      /// <param name="item">object to be added</param>
      /// <returns>true if the object has been added, false otherwise</returns>
      bool Enqueue(object item);

      bool EnqueueRange(IEnumerable<object> elements);

      /// <summary>
      /// Queue of the added objects
      /// </summary>
      IEnumerable<object> Queue { get; }
   }
}