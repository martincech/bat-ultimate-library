﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Utilities
{
   public class DefaultAppConfigParser
   {
      protected readonly Dictionary<string, string> DefaultTagValues;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public DefaultAppConfigParser(Dictionary<string, string> defaultTagValues)
      {
         DefaultTagValues = defaultTagValues;
      }

      protected long NumberFromAppSettings(string tag)
      {
         var str = FromAppSettingsOrDefault(tag);
         long id;
         if (long.TryParse(str, out id))
         {
            return id;
         }
         throw new ArrayTypeMismatchException();
      }

      protected string FromAppSettingsOrDefault(string appConfigTag)
      {
         string tagValue;
         if (ConfigurationManager.AppSettings.AllKeys.Contains(appConfigTag))
         {
            tagValue = ConfigurationManager.AppSettings[appConfigTag];
         }
         else
         {
            Logger.Logger.Write("No {0} settings found in App.config, using default", appConfigTag);
            try
            {
               tagValue = DefaultTagValues[appConfigTag];
            }
            catch (Exception)
            {
               throw new InvalidOperationException("Wrong default values specified for app config!");
            }
         }
         return tagValue;
      }
   }
}
