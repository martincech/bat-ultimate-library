using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Utilities
{
   public class ProcessQueue : IProcessQueue, IDisposable
   {
      #region Private fields

      private CancellationTokenSource cts;
      private readonly Dictionary<Type, List<Func<object, bool>>> actionsToExecuteForType;
      private readonly Dictionary<object, Func<object, bool>> wrappedActions;
      private BlockingCollection<object> collection;
      private readonly TaskScheduler scheduler;

      #endregion

      #region Constructors

      public ProcessQueue()
         : this(new Dictionary<Type, List<Func<object, bool>>>())
      {
      }

      public ProcessQueue(Dictionary<Type, List<Func<object, bool>>> actions)
         : this(actions, TaskScheduler.Default)
      {
      }

      public ProcessQueue(Dictionary<Type, List<Func<object, bool>>> actions, TaskScheduler scheduler)
      {
         actionsToExecuteForType = new Dictionary<Type, List<Func<object, bool>>>();
         wrappedActions = new Dictionary<object, Func<object, bool>>();
         if (actions != null)
         {
            actions.ForEach(v =>
            {
               if (v.Value == null)
               {
                  throw new ArgumentException(string.Format("Action is not validly registered for type {0}", v.Key));
               }
            });
            foreach (var action in actions)
            {
               foreach (var func in action.Value)
               {
                  AddProcessing(action.Key, func);
               }
            }
         }

         this.scheduler = scheduler;
         collection = new BlockingCollection<object>();
         InitDigest();
      }

      #endregion

      public IEnumerable<object> Queue
      {
         get { return collection.ToList(); }
      }

      public bool Enqueue(object element)
      {
         if (element == null)
         {
            return false;
         }
         var servableType = actionsToExecuteForType.Keys.FirstOrDefault(key => key.IsInstanceOfType(element));
         if (servableType == null)
         {
            return false;
         }
         if (actionsToExecuteForType[servableType].Any())
         {
            return collection.TryAdd(element);
         }
         return true;
      }

      public bool EnqueueRange(IEnumerable<object> elements)
      {
         if (elements == null)
         {
            return true;
         }
         foreach (var element in elements)
         {
            Enqueue(element);
         }
         return true;
      }

      public bool AddProcessing<T>(Func<T, bool> action)
      {
         return AddProcessing(typeof(T), action);
      }

      public bool AddProcessing<T>(Type type, Func<T, bool> action)
      {
         lock (actionsToExecuteForType)
         {
            if (action == null || type == null || wrappedActions.ContainsKey(action))
            {
               return false;
            }
            if (!actionsToExecuteForType.ContainsKey(type))
            {
               actionsToExecuteForType.Add(type, new List<Func<object, bool>>());
            }
            actionsToExecuteForType[type].Add(WrapAction(action));
            return true;
         }
      }

      public void RemoveObjectsOfTypeFromQueue<T>()
      {
         Dispose();
         var otherTypes = collection.Where(i => !(i is T)).ToList();

         //other objects except removing type will be return to collection
         collection = new BlockingCollection<object>();
         foreach (var item in otherTypes)
         {
            collection.TryAdd(item);
         }
         InitDigest();
      }

      private void InitDigest()
      {
         cts = new CancellationTokenSource();
         Task.Factory.StartNew(StartDigest, cts.Token, TaskCreationOptions.LongRunning, scheduler);
      }


      public bool RemoveProcessing<T>()
      {
         return RemoveProcessing(typeof(T));
      }

      public bool RemoveProcessing(Type type)
      {
         return RemoveRegisteredAction(type);
      }

      private bool RemoveRegisteredAction(Type type, IEnumerable<Func<object, bool>> func = null)
      {
         lock (actionsToExecuteForType)
         {
            if (type == null || !actionsToExecuteForType.ContainsKey(type))
            {
               return false;
            }
            if (func == null)
            {
               func = new List<Func<object, bool>>(actionsToExecuteForType[type]);
            }
            foreach (var wrapperFunc in func)
            {
               wrappedActions.Remove(wrappedActions.First(k => k.Value == wrapperFunc).Key);
               actionsToExecuteForType[type].Remove(wrapperFunc);
            }

            //if (!actionsToExecuteForType[type].Any())
            //{
            //   actionsToExecuteForType.Remove(type);
            //}
            return true;
         }
      }


      public bool RemoveProcessing<T>(Func<T, bool> action)
      {
         return RemoveProcessing(typeof(T), action);
      }

      public bool RemoveProcessing<T>(Type type, Func<T, bool> action)
      {
         if (!wrappedActions.ContainsKey(action))
         {
            return false;
         }
         var wrapperForAction = wrappedActions[action];
         return RemoveRegisteredAction(type, new List<Func<object, bool>> { wrapperForAction });
      }

      private Func<object, bool> WrapAction<T>(Func<T, bool> action)
      {
         var wrapper = new Func<object, bool>(a =>
         {
            var item = (T)a;
            return item != null && action(item);
         });
         wrappedActions.Add(action, wrapper);
         return wrapper;
      }

      private void StartDigest()
      {
         foreach (var element in collection.GetConsumingEnumerable(cts.Token))
         {
            Digest(element);
         }
      }

      private void Digest(object element)
      {
         lock (actionsToExecuteForType)
         {
            if (element == null)
            {
               return;
            }
            if (!actionsToExecuteForType.Keys.Any(key => key.IsInstanceOfType(element)))
            {
               return;
            }
            {
               try
               {
                  foreach (var action in actionsToExecuteForType[
                     actionsToExecuteForType.Keys.First(key => key.IsInstanceOfType(element))].ToArray())
                  {
                     action(element);
                  }
               }
               catch (Exception e)
               {
                  Logger.Logger.Write(e.Message);
                  throw;
               }
            }
         }
      }

      #region Implementation of IDisposable

      /// <summary>
      ///    Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         if (cts == null) return;
         cts.Cancel();
         cts.Dispose();
         cts = null;
      }

      #endregion
   }
}