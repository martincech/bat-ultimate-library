﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Utilities.Logger
{
   public static class Logger
   {
      private static readonly BlockingCollection<Tuple<DateTime, string>> MESSAGES; 
      static Logger()
      {
         MESSAGES = new BlockingCollection<Tuple<DateTime, string>>();
         Task.Factory.StartNew(LogMessages, TaskCreationOptions.LongRunning);
         Write("=============================");
      }

      private static void LogMessages()
      {
         foreach (var tuple in MESSAGES.GetConsumingEnumerable())
         {
            Log(tuple.Item1, tuple.Item2);
         }
      }

      public static ILogger Instance { get; set; }

      public static void Write(string format, params object[] args)
      {
         Write(string.Format(format, args));
      }
      /// <summary>
      /// Write string to log file.
      /// </summary>

      public static void Write(string message)
      {
#if DEBUG
         message = string.Format("Task: {1}, message: {0}", message, Task.CurrentId);
#endif
         MESSAGES.Add(new Tuple<DateTime, string>(DateTime.Now, message));
      }


      private static void WriteWithoutTime(string message)
      {
         if (Instance != null)
         {
            Instance.Log(message);
         }
         else
         {
            Trace.WriteLine(message);
         }
      }

      private static void Log(DateTime time, string message)
      {        
         if (Instance != null)
         {
               Instance.Log(time, message);
         }
         else
         {
            Trace.WriteLine(time.ToString("hh:mm:ss:fff tt") + " - " + message);
         }
      }
   }
}
