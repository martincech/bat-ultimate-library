﻿using System;
using System.IO;

namespace Utilities.Logger
{
   public class FileLogger : ILogger
   {
      #region Private fields

      private readonly string fileName;
      private readonly RollingFile.RollingFile file;

      #endregion

      #region Constructors

      public FileLogger()
         : this(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log.txt"))
      {
      }

      public FileLogger(string fileName)
      {
         this.fileName = fileName;
         var dir = Path.GetDirectoryName(fileName);
         if (!string.IsNullOrEmpty(dir))
         {
            Directory.CreateDirectory(dir);
         }

         file = new RollingFile.RollingFile { FileName = fileName};
      }

      #endregion

      public bool RollFileLog { get; set; }

      #region Implementation of ILogger

      public void Log(string message)
      {
         Log(DateTime.Now, message);
      }

      public void Log(DateTime time, string message)
      {
         var msg = string.Format("{0}: {1}", time, message);
         if (!RollFileLog)
         {
            ClassicFileLog(msg);
         }
         else
         {
            RollingFileLog(msg);       
         }
      }

      private void ClassicFileLog(string message)
      {
         using (var writer = File.AppendText(fileName))
         {
            writer.WriteLine(message);
         }
      }

      private void RollingFileLog(string message)
      {
         file.LogMessage(message);
      }

      #endregion
   }
}
