﻿using System;

namespace Utilities.Logger
{
   public class ConsolleLogger : ILogger
   {
      #region Implementation of ILogger

      public void Log(string message)
      {
         Log(DateTime.Now, message);
      }

      public void Log(DateTime time, string message)
      {
         Console.WriteLine("{0}: {1}", time, message);
      }

      #endregion
   }
}