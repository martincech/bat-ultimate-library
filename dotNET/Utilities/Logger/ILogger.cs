﻿using System;

namespace Utilities.Logger
{
   public interface ILogger
   {
      void Log(string message);
      void Log(DateTime time, string message);
   }
}