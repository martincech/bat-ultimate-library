﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions
{
   public static class CollectionExtensions
   {
      public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
      {
         if (collection == null)
         {
            throw new ArgumentNullException("collection");
         }
         if (action == null)
         {
            throw new ArgumentNullException("action");
         }
         foreach (var item in collection)
         {
            action(item);
         }
      }

      /// <summary>
      /// Compare collections.
      /// </summary>
      /// <param name="first">collection to check in</param>
      /// <param name="second">collection to be tested whether it is in <see cref="first"/></param>
      /// <returns>true if <see cref="first"/> contains all elements from <see cref="second"/></returns>
      public static bool ContainsCollection<T>(this IEnumerable<T> first, IEnumerable<T> second)
      {

         if (second == null )
         {
            return false;
         }
         return !second.Except(first).Any();
      }

      public static bool SequenceEqualsUnOrdered<T>(this IEnumerable<T> first, IEnumerable<T> second)
      {
         if (first == second) 
         {
            return true;
         }
         if (first == null || second == null) 
         {
            return false;
         }

         var l1 = first.ToLookup(t => t);
         var l2 = second.ToLookup(t => t);
         return l1.Count == l2.Count
             && l1.All(group => l2.Contains(group.Key) && l2[group.Key].Count() == group.Count()); 
      }
   }
}
