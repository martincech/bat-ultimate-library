﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Utilities.Extensions
{
   public static class ObjectExtensions
   {
      /// <summary>
      /// Clones one object into another. Deep cloning is performed.
      /// </summary>
      /// <param name="o">object to be cloned</param>
      /// <returns>new object</returns>
      public static object Clone(this object o)
      {
         return o.Clone<object>();
      }
      public static T Clone<T>(this T o)
      {
         var memoryStream = new MemoryStream();
         var serializer = new NetDataContractSerializer();
         serializer.Serialize(memoryStream, o);

         memoryStream.Position = 0;
         var returnValue = (T)serializer.Deserialize(memoryStream);

         memoryStream.Close();
         return returnValue;
      }

      /// <summary>
      /// Compares two object for equality. Deep compare is performed.
      /// </summary>
      /// <param name="a">First object</param>
      /// <param name="b">Second object</param>
      /// <returns>true/false => equals/non equals</returns>
      public new static bool Equals(this object a, object b)
      {
         //compare types
         if (a == null || b == null || b.GetType() != a.GetType())
         {
            return false;
         }
         //compare references
         if (ReferenceEquals(a, b))
         {
            return true;
         }

         // compare byte by byte
         var serializer = new NetDataContractSerializer();
         var memoryStream = new MemoryStream();

         serializer.Serialize(memoryStream, a);
         var aBytes = memoryStream.ToArray();
         memoryStream = new MemoryStream();
         serializer.Serialize(memoryStream, b);
         var bBytes = memoryStream.ToArray();

         return aBytes.Length == bBytes.Length && aBytes.SequenceEqual(bBytes);
      }

      /// <summary>
      /// Compare all readable properties of two objects using reflection.
      /// With collections check all their items as well.
      /// </summary>
      /// <param name="objectA">First object</param>
      /// <param name="objectB">Second object</param>
      /// <returns>true if all properties have same value</returns>
      public static bool Same(this object objectA, object objectB)
      {
         if (objectA == objectB)
         {
            // no need to compare for same instance
            return true;
         }
         if (objectA == null || objectB == null)
         {
            return false;
         }

         var objectType = objectA.GetType();
         if (objectB.GetType() != objectType)
         {
            return false;
         }
         
         if (CanDirectlyCompare(objectType))
         {
            return AreValuesEqual(objectA, objectB);
         }

         // if it implements IEnumerable, then scan any items
         if (typeof(IEnumerable).IsAssignableFrom(objectType))
         {
            if (!CheckCollections(objectA as IEnumerable, objectB as IEnumerable))
            {
               Logger.Logger.Write(string.Format("Mismatch in collections '{0}' found.", objectType.FullName));
               return false;
            }
         }

         foreach (
            var propertyInfo in
               objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead))
         {
            object valueA = null;
            object valueB = null;
            Exception valueAException = null;
            Exception valueBException = null;

            try
            {
               valueA = propertyInfo.GetValue(objectA, null);
            }
            catch (Exception e)
            {
               valueAException = e;
            }
            try
            {
               valueB = propertyInfo.GetValue(objectB, null);
            }
            catch (Exception e)
            {
               valueBException = e;
            }

            // some exception occured?
            if (valueAException != null || valueBException != null)
            {
               // just single exception - different values 
               if (valueAException == null || valueBException == null)
               {
                  return false;
               }

               if (valueAException.GetType() == valueBException.GetType())
               {
                  continue;
               }
               return false;
            }


            // if it is a primative type, value type or implements IComparable, just directly try and compare the value
            if (CanDirectlyCompare(propertyInfo.PropertyType))
            {
               if (AreValuesEqual(valueA, valueB)) continue;
               Debug.Write(string.Format("Mismatch with property '{0}.{1}' found.", objectType.FullName,
                  propertyInfo.Name));
               return false;
            }

            // if it implements IEnumerable, then scan any items
            if (typeof (IEnumerable).IsAssignableFrom(propertyInfo.PropertyType))
            {
               if (!CheckCollections(valueA as IEnumerable, valueB as IEnumerable))
               {
                  Logger.Logger.Write(string.Format("Mismatch with property '{0}.{1}' found.", objectType.FullName,
                     propertyInfo.Name));
                  return false;
               }
               continue;
            }

            // if it is a class item go recursivelly
            if (propertyInfo.PropertyType.IsClass)
            {
               if (Same(propertyInfo.GetValue(objectA, null), propertyInfo.GetValue(objectB, null))) continue;
               Debug.Write(string.Format("Mismatch with property '{0}.{1}' found.", objectType.FullName,
                  propertyInfo.Name));
               return false;
            }

            // this is a problem with not complete implementation of this 
            Debug.Assert(false,
               string.Format("Cannot compare property '{0}.{1}', this type is not supported by Same method.",
                  objectType.FullName, propertyInfo.Name));
         }
         return true;
      }


      /// <summary>
      /// Determines whether value instances of the specified type can be directly compared.
      /// </summary>
      /// <param name="type">The type.</param>
      /// <returns>
      ///   <c>true</c> if this value instances of the specified type can be directly compared; otherwise, <c>false</c>.
      /// </returns>
      private static bool CanDirectlyCompare(Type type)
      {
         return typeof (IComparable).IsAssignableFrom(type) || type.IsPrimitive || type.IsValueType;
      }

      /// <summary>
      /// Compares two values and returns if they are the same.
      /// </summary>
      /// <param name="valueA">The first value to compare.</param>
      /// <param name="valueB">The second value to compare.</param>
      /// <returns><c>true</c> if both values match, otherwise <c>false</c>.</returns>
      private static bool AreValuesEqual(object valueA, object valueB)
      {
         var selfValueComparer = valueA as IComparable;

         if (valueA == null && valueB != null || valueA != null && valueB == null)
            return false; // one of the values is null
         if (selfValueComparer != null && selfValueComparer.CompareTo(valueB) != 0)
            return false; // the comparison using IComparable failed
         return object.Equals(valueA, valueB);
      }

      /// <summary>
      /// Check collections for Same items and its types
      /// </summary>
      /// <param name="valueA"></param>
      /// <param name="valueB"></param>
      /// <returns></returns>
      private static bool CheckCollections(IEnumerable valueA, IEnumerable valueB)
      {
         if (valueA == null || valueB == null)
         {
            return valueA == null && valueB == null;
         }

         var collectionItems1 = valueA.Cast<object>().ToArray();
         var collectionItems2 = valueB.Cast<object>().ToArray();

         // check the counts to ensure they match
         if (collectionItems1.Length != collectionItems2.Length)
         {
            return false;
         }
         // and if they do, compare each item... this assumes both collections have the same order
         for (var i = 0; i < collectionItems1.Length; i++)
         {
            var collectionItem1 = collectionItems1[i];
            var collectionItem2 = collectionItems2[i];
            if (collectionItem1 == collectionItem2) continue;
            if (collectionItem1 != null && collectionItem1.Same(collectionItem2))
            {
               continue;
            }
            return false;
         }
         return true;
      }

   }
}
