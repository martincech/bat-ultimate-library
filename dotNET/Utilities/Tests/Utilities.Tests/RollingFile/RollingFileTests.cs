﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.RollingFile;

namespace Utilities.Tests.RollingFile
{
   [TestClass]
   public class RollingFileTests
   {
      [TestMethod]
      public void ActualDataRolling_ChangeParameters_FileName()
      {
         var path = "c:/DifferentFromDefaultFolder/MyFile.txt";
         WriteDataToChangedFileName(path, RollingMode.RollActualData);

      }

      [TestMethod]
      public void HistoricDataRolling_ChangeParameters_FileName()
      {
         var path = "c:/DifferentFromDefaultFolder/MyFile.txt";
         WriteDataToChangedFileName(path, RollingMode.RollHistoricdata);
      }


      private void WriteDataToChangedFileName(string name, RollingMode mode)
      {
         var dir = Path.GetDirectoryName(name);
         var fileNameWithoutSuffix = Path.GetFileNameWithoutExtension(name);
         var extension = Path.GetExtension(name);
         const int index = 0;

         Assert.IsNotNull(dir);
         var fullName = Path.Combine(dir, fileNameWithoutSuffix);
         if (mode == RollingMode.RollActualData)
         {
            fullName += index;
         }
         fullName += extension;

         var rf = new Utilities.RollingFile.RollingFile(mode)
         {
            FileName = name
         };

         Assert.IsFalse(File.Exists(fullName));
         rf.LogMessage("Test");
         Assert.IsTrue(File.Exists(fullName));

         if (!Directory.Exists(dir)) return;
         Directory.Delete(dir, true);
      }
   }
}
