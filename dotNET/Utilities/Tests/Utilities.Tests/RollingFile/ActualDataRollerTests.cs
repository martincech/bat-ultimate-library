﻿using System;
using System.Globalization;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.RollingFile;

namespace Utilities.Tests.RollingFile
{
   [TestClass]
   public class ActualDataRollerTests
   {
      #region Private fields

      private const string BASE_PATH = "TestFolder";
      private string path = BASE_PATH;
      private string fileNameWithoutExtension = "MyFile";
      private int fileIndex;
      private string fileExtension = ".txt";

      private Parameters parameters;
      private ActualDataRoller roller;

      #endregion

      [TestInitialize]
      public void Init()
      {
         ClearTemp();
         Directory.CreateDirectory(path);
         parameters = new Parameters
         {
            FileName = Path.Combine(path, fileNameWithoutExtension + fileExtension),
            MaxFileSize = 30,
            MaxRolledFileCount = 3
         };
         roller = new ActualDataRoller(parameters);
      }

      [TestCleanup]
      public void ClearTemp()
      {
         if (Directory.Exists(path))
         {
            Directory.Delete(BASE_PATH, true);
         }
      }



      [TestMethod]
      public void FileNotExist_CreateNewWithIndexZero()
      {
         Assert.IsFalse(IsFileExist());
         roller.LogMessage(DateTime.Now.ToString(CultureInfo.CurrentCulture));
         Assert.IsTrue(IsFileExist());
      }

      [TestMethod]
      public void FileSizeExceed_SwitchToNextIndex()
      {
         FileNotExist_CreateNewWithIndexZero();
         FillCurrentFileToMaximumSize();

         // new file
         var nexIndex = ++fileIndex;
         Assert.IsFalse(IsFileExist(nexIndex));
         roller.LogMessage(DateTime.Now.ToString(CultureInfo.CurrentCulture));
         Assert.IsTrue(IsFileExist(nexIndex));
      }

      [TestMethod]
      public void FillAllFiles_StartNewCycle()
      {
         for (fileIndex = 0; fileIndex < parameters.MaxRolledFileCount; fileIndex++)
         {
            Assert.IsFalse(IsFileExist(fileIndex));
            FillCurrentFileToMaximumSize();
            Assert.IsTrue(IsFileExist(fileIndex));
         }

         Assert.IsTrue(IsFileExist(0));
         const string testString = "Test string";
         roller.LogMessage(testString);

         //check file
         var read = File.ReadAllText(GetFullFileName(0)).TrimEnd();
         Assert.AreEqual(testString, read);
      }

      [TestMethod]
      public void DirectoryToFileNotExist_Create()
      {
         var additionalPath = Path.DirectorySeparatorChar + "TestFolder2" + Path.DirectorySeparatorChar + "TestFolder3";
         path += additionalPath;
         parameters.FileName = Path.Combine(path, fileNameWithoutExtension + fileExtension);

         Assert.IsFalse(IsFileExist());
         roller.LogMessage("test");
         Assert.IsTrue(IsFileExist());
      }

      #region Private helpers

      private void FillCurrentFileToMaximumSize()
      {
         var file = GetFullFileName(fileIndex);
         var fileSizeExceed = false;
         while (!fileSizeExceed)
         {
            roller.LogMessage(DateTime.Now.ToString(CultureInfo.CurrentCulture));

            var fileInfo = new FileInfo(file);
            if (fileInfo.Length >= parameters.MaxFileSize)
            {
               fileSizeExceed = true;
            }
         }
      }

      private string GetFullFileName(int index)
      {
         return Path.Combine(path, fileNameWithoutExtension + index + fileExtension);
      }

      private bool IsFileExist(int index)
      {
         return File.Exists(GetFullFileName(index));
      }

      private bool IsFileExist()
      {
         return File.Exists(GetFullFileName(fileIndex));
      }

      #endregion
   }
}
