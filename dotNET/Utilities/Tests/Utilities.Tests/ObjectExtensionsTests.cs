﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Extensions;

namespace Utilities.Tests
{
   [TestClass]
   public class ObjectExtensionsTests
   {
      [TestMethod]
      public void DifferentTypes_ReturnsFalse()
      {
         var c1 = new C1();
         var c2 = new C2();

         CompareAndExpect(c1, c2, false);
      }

      [TestMethod]
      public void SameObjects_ReturnsTrue()
      {
         var c1 = new C1();
         var c2 = c1;
         CompareAndExpect(c1, c2, true);
      }

      [TestMethod]
      public void CollectionProperties_NotSame_ReturnsFalse()
      {
         
         var c1 = new C1 {P1 = 1, P2 = 3, P3 = new List<int?> {1, null, 3}};
         var c2 = new C1 {P1 = 1, P2 = 3, P3 = new List<int?> {1, 2, 3}};
         CompareAndExpect(c1, c2, false);
      }

      [TestMethod]
      public void CollectionProperties_Same_ReturnsTrue()
      {

         var c1 = new C1 { P1 = 1, P2 = 3, P3 = new List<int?> { 1, null, 3 } };
         var c2 = new C1 { P1 = 1, P2 = 3, P3 = new List<int?> { 1, null, 3 } };
         CompareAndExpect(c1, c2, true);
      }

      [TestMethod]
      public void CollectionsOfCollectionsProperties_Same_ReturnsTrue()
      {
         var c1 = new C1 {P1 = 1, P2 = 3, P3 = new List<int?> {1, null, 3}};
         c1.P4 = new List<IEnumerable<int?>> {c1.P3, new List<int?> {2, 3, 4}};
         var c2 = new C1 {P1 = 1, P2 = 3, P3 = new List<int?> {1, null, 3}};
         c2.P4 = new List<IEnumerable<int?>> {c2.P3, new List<int?> {2, 3, 4}};
         CompareAndExpect(c1, c2, true);
      }

      [TestMethod]
      public void CollectionsOfCollectionsProperties_NotSame_ReturnsFalse()
      {
         var c1 = new C1 { P1 = 1, P2 = 3, P3 = new List<int?> { 1, null, 3 } };
         c1.P4 = new List<IEnumerable<int?>> { c1.P3, new List<int?> { 2, 3, 4 } };
         var c2 = new C1 { P1 = 1, P2 = 3, P3 = new List<int?> { 1, null, 3 } };
         c2.P4 = new List<IEnumerable<int?>> { c2.P3, new List<int?> { 2, 3, 5 } };
         CompareAndExpect(c1, c2, false);
      }

      [TestMethod]
      public void Collections_Same_ReturnsTrue()
      {
         var c1 = new List<int?> {1, null, 3};
         var c2 = new List<int?> {1, null, 3};
         CompareAndExpect(c1, c2, true);
      }

      [TestMethod]
      public void Collections_NotSame_ReturnsFalse()
      {
         var c1 = new List<int?> { 1, null, 3 };
         var c2 = new List<int?> { 1, null, 4 };
         CompareAndExpect(c1, c2, false);
      }

      [TestMethod]
      public void CollectionsDiferentTypes_ButSame_ReturnsFalse()
      {
         var c1 = new List<int?> { 1, null, 3 };
         var c2 = new int?[] { 1, null, 3 };
         CompareAndExpect(c1, c2, false);
      }


      /// <summary>
      /// Compare 2 objects(in both ways, i.e. c1 to c2 and c2 to c1) and expect this comparison to be true/false
      /// </summary>
      /// <param name="c1">object 1 to compare</param>
      /// <param name="c2">object 2 to compare</param>
      /// <param name="expect">expected result o comparison</param>
      private static void CompareAndExpect(object c1, object c2, bool expect)
      {
         if (expect)
         {
            Assert.IsTrue(c1.Same(c2));
            Assert.IsTrue(c2.Same(c1));
         }
         else
         {
            Assert.IsFalse(c1.Same(c2));
            Assert.IsFalse(c2.Same(c1));
         }
      }

      public class C2
      {
         public int P1 { get; set; }
      }

      public class C1
      {
         public int? P1 { get; set; }
         public int? P2 { get; set; }
         public IEnumerable<int?> P3 { get; set; }
         public IEnumerable<IEnumerable<int?>> P4 { get; set; }
      }
   };
}
