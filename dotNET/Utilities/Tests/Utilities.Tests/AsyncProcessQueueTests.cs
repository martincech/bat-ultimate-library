﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Utilities.Tests
{
   [TestClass]
   public class AsyncProcessQueueTests
   {
      private ProcessQueue uut;
      private DeterministicTaskScheduler scheduler;
      private BlockingCollection<object> privateBlockingCollection;

      [TestInitialize]
      public void Init()
      {
         scheduler = new DeterministicTaskScheduler();
         uut = new ProcessQueue(null, scheduler);
         var po = new PrivateObject(uut);
         privateBlockingCollection = po.GetField("collection") as BlockingCollection<object>;
         Assert.IsNotNull(privateBlockingCollection,
            "Blocking collection in ActionQueue changed and tests do not reflect this");
      }


      private void DoAsyncProcessing()
      {
         privateBlockingCollection.CompleteAdding();
         scheduler.RunPendingTasks();
      }

      [TestMethod]
      public void Action_InvokedAfterRegistration()
      {
         const int value = 5;
         var raised = false;
         uut.AddProcessing<int>(i =>
         {
            raised = true;
            Assert.AreEqual(value, i);
            return true;
         });
         uut.Enqueue(value);
         DoAsyncProcessing();
         Assert.IsTrue(raised);
      }


      [TestMethod]
      public void Process_Asynchronously()
      {
         const int value = 5;
         var raised = false;
         uut.AddProcessing<int>(i =>
         {
            raised = true;
            Assert.AreEqual(value, i);
            return true;
         });
         uut.Enqueue(value);
         Assert.IsFalse(raised);
         DoAsyncProcessing();
         Assert.IsTrue(raised);
      }

      [TestMethod]
      public void Process_MultipleInvocationWithTheSameData()
      {
         const int value = 5;
         var raisedTimes = 0;
         uut.AddProcessing<int>(i =>
         {
            raisedTimes++;
            Assert.AreEqual(value, i);
            return true;
         });
         for (var i = 0; i < value; i++)
         {
            uut.Enqueue(value);
         }

         DoAsyncProcessing();
         Assert.AreEqual(value, raisedTimes);
      }

      [TestMethod]
      public void Proces_MultipleActions()
      {
         const int value = 5;
         var raisedFirst = false;
         var raisedSecond = false;
         uut.AddProcessing<int>(i =>
         {
            raisedFirst = true;
            return true;
         });
         uut.AddProcessing<int>(i =>
         {
            raisedSecond = true;
            return true;
         });
         uut.Enqueue(value);
         DoAsyncProcessing();
         Assert.IsTrue(raisedFirst);
         Assert.IsTrue(raisedSecond);
      }

      [TestMethod]
      public void Process_DifferentDataTypes()
      {
         var intProcessed = false;
         var doubleProcessed = false;
         var stringProcessed = false;
         var uutProcessed = false;
         var values = new object[]
         {
            5,
            5.3,
            "string",
            uut
         };

         uut.AddProcessing<int>(i =>
         {
            intProcessed = true;
            Assert.AreEqual(values[0], i);
            return true;
         });
         uut.AddProcessing<double>(i =>
         {
            doubleProcessed = true;
            Assert.AreEqual(values[1], i);
            return true;
         });
         uut.AddProcessing<string>(i =>
         {
            stringProcessed = true;
            Assert.AreEqual(values[2], i);
            return true;
         });
         uut.AddProcessing<ProcessQueue>(i =>
         {
            uutProcessed = true;
            Assert.AreEqual(values[3], i);
            return true;

         });


         uut.EnqueueRange(values);
         DoAsyncProcessing();

         Assert.IsTrue(intProcessed);
         Assert.IsTrue(doubleProcessed);
         Assert.IsTrue(stringProcessed);
         Assert.IsTrue(uutProcessed);
      }

      [TestMethod]
      public void NullActionCantRegister()
      {
         Assert.IsFalse(uut.AddProcessing<int>(null));
         Assert.IsFalse(uut.AddProcessing<double>(typeof (int), null));
      }

      [TestMethod]
      public void SameAction_CanBeRegisteredOnlyOnce()
      {
         var action = new Func<int, bool>(i => true);
         Assert.IsTrue(uut.AddProcessing(action));
         Assert.IsFalse(uut.AddProcessing(action));
      }


      [TestMethod]
      public void DisposeStopsProcessing()
      {
         var intProcessed = false;
         uut.AddProcessing<int>(i =>
         {
            intProcessed = true;
            return true;
         });
         uut.Enqueue(5);
         privateBlockingCollection.CompleteAdding();
         using (uut)
         {
         }
         scheduler.RunPendingTasks();
         Assert.IsFalse(intProcessed);

      }

      [TestMethod]
      [ExpectedException(typeof (ArgumentException))]
      public void InvalidConstructorCall_ThrowArgumentException_ForInvalidAction()
      {
         uut = new ProcessQueue(new Dictionary<Type, List<Func<object, bool>>>
         {
            {typeof (int), null}
         });
      }

      [TestMethod]
      public void ActionTypeDeregistered()
      {
         var intProcessed = false;
         var action = new Func<int, bool>(i =>
         {
            intProcessed = true;
            return true;
         });
         uut.AddProcessing(action);
         uut.RemoveProcessing(typeof(int));
         DoAsyncProcessing();
         Assert.IsFalse(intProcessed);
      }

      [TestMethod]
      public void ActionTypeDeregistered_ForMultipleRegistrations()
      {
         var intProcessed = false;
         var action = new Func<int, bool>(i =>
         {
            intProcessed = true;
            return true;
         });
         var action2 = new Func<int, bool>(i =>
         {
            intProcessed = true;
            return false;
         });
         uut.AddProcessing(action);
         uut.AddProcessing(action2);
         uut.RemoveProcessing<int>();
         DoAsyncProcessing();
         Assert.IsFalse(intProcessed);
      }

      [TestMethod]
      public void ActionDeregistered()
      {
         const int value = 5;
         var raisedFirst = false;
         var raisedSecond = false;
         Func<int, bool> action1 = i =>
         {
            raisedFirst = true;
            return true;
         };
         Func<int, bool> action2 = i =>
         {
            raisedSecond = true;
            return true;
         };

         uut.AddProcessing(action1);
         uut.AddProcessing(action2);
         uut.Enqueue(value);
         uut.RemoveProcessing(action2);
         DoAsyncProcessing();
         Assert.IsTrue(raisedFirst);
         Assert.IsFalse(raisedSecond);
      }

      [TestMethod]
      public void QueueContainsMyItem()
      {
         const int o = 5;
         var action = new Func<int, bool>(i => true);
         uut.AddProcessing(action);
         uut.Enqueue(o);
         Assert.IsTrue(uut.Queue.Contains(o));
      }

      [TestMethod]
      public void QueueDoesNotEnqueueItem_WhichDoesNotHaveProcessing()
      {
         const int intValue = 5;
         const double doubleValue = 1.5;
         var action = new Func<int, bool>(i => true);

         uut.AddProcessing(action);
         Assert.IsTrue(uut.Enqueue(intValue));
         Assert.IsFalse(uut.Enqueue(doubleValue));
         Assert.IsTrue(uut.Queue.Contains(intValue));
         Assert.IsFalse(uut.Queue.Contains(doubleValue));
      }

      [TestMethod]
      public void QueueDoesEnqueueItem_Which_DoesEverHadProcessing()
      {
         const int intValue = 5;
         var action = new Func<int, bool>(i => true);

         uut.AddProcessing(action);
         uut.RemoveProcessing(action);
         Assert.IsTrue(uut.Enqueue(intValue));
         Assert.IsFalse(uut.Queue.Contains(intValue));
      }
   }
}
