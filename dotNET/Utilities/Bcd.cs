﻿using System;

namespace Utilities
{
   /// <summary>
   /// Converting BCD numbers
   /// </summary>
   public static class Bcd
   {
      /// <summary>
      /// Convert BCD stored in integer to integer number (0x1234 -> 1234)
      /// </summary>
      /// <param name="bcd">BCD</param>
      /// <returns>Binary value</returns>
      public static int ToInt32(int bcd)
      {
         // Prevedu 4 bajty na max. 8 znaku a ty prevedu zpet na integer
         return Convert.ToInt32(bcd.ToString("X8"));
      }
   }
}
