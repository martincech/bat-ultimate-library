﻿using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;

namespace Utilities.Process
{
   public class ProcessUtility
   {
      private const int MAX_INSTALL_DELAY = 1000 * 60 * 2;

      [Flags]
      public enum AccessTokenRights : uint
      {
         STANDARD_RIGHTS_REQUIRED = 0x000F0000,
         STANDARD_RIGHTS_READ = 0x00020000,
         TOKEN_ASSIGN_PRIMARY = 0x0001,
         TOKEN_DUPLICATE = 0x0002,
         TOKEN_IMPERSONATE = 0x0004,
         TOKEN_QUERY = 0x0008,
         TOKEN_QUERY_SOURCE = 0x0010,
         TOKEN_ADJUST_PRIVILEGES = 0x0020,
         TOKEN_ADJUST_GROUPS = 0x0040,
         TOKEN_ADJUST_DEFAULT = 0x0080,
         TOKEN_ADJUST_SESSIONID = 0x0100,
         TOKEN_READ = (STANDARD_RIGHTS_READ | TOKEN_QUERY),
         TOKEN_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | TOKEN_ASSIGN_PRIMARY |
                                     TOKEN_DUPLICATE | TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_QUERY_SOURCE |
                                     TOKEN_ADJUST_PRIVILEGES | TOKEN_ADJUST_GROUPS | TOKEN_ADJUST_DEFAULT |
                                     TOKEN_ADJUST_SESSIONID)
      }

      [StructLayout(LayoutKind.Sequential)]
      public struct STARTUPINFO
      {
         public Int32 cb;
         public string lpReserved;
         public string lpDesktop;
         public string lpTitle;
         public Int32 dwX;
         public Int32 dwY;
         public Int32 dwXSize;
         public Int32 dwXCountChars;
         public Int32 dwYCountChars;
         public Int32 dwFillAttribute;
         public Int32 dwFlags;
         public Int16 wShowWindow;
         public Int16 cbReserved2;
         public IntPtr lpReserved2;
         public IntPtr hStdInput;
         public IntPtr hStdOutput;
         public IntPtr hStdError;
      }

      [StructLayout(LayoutKind.Sequential)]
      public struct PROCESS_INFORMATION
      {
         public IntPtr hProcess;
         public IntPtr hThread;
         public Int32 dwProcessID;
         public Int32 dwThreadID;
      }

      [StructLayout(LayoutKind.Sequential)]
      public struct SECURITY_ATTRIBUTES
      {
         public Int32 Length;
         public IntPtr lpSecurityDescriptor;
         public bool bInheritHandle;
      }

      public enum SECURITY_IMPERSONATION_LEVEL
      {
         SecurityAnonymous,
         SecurityIdentification,
         SecurityImpersonation,
         SecurityDelegation
      }


      public enum LogonFlags
      {
         WithProfile = 1,
         NetCredentialsOnly
      }

      public enum CreationFlags
      {
         DefaultErrorMode = 0x04000000,
         NewConsole = 0x00000010,
         NewProcessGroup = 0x00000200,
         SeparateWOWVDM = 0x00000800,
         Suspended = 0x00000004,
         UnicodeEnvironment = 0x00000400,
         ExtendedStartupInfoPresent = 0x00080000
      }

      [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
      [SuppressUnmanagedCodeSecurity]
      public static extern bool CloseHandle(
         IntPtr handle
      );

      [DllImport("advapi32.dll", SetLastError = true)]
      [SuppressUnmanagedCodeSecurity]
      public static extern bool DuplicateTokenEx(
         SafeHandle hExistingToken,
         Int32 dwDesiredAccess,
         ref SECURITY_ATTRIBUTES lpThreadAttributes,
         Int32 ImpersonationLevel,
         Int32 dwTokenType,
         out SafeFileHandle phNewToken
      );


      [DllImport("advapi32.dll", SetLastError = true)]
      [SuppressUnmanagedCodeSecurity]
      [return: MarshalAs(UnmanagedType.Bool)]
      public static extern bool OpenProcessToken(
         IntPtr ProcessHandle,
         UInt32 DesiredAccess,
         out SafeFileHandle TokenHandle
      );


      [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode, EntryPoint = "CreateProcessAsUserW", CallingConvention = CallingConvention.StdCall)]
      [SuppressUnmanagedCodeSecurity]
      public static extern bool CreateProcessAsUser(
          SafeHandle hToken,
          string lpApplicationName,
          string lpCommandLine,
          ref SECURITY_ATTRIBUTES lpProcessAttributes,
          ref SECURITY_ATTRIBUTES lpThreadAttributes,
          bool bInheritHandles,
          uint dwCreationFlags,
          IntPtr lpEnvironment,
          string lpCurrentDirectory,
          ref STARTUPINFO lpStartupInfo,
          out PROCESS_INFORMATION lpProcessInformation
      );



      public System.Diagnostics.Process GetSingleProcess(string name)
      {
         try
         {
            var procesess = System.Diagnostics.Process.GetProcessesByName(name);
            var count = procesess.Length;
            if (count == 0)
            {
               return null;
            }
            else if (count > 1)
            {
               //More procesess with the same name are running
               return null;
            }
            else
            {
               return procesess[0];
            }
         }
         catch (Exception)
         {
            return null;
         }
      }

      public bool StopProcess(string name)
      {
         var proc = GetSingleProcess(name);
         if (proc == null)
         {
            return false;
         }
         else
         {
            proc.Kill();
            proc.WaitForExit(MAX_INSTALL_DELAY);
            proc.Dispose();
         }

         return GetSingleProcess(name) == null;
      }

      public void LaunchApp(string fileName, string arguments = "", ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal)
      {
         var startInfo = new ProcessStartInfo
         {
            FileName = fileName,
            Arguments = arguments,
            WindowStyle = windowStyle
         };

         using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
         {
            exeProcess.WaitForExit(MAX_INSTALL_DELAY);
         }
      }

      public void InstallMsi(string fullPath, bool quietMode = false)
      {
         var mode = "";
         if (quietMode)
         {
            mode = "/quiet";
         }

         LaunchApp("msiexec", $"{mode} /i \"{fullPath}\"");
         //install with log information
         //LaunchApp("msiexec", $"{mode} /i \"{fullPath}\" /L*v \"c:\\temp\\install.txt\"");
      }

      public void UninstallMsi(string guid)
      {
         LaunchApp("msiexec", $"/x {guid} /qn");
      }

      public InstallApplicationProperties GetInstallApplicationProperties(string msiName)
      {
         InstallApplicationProperties uninstallString = null;
         try
         {
            var path = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products";

            //works on 32 and 64 bit OS
            var key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64).OpenSubKey(path);

            foreach (string tempKeyName in key.GetSubKeyNames())
            {
               var tempKey = key.OpenSubKey(tempKeyName + "\\InstallProperties");
               if (tempKey != null)
               {
                  if (string.Equals(Convert.ToString(tempKey.GetValue("DisplayName")), msiName, StringComparison.CurrentCultureIgnoreCase))
                  {
                     var uninstallStr = Convert.ToString(tempKey.GetValue("UninstallString"));
                     uninstallStr = uninstallStr.Replace("/I", "");
                     uninstallStr = uninstallStr.Replace("MsiExec.exe", "").Trim();

                     uninstallString = new InstallApplicationProperties
                     {
                        DisplayName = msiName,
                        DisplayVersion = Convert.ToString(tempKey.GetValue("DisplayVersion")),
                        InstallLocation = Convert.ToString(tempKey.GetValue("InstallLocation")),
                        UninstallString = uninstallStr
                     };
                     break;
                  }
               }
            }
            return uninstallString;
         }
         catch (Exception ex)
         {
            throw new ApplicationException(ex.Message);
         }
      }
   }
}
