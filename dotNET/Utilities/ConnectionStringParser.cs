﻿using System.Text.RegularExpressions;

namespace Utilities
{
   public class ConnectionStringParser
   {
      public ConnectionStringParser(string connectionString)
      {
         InitialCatalog = "";
         DataSource = "";
         ParseConnectionString(connectionString);
      }

      public string InitialCatalog { get; private set; }
      public string DataSource { get; private set; }

      private void ParseConnectionString(string connectionString)
      {
         if (connectionString == null) return;
         var regex = new Regex("data source=(?<dataSource>.+?);.*initial catalog=(?<initialCatalog>.+?);");
         var match = regex.Match(connectionString);

         InitialCatalog = match.Groups["initialCatalog"].Value;
         DataSource = match.Groups["dataSource"].Value;
      }
   }
}
