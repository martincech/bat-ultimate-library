﻿using System.Runtime.CompilerServices;

namespace Utilities.Modules
{
   /// <summary>
   /// Interface for a module controller which is responsible for the module lifecycle.
   /// </summary>
   public interface IModuleController
   {
      /// <summary>
      /// Status of module
      /// </summary>
      bool IsRunning { get; }

      /// <summary>
      /// Get the description of the module.
      /// </summary>
      string Description { get; }

      /// <summary>
      /// Run the module, module can be created and not running,
      /// this method runs it.
      /// </summary>
      [MethodImpl(MethodImplOptions.Synchronized)]
      void Run();

      /// <summary>
      /// Shutdown the controller, module can be created and not running,
      /// this method stops it.
      /// </summary>
      [MethodImpl(MethodImplOptions.Synchronized)]
      void Shutdown();
   }  
}
