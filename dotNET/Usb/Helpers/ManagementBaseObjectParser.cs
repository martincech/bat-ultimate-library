﻿using System;
using System.Management;
using System.Text.RegularExpressions;

namespace Usb.Helpers
{
   public class ManagementBaseObjectParser
   {
      public static string GetPort(ManagementBaseObject mbo)
      {
         var name = mbo["Name"].ToString();
         return GetPort(name);
      }

      private static string GetPort(string name)
      {
         try
         {
            var regex = new Regex(@"[(](.*)[)]");
            var match = regex.Match(name);
            return match.Groups[1].ToString();
         }
         catch (Exception)
         {
            return "";
         }
      }
   }
}
