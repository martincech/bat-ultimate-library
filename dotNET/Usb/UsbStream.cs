﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Usb.SerialPortAdapters;
using Utilities;

#pragma warning disable 420

namespace Usb
{
   public abstract class UsbStream : UartStream
   {
      protected internal class CommonOutput
      {
         public byte[] Bytes;             // data to be send 
         // ReSharper disable once InconsistentNaming
         public bool DoneOK;              // if send was OK (inside the WriteThread)
         public volatile bool Done;       // if data was send (inside the WriteThread
         public object DataDescription;    // some aditional description for write thread to check
      }
      protected internal class CommonInput
      {
         public byte[] Bytes;             // readed data
         public object DataDescription;    // some aditional description of thad data
      }


      #region Private and protected fields

      private int opened, closed;
      private volatile int refCount;
      private int inputQueueFirstItemOffset;      
      private readonly Thread readThreadObject;
      private readonly Thread writeThreadObject;
      private readonly CancellationTokenSource cancellation;
      protected readonly List<CommonInput> InputQueue;
      protected readonly Queue<CommonOutput> OutputQueue;
      /// <summary>
      /// This method is supposed to be a read thread specific from concrete device.
      /// It should fill the <see cref="InputQueue"/> with data when some are available.
      /// <see cref="InputQueue"/>  has to be validly synchronized (as other thread read from it)
      /// </summary>
      /// <param name="cancellationToken">when thread sould be stopped, cancel is requested</param>
      protected abstract void ReadThread(CancellationToken cancellationToken);
      /// <summary>
      /// This method is supposed to be a write thread specific to concrete device.
      /// It should take the data from <see cref="OutputQueue"/> if some available and send them through device.
      /// <see cref="OutputQueue"/>  has to be validly synchronized (as other thread read from it)
      /// </summary>
      /// <param name="cancellationToken">when thread sould be stopped, cancel is requested</param>
      protected abstract void WriteThread(CancellationToken cancellationToken);
      #endregion

      internal UsbStream()
      {
         InputQueue = new List<CommonInput>();
         OutputQueue = new Queue<CommonOutput>();
         readThreadObject = new Thread(o => ReadThread((CancellationToken) o)) {IsBackground = true};
         writeThreadObject = new Thread(o => WriteThread((CancellationToken) o)) {IsBackground = true};
         cancellation = new CancellationTokenSource();
         ReadTimeout = 3000;
         WriteTimeout = 3000;
      }

      protected override sealed void Dispose(bool disposing)
      {
         base.Dispose(disposing);
         if (!HandleClose())
         {
            return;
         }

         cancellation.Cancel();
         try
         {
            lock (InputQueue)
            {
               Monitor.PulseAll(InputQueue);
            }
         }
         catch
         {
         }
         try
         {
            lock (OutputQueue)
            {
               Monitor.PulseAll(OutputQueue);
            }
         }
         catch
         {
         }

         try
         {
            readThreadObject.Join();
         }
         catch
         {
         }
         try
         {
            writeThreadObject.Join();
         }
         catch
         {
         }

         HandleRelease();
      }

      private static int GetTimeout(int startTime, int timeout)
      {
         return Math.Min(timeout, Math.Max(0, startTime + timeout - Environment.TickCount));
      }

      protected int CommonRead(byte[] buffer, int offset, int count, object dataDescription = null)
      {
         Throw.If.OutOfRange(buffer, offset, count);
         if (count == 0)
         {
            return 0;
         }

         var readTimeout = ReadTimeout;
         var startTime = Environment.TickCount;
         var returnLen = 0;

         HandleAcquireIfOpenOrFail();
         try
         {
            lock (InputQueue)
            {
               while (true)
               {
                  Func<CommonInput, bool> condition;
                  if (dataDescription != null)
                  {
                     condition = p => p.DataDescription.Equals(dataDescription);
                  }
                  else
                  {
                     condition = p=>true;
                  }
                  if (InputQueue.Count > 0 && InputQueue.Any(condition))
                  {
                     var packet = InputQueue.First(condition);
                     var newOfset = 0;
                     var qDataLen = packet.Bytes.Length - inputQueueFirstItemOffset;
                     if (count < qDataLen)
                     {
                        newOfset = inputQueueFirstItemOffset + count;
                        qDataLen = Math.Min(count, qDataLen);
                     }
                     else
                     {
                        InputQueue.Remove(packet);
                     }
                     Array.Copy(packet.Bytes, inputQueueFirstItemOffset, buffer, offset + returnLen, qDataLen);
                     count -= qDataLen;
                     returnLen += qDataLen;
                     inputQueueFirstItemOffset = newOfset;
                     if (count > 0)
                     {
                        // can read more data
                        continue;
                     }
                     // data succesfully readed
                     return returnLen;
                  }

                  if (readTimeout == Timeout.Infinite)
                  {
                     Monitor.Wait(InputQueue);
                  }
                  else
                  {
                     var timeout = GetTimeout(startTime, readTimeout);
                     if (!Monitor.Wait(InputQueue, timeout))
                     {
                        return returnLen;
                     }
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      protected void CommonWrite(byte[] buffer, int offset, int count, object dataDescription = null)
      {
         Throw.If.OutOfRange(buffer, offset, count);
         if (count == 0)
         {
            return;
         }
         var writeTimeout = WriteTimeout;
         var startTime = Environment.TickCount;
         HandleAcquireIfOpenOrFail();
         try
         {
            lock (OutputQueue)
            {
               while (true)
               {
                  var packet = new byte[count];
                  Array.Copy(buffer, offset, packet, 0, count);
                  var outputReport = new CommonOutput {Bytes = packet, DataDescription = dataDescription};
                  OutputQueue.Enqueue(outputReport);
                  Monitor.PulseAll(OutputQueue);

                  while (true)
                  {
                     if (outputReport.Done)
                     {
                        if (!outputReport.DoneOK)
                        {
                           throw new IOException();
                        }
                        return;
                     }

                     if (writeTimeout == Timeout.Infinite)
                     {
                        Monitor.Wait(OutputQueue);
                     }
                     else
                     {
                        var timeout = GetTimeout(startTime, writeTimeout);
                        if (!Monitor.Wait(OutputQueue, timeout))
                        {
                           throw new TimeoutException();
                        }
                     }
                     
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      /// <summary>
      /// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
      /// </summary>
      /// <returns>
      /// The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
      /// </returns>
      /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source. </param><param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream. </param><param name="count">The maximum number of bytes to be read from the current stream. </param><exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length. </exception><exception cref="T:System.ArgumentNullException"><paramref name="buffer"/> is null. </exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="offset"/> or <paramref name="count"/> is negative. </exception><exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support reading. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override int Read(byte[] buffer, int offset, int count)
      {
         return CommonRead(buffer, offset, count);
      }

      /// <summary>
      /// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
      /// </summary>
      /// <param name="buffer">An array of bytes. This method copies <paramref name="count"/> bytes from <paramref name="buffer"/> to the current stream. </param><param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin copying bytes to the current stream. </param><param name="count">The number of bytes to be written to the current stream. </param>
      public override void Write(byte[] buffer, int offset, int count)
      {
         CommonWrite(buffer, offset, count);
      }
      /// <inheritdoc />
      public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
      {
         Throw.If.OutOfRange(buffer, offset, count);
         return AsyncResult<int>.BeginOperation(delegate
         {
            Write(buffer, offset, count);
            return 0;
         }, callback, state);
      }

      /// <inheritdoc />
      public override void EndWrite(IAsyncResult asyncResult)
      {
         Throw.If.Null(asyncResult, "asyncResult");
         ((AsyncResult<int>) asyncResult).EndOperation();
      }

      /// <inheritdoc />
      public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
      {
         Throw.If.OutOfRange(buffer, offset, count);
         return AsyncResult<int>.BeginOperation(() => Read(buffer, offset, count), callback, state);
      }

      /// <inheritdoc />
      public override int EndRead(IAsyncResult asyncResult)
      {
         Throw.If.Null(asyncResult, "asyncResult");
         return ((AsyncResult<int>) asyncResult).EndOperation();
      }

      /// <summary>
      /// The maximum amount of time, in milliseconds, to wait for to receive data.
      /// 
      /// The default is 3000 milliseconds.
      /// To disable the timeout, set this to <see cref="Timeout.Infinite"/>.
      /// </summary>
      public override sealed int ReadTimeout { get; set; }

      /// <summary>
      /// The maximum amount of time, in milliseconds, to wait for data write.
      /// 
      /// The default is 3000 milliseconds.
      /// To disable the timeout, set this to <see cref="Timeout.Infinite"/>.
      /// </summary>
      public override sealed int WriteTimeout { get; set; }

      protected void HandleInitAndOpen()
      {
         opened = 1;
         refCount = 1;
         if (!readThreadObject.IsAlive)
         {
            readThreadObject.Start(cancellation.Token);
         }
         if (!writeThreadObject.IsAlive)
         {
            writeThreadObject.Start(cancellation.Token);
         }
      }

      protected bool HandleClose()
      {
         return 0 == Interlocked.CompareExchange(ref closed, 1, 0) && opened != 0;
      }

      protected bool HandleAcquire()
      {
         while (true)
         {
            var count = refCount;
            if (count == 0)
            {
               return false;
            }

            if (count == Interlocked.CompareExchange
               (ref refCount, count + 1, count))
            {
               return true;
            }
         }
      }

      protected void HandleAcquireIfOpenOrFail()
      {
         if (closed != 0 || !HandleAcquire())
         {
            throw new IOException("Device Closed.");
         }
      }

      protected void HandleRelease()
      {
         if (0 != Interlocked.Decrement(ref refCount)) return;

         if (opened != 0)
         {
            HandleFree();
         }
      }

      protected abstract void HandleFree();
   }
}

#pragma warning restore 420