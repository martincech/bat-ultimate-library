﻿using System.Collections.Generic;
using System.Linq;
using Usb.UMS.Platform;

namespace Usb.UMS
{
   public class UmsDeviceLoader
   {
      /// <summary>
      /// Gets a list of connected USB devices.
      /// </summary>
      /// <returns>The device list.</returns>
      public IEnumerable<UmsDevice> GetDevices()
      {
         return UmsSelector.Instance.GetDevices();
      }

      /// <summary>
      /// Gets a list of connected USB devices, filtered by some criteria.
      /// </summary>
      /// <param name="filePath">Base path to USB mass storage, ie. mounting point or volume label</param>
      /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
      /// <param name="productId">The product ID, or null to not filter by product ID.</param>
      /// <param name="productVersion">The product version, or null to not filter by product version.</param>
      /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
      /// <returns>The filtered device list.</returns>
      public IEnumerable<UmsDevice> GetDevices
          (string filePath = "", int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
      {
         int vid = vendorId ?? -1, pid = productId ?? -1, ver = productVersion ?? -1;
         return GetDevices().Where(ums => (string.IsNullOrEmpty(filePath) || ums.BasePaths.Any(p => p.Equals(filePath))) &&
                                          (ums.VendorID == vendorId || vid < 0) &&
                                          (ums.ProductID == productId || pid < 0) &&
                                          (ums.ProductVersion == productVersion || ver < 0) &&
                                          (ums.SerialNumber == serialNumber || string.IsNullOrEmpty(serialNumber)));
      }

      /// <summary>
      /// Gets the first connected USB device that matches specified criteria.
      /// </summary>
      /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
      /// <param name="productId">The product ID, or null to not filter by product ID.</param>
      /// <param name="productVersion">The product version, or null to not filter by product version.</param>
      /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
      /// <returns>The device, or null if none was found.</returns>
      public UmsDevice GetDeviceOrDefault
          (string filePath = "", int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
      {
         return GetDevices(filePath, vendorId, productId, productVersion, serialNumber).FirstOrDefault();
      }

   }
}
