﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using Usb.Helpers;
using Usb.Platform.Windows;

namespace Usb.UMS.Platform.Windows
{
   internal class WinUmsManager : WinManager<UmsDevice>
   {
      private struct VidPid
      {
         public int vid;
         public int pid;
      }
      protected override object[] Refresh()
      {
         try
         {
            var deviceIds = UsbStorageDeviceIds();
            return deviceIds.Cast<object>().ToArray();
         }
         catch (Exception)
         {
            return new object[]{};
         }
      }

      protected override bool TryCreateDevice(object key, out UmsDevice device, out object creationState)
      {
         try
         {
            Debug.Assert(key is KeyValuePair<string, VidPid>, "Invalid use of WinUmsManager");
            var deviceId = ((KeyValuePair<string, VidPid>) key).Key;
            var vidPid = ((KeyValuePair<string, VidPid>) key).Value;
            ManagementObject management;
            var volumes = GetDriveLetters(deviceId, out management);
            var umsDevice = new WinUmsDevice(volumes);
            umsDevice.GetInfo(management, vidPid.vid, vidPid.pid);
            device = umsDevice;
            creationState = null;
            return true;
         }
         catch (Exception )
         {
            device = null;
            creationState = null;
            return false;
         }
      }

      protected override void CompleteDevice(object key, UmsDevice device, object creationState)
      {
      }

      private static IEnumerable<KeyValuePair<string, VidPid>> UsbStorageDeviceIds()
      {
         var usbObjects = new Dictionary<string, VidPid>();
         foreach (
            var managementObject in
               new ManagementObjectSearcher("SELECT * from Win32_USBHub Where DeviceID Like '%VID_%&PID_%'").Get()
                  .Cast<ManagementObject>()
                  .Where(entity => entity != null))
         {
            var usbHub = managementObject["DeviceID"].ToString();
            foreach (var controller in managementObject.GetRelated("Win32_USBController"))
            {
               foreach (
                  var obj in
                     new ManagementObjectSearcher("ASSOCIATORS OF {Win32_USBController.DeviceID='" +
                                                  controller["PNPDeviceID"] + "'}").Get())
               {
                  if (!obj.ToString().Contains("DeviceID") || !obj["DeviceID"].ToString().Contains("USBSTOR")
                     || usbObjects.ContainsKey(obj["DeviceID"].ToString())) continue;

                  var v = VidPidParser.GetVid(usbHub);
                  var p = VidPidParser.GetPid(usbHub);
                  usbObjects.Add(obj["DeviceID"].ToString(), new VidPid { vid = v, pid = p });
               }
            }
         }

         return usbObjects.Distinct();
      }

      private static IEnumerable<char> GetDriveLetters(string deviceId, out ManagementObject driveDescription)
      {
         var driveLetters = new List<char>();
         driveDescription = null;
         var drive =
            new ManagementObjectSearcher("SELECT * from Win32_DiskDrive").Get()
               .Cast<ManagementObject>()
               .FirstOrDefault(entity => entity != null && entity["PNPDeviceID"].ToString() == deviceId);
         if (drive == null) return driveLetters;

         foreach (
            var o in
               drive.GetRelated("Win32_DiskPartition").Cast<ManagementObject>().Where(entity => entity != null))
         {
            foreach (var i in o.GetRelated("Win32_LogicalDisk"))
            {
               driveLetters.Add(((string) i["Name"]).First());
            }
         }
         driveDescription = drive;
         return driveLetters;
      }
   }
}