﻿using System.Threading;
using Usb.UMS.Platform.Unsupported;
using Usb.UMS.Platform.Windows;

namespace Usb.UMS.Platform
{
   internal sealed class UmsSelector
   {
      public static readonly UsbManager<UmsDevice> Instance;

      static UmsSelector()
      {
         foreach (var umsManager in new UsbManager<UmsDevice>[]
         {
            new WinUmsManager(),
            //new LinuxUmsManager(),
            //new MacUmsManager(),
            new UnsupportedUmsManager()
         })
         {
            if (!umsManager.IsSupported) continue;
            var readyEvent = new ManualResetEvent(false);

            Instance = umsManager;
            var managerThread = new Thread(Instance.RunImpl) {IsBackground = true};
            managerThread.Start(readyEvent);
            readyEvent.WaitOne();

            break;
         }
      }
   }
}