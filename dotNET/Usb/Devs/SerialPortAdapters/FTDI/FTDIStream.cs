﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace Usb.FTDI
{
// ReSharper disable once InconsistentNaming
   /// <summary>
   /// Communicates with a FTDI device
   /// </summary>
   [ComVisible(true), Guid("0C263D05-0D58-4c6c-AEA7-EB9E0C5338A2")]
   public abstract class FTDIStream : UsbStream
   {
      protected readonly EventWaitHandle RxEvent;

      protected FTDIStream()
      {
         RxEvent = new AutoResetEvent(false);
      }

      #region Overrides of UsbStream

      protected override void HandleFree()
      {
      }

      #endregion

      /// <exclude />
      public override void Flush()
      {
      }

      /// <exclude />
      public override bool CanRead
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanSeek
      {
         get { return false; }
      }

      /// <exclude />
      public override bool CanWrite
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanTimeout
      {
         get { return true; }
      }

      /// <exclude />
      public override void SetLength(long value)
      {
         throw new NotSupportedException();
      }

      /// <exclude />
      public override long Length
      {
         get { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Position
      {
         get { throw new NotSupportedException(); }
         set { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Seek(long offset, SeekOrigin origin)
      {
         throw new NotSupportedException();
      }

      protected override void ReadThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }

         try
         {
            while (!cancellationToken.IsCancellationRequested)
            {
               if (!RxEvent.WaitOne(ReadTimeout))
               {
                  continue;
               }

               if (cancellationToken.IsCancellationRequested)
               {
                  break;
               }

               lock (InputQueue)
               {
                  const uint bufLen = 255;
                  var inputBuffer = new byte[bufLen];
                  var readLength = bufLen;
                  SetTimeouts();
                  while (readLength == bufLen)
                  {
                     if (!NativeRead(inputBuffer, bufLen, ref readLength))
                     {
                        break;
                     }
                     if (readLength == 0) break;
                     var input = new byte[readLength];
                     Array.Copy(inputBuffer, input, readLength);
                     InputQueue.Add(new CommonInput {Bytes = input});
                     Monitor.PulseAll(InputQueue);
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      protected override void WriteThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }

         try
         {
            lock (OutputQueue)
            {
               while (true)
               {
                  while (!cancellationToken.IsCancellationRequested && OutputQueue.Count == 0)
                  {
                     Monitor.Wait(OutputQueue);
                  }
                  if (cancellationToken.IsCancellationRequested)
                  {
                     break;
                  }

                  var output = OutputQueue.Dequeue();
                  try
                  {
                     Monitor.Exit(OutputQueue);
                     SetTimeouts();
                     try
                     {
                        uint length = 0;
                        if (!NativeWrite(output.Bytes, output.Bytes.Length, ref length))
                        {
                           length = 0;
                        }
                        output.DoneOK = length == output.Bytes.Length;
                     }
                     finally
                     {
                        Monitor.Enter(OutputQueue);
                     }
                  }
                  finally
                  {
                     output.Done = true;
                     Monitor.PulseAll(OutputQueue);
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      protected abstract bool NativeRead(byte[] inputBuffer, uint bufLen, ref uint readLength);
      protected abstract bool NativeWrite(byte[] bytes, int inlength, ref uint outlength);
      protected abstract void SetTimeouts();
   }
}