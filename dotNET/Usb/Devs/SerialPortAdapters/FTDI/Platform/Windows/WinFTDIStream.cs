﻿#region License

/* Copyright 2012-2013 James F. Bellinger <http://www.zer7.com/software/FTDIsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#endregion

using System.Threading;

namespace Usb.FTDI.Platform.Windows
{
// ReSharper disable once InconsistentNaming
   internal class WinFTDIStream : FTDIStream
   {

      private Ftdi.Ftdi ftdi;

      ~WinFTDIStream()
      {
         Close();
      }

      internal void Init(FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE deviceInfo, WinFTDIDevice winDevice)
      {
         ftdi = new Ftdi.Ftdi();
         ftdi.OpenBySerialNumber(deviceInfo.SerialNumber);
         ftdi.SetEventNotification(FTD2XX_NET.FTDI.FT_EVENTS.FT_EVENT_RXCHAR, RxEvent);

         Init(winDevice);
         HandleInitAndOpen();
      }


      protected override void HandleFree()
      {
         try
         {
            ftdi.Close();
         }
         catch
         {
         }
         ftdi = null;
      }

      

      protected override void SetTimeouts()
      {
         if (ftdi == null)
         {
            return;
         }
         ftdi.SetTimeouts(ReadTimeout == Timeout.Infinite ? 0 : (uint) ReadTimeout,
            WriteTimeout == Timeout.Infinite ? 0 : (uint) WriteTimeout);
      }

      protected override bool NativeRead(byte[] inputBuffer, uint bufLen, ref uint readLength)
      {
         return ftdi.Read(inputBuffer, bufLen, ref readLength) == FTD2XX_NET.FTDI.FT_STATUS.FT_OK;
      }

      protected override bool NativeWrite(byte[] bytes, int inlength, ref uint outlength)
      {
         return ftdi.Write(bytes, inlength, ref outlength) == FTD2XX_NET.FTDI.FT_STATUS.FT_OK;
      }
   }
}