﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using Usb.FTDI;
using Usb.FTDI.Platform.Windows;
using Usb.Helpers;
using Usb.Platform.Windows;

namespace Usb.SerialPortAdapters.FTDI.Platform.Windows
{
   // ReSharper disable once InconsistentNaming
   internal class WinFTDIManager : WinManager<FTDIDevice>
   {
      /// <summary>
      /// List of connected devices. If you call Refresh it returns only not opened devices  
      /// </summary>
      private readonly List<FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE> devices = new List<FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE>();

      private const string MANUFACTURER = "FTDI";

      #region Overrides of UsbManager<FTDIDevice>

      protected override object[] Refresh()
      {
         var devs = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] { };

         try
         {
            var ftdi = new Ftdi.Ftdi();
            do
            {
               uint devcount = 0;
               ftdi.GetNumberOfDevices(ref devcount);
               if (devcount == 0) break;

               devs = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[devcount];
               if (ftdi.GetDeviceList(devs) != FTD2XX_NET.FTDI.FT_STATUS.FT_OK) break;

            } while (false);
            ftdi.Close();
         }
         catch
         {
         }

         SaveDevices(devs);
         return devices.Where(d => d != null).Distinct().Cast<object>().ToArray();
      }

      protected override bool TryCreateDevice(object key, out FTDIDevice device, out object creationState)
      {
         try
         {
            Debug.Assert(key is FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE, "Invalid use of WinFTDIManager");
            device = new WinFTDIDevice((FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE)key);

            InitFtdiDevice(device);
            creationState = null;
            return true;
         }
         catch (Exception)
         {
            device = null;
            creationState = null;
            return false;
         }
      }


      protected override void CompleteDevice(object key, FTDIDevice device, object creationState)
      {
      }

      #endregion

      #region Private helpers

      private void SaveDevices(FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] newDevices)
      {
         CheckDevicesIfStillConnected();
         foreach (var info in newDevices.Where(p => p != null))
         {
            var foundDev = devices.FirstOrDefault(f => f.SerialNumber.Equals(info.SerialNumber));
            if (foundDev == null && !info.SerialNumber.Equals(string.Empty))
            {
               devices.Add(info);
            }
         }
      }

      private void CheckDevicesIfStillConnected()
      {
         if (!devices.Any()) return;

         //load system devices
         var systemDevices = new List<string>();
         foreach (var queryObj in new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Manufacturer = '" + MANUFACTURER + "'").Get())
         {
            var deviceId = queryObj["DeviceID"].ToString();
            var sn = GetSerialnumber(deviceId);
            if (sn.Equals(string.Empty)) continue;

            systemDevices.Add(sn);
         }

         //delete device from list if is not present in system call
         for (var i = devices.Count - 1; i >= 0; i--)
         {
            var exist =
               systemDevices.FirstOrDefault(f => devices[i].SerialNumber.StartsWith(f.Substring(0, f.Length - 1)));
            if (exist == null)
            {
               devices.RemoveAt(i);
            }
         }
      }

      private string GetDevicePortName(string serialNumber)
      {
         foreach (var queryObj in new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Manufacturer = '" + MANUFACTURER + "'").Get())
         {
            var deviceId = queryObj["DeviceID"].ToString();
            var sn = GetSerialnumber(deviceId);

            if (!sn.StartsWith(serialNumber)) continue;
            return ManagementBaseObjectParser.GetPort(queryObj);
         }
         return "";
      }


      private string GetSerialnumber(string deviceId)
      {
         try
         {
            var idParts = deviceId.Split('\\');
            var parts = idParts[1].Split('+');
            return parts.Last();
         }
         catch (Exception)
         {
            return "";
         }
      }

      private void InitFtdiDevice(FTDIDevice device)
      {
         var ftdi = new Ftdi.Ftdi();
         try
         {
            var ee2232 = new FTD2XX_NET.FTDI.FT2232_EEPROM_STRUCTURE();
            var ee232R = new FTD2XX_NET.FTDI.FT232R_EEPROM_STRUCTURE();
            var ee2232H = new FTD2XX_NET.FTDI.FT2232H_EEPROM_STRUCTURE();
            var ee4232H = new FTD2XX_NET.FTDI.FT4232H_EEPROM_STRUCTURE();
            var winDevice = (WinFTDIDevice)device;

            switch (winDevice.Type)
            {
               case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_2232:
                  ftdi.ReadFT2232EEPROM(ee2232);
                  winDevice.EepromData = ee2232;
                  break;
               case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_232R:
                  ftdi.ReadFT232REEPROM(ee232R);
                  winDevice.EepromData = ee232R;
                  break;
               case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_2232H:
                  ftdi.ReadFT2232HEEPROM(ee2232H);
                  winDevice.EepromData = ee2232H;
                  break;
               case FTD2XX_NET.FTDI.FT_DEVICE.FT_DEVICE_4232H:
                  ftdi.ReadFT4232HEEPROM(ee4232H);
                  winDevice.EepromData = ee4232H;
                  break;
            }

            var port = GetDevicePortName(winDevice.SerialNumber);
            winDevice.SetPort(port);
         }
         catch (Exception)
         {
         }
         finally
         {
            ftdi.Close();
         }
      }

      #endregion
   }
}