﻿using Usb.SerialPortAdapters;

namespace Usb.FTDI.Platform.Windows
{
   // ReSharper disable once InconsistentNaming
   internal sealed class WinFTDIDevice : FTDIDevice
   {
      internal WinFTDIDevice(FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE deviceInfo)
      {
         DeviceInfo = deviceInfo;
         EepromData = new FTD2XX_NET.FTDI.FT_EEPROM_DATA();
      }

      protected override UartStream CreateAndInitStream()
      {
         var stream = new WinFTDIStream();
         stream.Init(DeviceInfo, this);
         return stream;
      }

      public FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE DeviceInfo { get; private set; }

      internal FTD2XX_NET.FTDI.FT_DEVICE Type
      {
         get { return DeviceInfo.Type; }
      }

      internal FTD2XX_NET.FTDI.FT_EEPROM_DATA EepromData { get; set; }

      #region Overrides of UsbDevice

      /// <summary>
      /// The manufacturer name.
      /// </summary>
      public override string Manufacturer
      {
         get { return EepromData.Manufacturer; }
      }

      /// <summary>
      /// The USB product ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int ProductID
      {
         get { return EepromData.ProductID; }
      }

      /// <summary>
      /// The product name.
      /// </summary>
      public override string ProductName
      {
         get { return DeviceInfo.Description; }
      }

      /// <summary>
      /// The product version.
      /// This is a 16-bit number encoding the major and minor versions in the upper and lower 8 bits, respectively.
      /// </summary>
      public override int ProductVersion
      {
         get { return 1; }
      }

      /// <summary>
      /// The device serial number.
      /// </summary>
      public override string SerialNumber
      {
         get { return DeviceInfo.SerialNumber; }
      }

      /// <summary>
      /// The USB vendor ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int VendorID
      {
         get { return EepromData.VendorID; }
      }

      #endregion

      public override uint LockId
      {
         get { return DeviceInfo.LocId; }
      }

      #region Overrides of Object

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (!(obj is WinFTDIDevice))
         {
            return false;
         }
         return DeviceInfo.Equals((obj as WinFTDIDevice).DeviceInfo);
      }

      /// <summary>
      /// Serves as a hash function for a particular type. 
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         return DeviceInfo.GetHashCode();
      }

      #endregion
   }

}