﻿using System.Threading;
using Usb.FTDI.Platform.Unsupported;
using Usb.SerialPortAdapters.FTDI.Platform.Windows;

namespace Usb.FTDI.Platform
{
// ReSharper disable once InconsistentNaming
   internal sealed class FTDISelector
   {
      public static readonly UsbManager<FTDIDevice> Instance;

      static FTDISelector()
      {
         foreach (var ftdiManager in new UsbManager<FTDIDevice>[]
         {
            new WinFTDIManager(),
            new UnsupportedFTDIManager()
         })
         {
            if (!ftdiManager.IsSupported) continue;
            var readyEvent = new ManualResetEvent(false);

            Instance = ftdiManager;
            var managerThread = new Thread(Instance.RunImpl) {IsBackground = true};
            managerThread.Start(readyEvent);
            readyEvent.WaitOne();

            break;
         }
      }
   }
}