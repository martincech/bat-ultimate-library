﻿using Usb.ELO;

namespace Usb.FTDI
{
   // ReSharper disable once InconsistentNaming
   public abstract class FTDIDevice : UartUsbDevice
   {
      private string portName = "";

      public void SetPort(string port)
      {
         portName = port;
      }

      public override string PortName { get { return portName; } }
      public abstract uint LockId { get; }
   }
}
