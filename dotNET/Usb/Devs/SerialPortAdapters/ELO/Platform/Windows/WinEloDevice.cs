﻿using Usb.SerialPortAdapters;

namespace Usb.ELO.Platform.Windows
{
   internal sealed class WinEloDevice : EloDevice
   {
      #region Private Fields

      private readonly string manufacturer;
      private readonly int productId;
      private readonly string productName;
      private readonly int productVersion;
      private readonly string serialNumber;
      private readonly int vendorId;

      #endregion

      #region Constructor

      public WinEloDevice(string port, string productName, int pid, int vid, string manufacturer, string serialNumber)
         : base(port)
      {
         this.productName = productName;

         this.manufacturer = manufacturer;
         this.serialNumber = serialNumber;
         productId = pid;
         productVersion = -1;
         vendorId = vid;
      }

      #endregion

      #region Overrides of EloDevice

      protected override UartStream CreateAndInitStream()
      {
         var stream = new WinEloStream();
         stream.Init(this);
         return stream;
      }

      #endregion

      #region Overrides of UsbDevice

      public override string Manufacturer
      {
         get { return manufacturer; }
      }

      public override int ProductID
      {
         get { return productId; }
      }

      public override string ProductName
      {
         get { return productName; }
      }

      public override int ProductVersion
      {
         get { return productVersion; }
      }

      public override string SerialNumber
      {
         get { return serialNumber; }
      }

      public override int VendorID
      {
         get { return vendorId; }
      }

      #endregion
   }
}
