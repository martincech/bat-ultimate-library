﻿using System;
using System.Runtime.InteropServices;
using SerialPort;

namespace Usb.ELO.Platform.Windows
{
   public class Uart
   {
      protected int MHandle = InvalidHandleValue;

      public bool IsOpen
      {
         get { return (MHandle != InvalidHandleValue); }
      }

      
      /// <summary>
      /// Check for device <Name> presention
      /// </summary>
      /// <param name="name"></param>
      /// <returns></returns>
      public static bool Access(string name)
      {  
         var deviceName = ComName(name);
         var handle = CreateFile(deviceName, GenericRead | GenericWrite, 0, 0, OpenExisting, 0, 0);
         if (handle == InvalidHandleValue)
         {
            return (false);
         }
         CloseHandle(handle);
         return (true);
      }


      /// <summary>
      /// Open device by <Name>.
      /// </summary>
      /// <param name="name"></param>
      /// <returns></returns>
      public bool Open(string name)
      {
         Close();                       // close previous
         var deviceName = ComName(name);
         MHandle = CreateFile(deviceName, GenericRead | GenericWrite, 0, 0, OpenExisting, 0, 0);
         return MHandle != InvalidHandleValue;
      }


      /// <summary>
      /// Close device
      /// </summary>
      public void Close()
      {
         if (MHandle == InvalidHandleValue)
         {
            return;
         }
         CloseHandle(MHandle);
         MHandle = InvalidHandleValue;
      }


      /// <summary>
      /// Write <Data> of size <Length>, returns written length (or 0 if error)
      /// </summary>
      /// <param name="data"></param>
      /// <param name="length"></param>
      /// <returns></returns>
      public int Write(byte[] data, int length)
      {
         if (!IsOpen)
         {
            throw new Exception("Not open");
         }
         int w;
         return !WriteFile(MHandle, data, length, out w, 0) ? 0 : w;
      }


      /// <summary>
      /// Read data <Data> of size <Length>, returns true length (or 0 if error)
      /// </summary>
      /// <param name="data"></param>
      /// <param name="length"></param>
      /// <returns></returns>
      public int Read(byte[] data, int length)
      {
         if (!IsOpen)
         {
            throw new Exception("Not open");
         }
         var r = 0;
         return !ReadFile(MHandle, data, length, out r, 0) ? 0 : r;
      }


      /// <summary>
      /// Make input/output queue empty
      /// </summary>
      public void Flush()
      {
         if (!IsOpen)
         {
            throw new Exception("Not open");
         }
         PurgeComm(MHandle, PurgeRxclear | PurgeTxclear);
      }


      /// <summary>
      /// Set timing of receiver - returns collected data immediately
      /// </summary>
      public void SetRxNowait()
      {
         Commtimeouts to;

         if (!IsOpen)
         {
            return;                    // return silently
         }
         // Tx timeout :
         to.WriteTotalTimeoutMultiplier = 1;
         to.WriteTotalTimeoutConstant = 2000;
         // RxTimeout :
         to.ReadIntervalTimeout = 0xffffffff;
         to.ReadTotalTimeoutMultiplier = 0;
         to.ReadTotalTimeoutConstant = 0;
         SetCommTimeouts(MHandle, ref to);
      }


      /// <summary>
      /// Set Rx <Timeout> [ms]
      /// </summary>
      /// <param name="timeout"></param>
      public void SetRxWait(int timeout)
      {
         Commtimeouts to;

         if (!IsOpen)
         {
            return;                    // return silently
         }
         // Tx timeout :
         to.WriteTotalTimeoutMultiplier = 1;
         to.WriteTotalTimeoutConstant = 2000;
         // RxTimeout :
         to.ReadIntervalTimeout = 0;
         to.ReadTotalTimeoutMultiplier = 0;
         to.ReadTotalTimeoutConstant = (uint)timeout;
         SetCommTimeouts(MHandle, ref to);
      }


      public void SetParameters(int baudRate, Parity parity, int bits, StopBit stopBits, Handshake handshake)
      {
         Dcb dcb;

         if (!IsOpen)
         {
            return;                    // return silently
         }
         GetCommState(MHandle, out dcb);
         dcb.BaudRate = (uint)baudRate;
         dcb.ByteSize = (byte)bits;
         dcb.Parity = (byte)parity;
         dcb.StopBits = (byte)stopBits;
         switch (handshake)
         {
            case Handshake.None:
               dcb.Handshake = (DtrControlEnable << FDtrControl) |
                               (RtsControlEnable << FRtsControl);
               break;

            case Handshake.Hardware:
               dcb.Handshake = (DtrControlHandshake << FDtrControl) |
                               (RtsControlHandshake << FRtsControl) |
                               (1 << FOutxCtsFlow) |
                               (1 << FOutxDsrFlow) |
                               (1 << FDsrSensitivity);
               break;

            case Handshake.XOnXOff:
               dcb.Handshake = (1 << FInX) |
                               (1 << FOutX) |
                               (DtrControlEnable << FDtrControl) |
                               (RtsControlEnable << FRtsControl);
               break;
         }
         SetCommState(MHandle, ref dcb);
      }


      public void SetParameters(int baudRate, Parity parity)
      {
         SetParameters(baudRate, parity, 8, StopBit.One, Handshake.None);
      }


      /// <summary>
      /// Converts <Name> to appropriate OS name
      /// </summary>
      /// <param name="name"></param>
      /// <returns></returns>
      static protected string ComName(string name)
      {
         return ("\\\\.\\" + name);
      }


      #region Win32 API

      internal const uint GenericRead = 0x80000000;
      internal const uint GenericWrite = 0x40000000;

      internal const uint PurgeTxclear = 0x0004;
      internal const uint PurgeRxclear = 0x0008;

      internal const int InvalidHandleValue = -1;

      internal const int OpenExisting = 3;

      [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      internal static extern int CreateFile(string lpFileName, uint dwDesiredAccess, int dwShareMode, int lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, int hTemplateFile);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern void CloseHandle(int hFile);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern Boolean ReadFile(int hFile, byte[] lpBuffer, int nNumberOfBytesToRead, out int lpNumberOfBytesRead, int lpOverlapped);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern Boolean WriteFile(int hFile, byte[] lpBuffer, int nNumberOfBytesToWrite, out int lpNumberOfBytesWritten, int lpOverlapped);

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      public struct Dcb
      {
         public uint DCBlength;
         public uint BaudRate;
         public uint Handshake;
         public short wReserved;
         public short XonLim;
         public short XoffLim;
         public byte ByteSize;
         public byte Parity;
         public byte StopBits;
         public byte XonChar;
         public byte XoffChar;
         public byte ErrorChar;
         public byte EofChar;
         public byte EvtChar;
         public short wReserved2;
      };

      // Handshake bitfield offsets :
      public const int FBinary = 0;     /* Binary Mode (skip EOF check)    */
      public const int FParity = 1;     /* Enable parity checking          */
      public const int FOutxCtsFlow = 2;     /* CTS handshaking on output       */
      public const int FOutxDsrFlow = 3;     /* DSR handshaking on output       */
      public const int FDtrControl = 4;     /* DTR Flow control                */
      public const int FDsrSensitivity = 6;     /* DSR Sensitivity              */
      public const int FTxContinueOnXoff = 7;     /* Continue TX when Xoff sent */
      public const int FOutX = 8;     /* Enable output X-ON/X-OFF        */
      public const int FInX = 9;     /* Enable input X-ON/X-OFF         */
      public const int FErrorChar = 10;    /* Enable Err Replacement          */
      public const int FNull = 11;    /* Enable Null stripping           */
      public const int FRtsControl = 12;    /* Rts Flow control                */
      public const int FAbortOnError = 14;    /* Abort all reads and writes on Error */
      // DTR handshake :
      public const uint DtrControlDisable = 0x00;
      public const uint DtrControlEnable = 0x01;
      public const uint DtrControlHandshake = 0x02;
      // RTS handshake
      public const uint RtsControlDisable = 0x00;
      public const uint RtsControlEnable = 0x01;
      public const uint RtsControlHandshake = 0x02;
      public const uint RtsControlToggle = 0x03;

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      public struct Commtimeouts
      {
         public uint ReadIntervalTimeout;
         public uint ReadTotalTimeoutMultiplier;
         public uint ReadTotalTimeoutConstant;
         public uint WriteTotalTimeoutMultiplier;
         public uint WriteTotalTimeoutConstant;
      };

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int GetCommState(int hFile, out Dcb dcb);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int SetCommState(int hFile, ref Dcb dcb);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int SetCommTimeouts(int hFile, ref Commtimeouts to);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int PurgeComm(int hFile, uint mode);

      #endregion
   }
}
