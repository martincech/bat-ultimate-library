﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using Usb.Helpers;
using Usb.Platform.Windows;

namespace Usb.ELO.Platform.Windows
{
   internal class WinEloManager : WinManager<EloDevice>
   {
      private struct DataContent
      {
         public int Vid;
         public int Pid;
         public string Description;
         public string Manufacturer;
         public string SerialNumber;
      }

      protected override object[] Refresh()
      {
         try
         {
            var coms = EloDevices();
            return coms.Cast<object>().ToArray();
         }
         catch (Exception)
         {
            return new object[] { };
         }
      }

      protected override bool TryCreateDevice(object key, out EloDevice device, out object creationState)
      {
         try
         {
            var comNumber = ((KeyValuePair<string, DataContent>)key).Key;
            var content = ((KeyValuePair<string, DataContent>)key).Value;

            var modemDevice = new WinEloDevice(comNumber, content.Description, content.Pid,
                  content.Vid, content.Manufacturer, content.SerialNumber);
            device = modemDevice;
            creationState = null;
            return true;
         }
         catch (Exception)
         {
            device = null;
            creationState = null;
            return false;
         }
      }

      protected override void CompleteDevice(object key, EloDevice device, object creationState)
      {
      }

      private static IEnumerable<KeyValuePair<string, DataContent>> EloDevices()
      {
         var eloObjects = new Dictionary<string, DataContent>();
         foreach (var managementObject in new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_SerialPort ").Get())
         {
            var status = managementObject["Status"].ToString();
            if (!status.Equals("OK")) continue;

            var deviceId = managementObject["PNPDeviceID"].ToString();
            var port = managementObject["DeviceID"].ToString();
            if (!deviceId.ToLower().Contains("vid") || !deviceId.ToLower().Contains("pid"))
            {
               continue;
            }

            var data = new DataContent
            {
               Description = managementObject["Description"].ToString(),
               Pid = VidPidParser.GetPid(deviceId),
               Vid = VidPidParser.GetVid(deviceId),
               Manufacturer = "",
               SerialNumber = deviceId.Split('\\').Last()
               
            };

            if (data.Vid == 4292 && data.Pid == 60000)
            {
               eloObjects.Add(port, data);
            }
         }

         return eloObjects;
      }
   }

}
