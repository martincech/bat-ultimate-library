using System;
using System.IO;
using SerialPort;
using Usb.SerialPortAdapters;

namespace Usb.ELO
{
   public abstract class UartUsbDevice : UsbDevice, ISerialPort
   {
      private UartStream stream;
      private readonly string portName;
      private int baudRate;
      private int rxTimeout;
      private Parity parity;
      private Handshake handshake;
      private int dataBits;
      private StopBit stopBits;
      private const int DEFAULT_BAUD_RATE = 19200;
      private const int DEFAULT_READ_TIMEOUT = 1000; // ms
      private const Handshake DEFAULT_HANDSHAKE = Handshake.None;
      private const Parity DEFAULT_PARITY = Parity.Even;
      private const StopBit DEFAULT_STOP_BITS = StopBit.One;
      private const int DEFAULT_DATA_BITS = 8;

      protected UartUsbDevice() : this("")
      {
      }

      protected UartUsbDevice(string port)
      {
         portName = port;
         dataBits = DEFAULT_DATA_BITS;
         baudRate = DEFAULT_BAUD_RATE;
         rxTimeout = DEFAULT_READ_TIMEOUT;
         parity = DEFAULT_PARITY;
         stopBits = DEFAULT_STOP_BITS;
         handshake = DEFAULT_HANDSHAKE;
      }

      #region Implementation of ISerialPort

      public virtual string PortName { get { return portName; } }

      public  int BaudRate
      {
         get { return baudRate; }
         set
         {
            if (SetProperty(ref baudRate, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      public  int RxTimeout
      {
         get { return rxTimeout; }
         set
         {
            if (SetProperty(ref rxTimeout, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      public  Parity Parity
      {
         get { return parity; }
         set
         {
            if (SetProperty(ref parity, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      public  Handshake Handshake
      {
         get { return handshake; }
         set
         {
            if (SetProperty(ref handshake, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      public  int DataBits
      {
         get { return dataBits; }
         set
         {
            if (SetProperty(ref dataBits, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      public bool RtsEnable { get; set; }

      public StopBit StopBits
      {
         get { return stopBits; }
         set
         {
            if (SetProperty(ref stopBits, value) && stream != null)
            {
               stream.ChangeParameters();
            }
         }
      }

      protected abstract UartStream CreateAndInitStream();

      public Stream Open()
      {
         if (stream != null)
         {
            stream.Close();
         }
         try
         {
            stream = CreateAndInitStream();
            return stream;
         }
         catch (Exception)
         {
            if (stream != null)
            {
               stream.Close();
            }
            throw;
         }
      }
      #endregion

      #region Private helpers

      private bool SetProperty<T>(ref T variable, T value)
      {
         if (variable.Equals(value)) return false;
         variable = value;
         return true;
      }

      #endregion
   }
}