using System;
using System.IO;
using System.Threading;
using SerialPort;
using Usb.ELO.Platform.Windows;

namespace Usb.SerialPortAdapters
{
   public abstract class UartStream : Stream
   {
      #region Private fields

      private readonly Uart port;
      private ISerialPort device;
      #endregion

      protected UartStream()
      {
         port = new Uart();
      }

      internal void Init(ISerialPort device)
      {
         this.device = device;
         ChangeParameters();
      }


      protected internal void ChangeParameters()
      {
         if (device == null) return;
         port.Close();
         CheckConnect();
      }

      

      #region Overrides of Stream

      /// <summary>
      ///    Closes the current stream and releases any resources (such as sockets and file handles) associated with the current
      ///    stream. Instead of calling this method, ensure that the stream is properly disposed.
      /// </summary>
      public override void Close()
      {
         if (port == null) return;
         if (port.IsOpen)
         {
            port.Close();
         }
      }

      public override void Flush()
      {
         CheckConnectWithThrow();
         port.Flush();
      }

      public override int Read(byte[] buffer, int offset, int count)
      {
         var i = port.Read(buffer, count);
         if (i == 0)
         {
            CheckConnectWithThrow();
            throw new IOException("No data available on line.");
         }
         return i;
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         Flush();
         port.Write(buffer, count);
      }

      public override int ReadTimeout
      {
         get
         {
            return device != null ? device.RxTimeout : 0;
         }
         set
         {
            if (device == null)
            {
               return;
            }
            device.RxTimeout = value;
            ChangeParameters();
         }
      }

      #endregion

      #region Private helpers
      private void CheckConnectWithThrow()
      {
         if (!CheckConnect())
         {
            throw new Exception("Port has been closed");
         }
      }

      private bool CheckConnect()
      {
         if (port.IsOpen)
         {
            return true;
         }
         if (device == null) return false;

         try
         {
            if (!port.Open(device.PortName)) return false;

            port.SetParameters(device.BaudRate,
               device.Parity,
               device.DataBits,
               device.StopBits,
               device.Handshake);

            port.SetRxWait(ReadTimeout);
            port.Flush();
            return true;
         }
         catch (IOException ioEx)
         {
            if (ioEx.Message.Contains("COM")) return false;
            Thread.Sleep(200);
            return CheckConnect();
         }
         catch (Exception)
         {
            return false;
         }
      }

      #endregion
   }
}