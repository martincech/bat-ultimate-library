﻿using System.Collections.Generic;
using Usb.TestAndMeasurement.Platform;

namespace Usb.TestAndMeasurement
{
   public class TestAndMeasurementDeviceLoader
   {
      /// <summary>
      /// Gets a list of connected USB devices.
      /// </summary>
      /// <returns>The device list.</returns>
      public IEnumerable<TestAndMeasurementDevice> GetDevices()
      {
         return TestAndMeasurementSelector.Instance.GetDevices();
      }
   }
}
