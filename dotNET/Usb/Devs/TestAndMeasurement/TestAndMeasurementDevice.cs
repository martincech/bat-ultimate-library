﻿namespace Usb.TestAndMeasurement
{
   public abstract class TestAndMeasurementDevice : UsbDevice
   {
      public string VisaUsbAddress
      {
         get { return "USB0::" + IdToHex(VendorID) + "::" + IdToHex(ProductID) + "::" + SerialNumber + "::0::INSTR"; }
      }

      private string IdToHex(int id)
      {
         //result must be in format 0x0000
         return string.Format("0x{0:X4}", id);
      }
   }
}
