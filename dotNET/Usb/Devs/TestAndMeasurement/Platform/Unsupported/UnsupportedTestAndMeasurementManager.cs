﻿namespace Usb.TestAndMeasurement.Platform.Unsupported
{
   internal class UnsupportedTestAndMeasurementManager : UsbManager<TestAndMeasurementDevice>
   {
      #region Overrides of UsbManager<TestAndMeasurementDevice>

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out TestAndMeasurementDevice device, out object creationState)
      {
         throw new System.NotImplementedException();
      }

      protected override void CompleteDevice(object key, TestAndMeasurementDevice device, object creationState)
      {
         throw new System.NotImplementedException();
      }

      public override bool IsSupported
      {
         get { return true; }
      }

      #endregion
   }
}
