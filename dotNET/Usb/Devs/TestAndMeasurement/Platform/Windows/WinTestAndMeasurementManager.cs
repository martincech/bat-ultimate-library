﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using Usb.Helpers;
using Usb.Platform.Windows;

namespace Usb.TestAndMeasurement.Platform.Windows
{
   internal sealed class WinTestAndMeasurementManager : WinManager<TestAndMeasurementDevice>
   {
      private static Dictionary<string, DataContent> _testAndMeasurementObjects;

      private struct DataContent
      {
         public int Vid;
         public int Pid;
         public string Manufacturer;
      }


      #region Overrides of UsbManager<TestAndMeasurementDevice>

      protected override object[] Refresh()
      {
         try
         {
            var devs = TestAndMeasurementDevices();
            return devs.Cast<object>().ToArray();
         }
         catch (Exception)
         {
            return new object[] { };
         }
      }

      protected override bool TryCreateDevice(object key, out TestAndMeasurementDevice device, out object creationState)
      {
         try
         {
            if (!(key is KeyValuePair<string, DataContent>))
            {
               throw new Exception();
            }
            var record = (KeyValuePair<string, DataContent>)key;
            var dev = new WinTestAndMeasurementDevice(record.Value.Pid, record.Value.Vid, record.Key, record.Value.Manufacturer);
            device = dev;
            creationState = null;
            return true;
         }
         catch (Exception)
         {
            device = null;
            creationState = null;
            return false;
         }
      }

      protected override void CompleteDevice(object key, TestAndMeasurementDevice device, object creationState)
      {
      }

      #endregion

      private static IEnumerable<KeyValuePair<string, DataContent>> TestAndMeasurementDevices()
      {
         _testAndMeasurementObjects = new Dictionary<string, DataContent>();
         foreach (var queryObj in new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Service = 'Usbtmc'").Get())
         {
            var deviceId = queryObj["DeviceID"].ToString();

            var parts = deviceId.Split('\\');
            var sn = parts.LastOrDefault();
            if (sn == null) continue;

            var data = new DataContent
            {
               Manufacturer = queryObj["Manufacturer"].ToString(),
               Pid = VidPidParser.GetPid(deviceId),
               Vid = VidPidParser.GetVid(deviceId)
            };

            _testAndMeasurementObjects.Add(sn, data);
         }
         return _testAndMeasurementObjects;
      }
   }
}
