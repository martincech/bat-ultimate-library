﻿namespace Usb.TestAndMeasurement.Platform.Windows
{
   internal sealed class WinTestAndMeasurementDevice : TestAndMeasurementDevice
   {
      #region Private fields

      private readonly int pid;
      private readonly int vid;
      private readonly string serialNumber;
      private readonly string manufacturer;

      #endregion

      #region Constructor

      public WinTestAndMeasurementDevice(int pid, int vid, string serialNumber, string manufacturer)
      {
         this.pid = pid;
         this.vid = vid;
         this.serialNumber = serialNumber;
         this.manufacturer = manufacturer;
      }

      #endregion

      #region Overrides of UsbDevice

      /// <summary>
      /// The manufacturer name.
      /// </summary>
      public override string Manufacturer
      {
         get { return manufacturer; }
      }

      /// <summary>
      /// The USB product ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int ProductID
      {
         get { return pid; }
      }

      /// <summary>
      /// The product name.
      /// </summary>
      public override string ProductName
      {
         get { return ""; }
      }

      /// <summary>
      /// The product version.
      /// This is a 16-bit number encoding the major and minor versions in the upper and lower 8 bits, respectively.
      /// </summary>
      public override int ProductVersion
      {
         get { return 1; }
      }

      /// <summary>
      /// The device serial number.
      /// </summary>
      public override string SerialNumber
      {
         get { return serialNumber; }
      }

      /// <summary>
      /// The USB vendor ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int VendorID
      {
         get { return vid; }
      }

      #endregion
   }
}
