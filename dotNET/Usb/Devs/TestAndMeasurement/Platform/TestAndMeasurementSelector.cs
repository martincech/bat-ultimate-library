﻿using System.Threading;
using Usb.TestAndMeasurement.Platform.Unsupported;
using Usb.TestAndMeasurement.Platform.Windows;

namespace Usb.TestAndMeasurement.Platform
{
   internal sealed class TestAndMeasurementSelector
   {
      public static readonly UsbManager<TestAndMeasurementDevice> Instance;

      static TestAndMeasurementSelector()
      {
         foreach (var testAndMeasurementManager in new UsbManager<TestAndMeasurementDevice>[]
         {
            new WinTestAndMeasurementManager(),
            new UnsupportedTestAndMeasurementManager(),
         })
         {
            if (!testAndMeasurementManager.IsSupported) continue;
            var readyEvent = new ManualResetEvent(false);

            Instance = testAndMeasurementManager;
            var managerThread = new Thread(Instance.RunImpl) {IsBackground = true};
            managerThread.Start(readyEvent);
            readyEvent.WaitOne();

            break;
         }
      }
   }
}
