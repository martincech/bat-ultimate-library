﻿#region License

/* Copyright 2012-2013 James F. Bellinger <http://www.zer7.com/software/hidsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#endregion

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Usb.Platform.Windows;

namespace Usb.HID.Platform.Windows
{
   internal class WinHidStream : HidStream
   {
      private IntPtr closeEventHandle;
      private WinHidDevice device;
      private IntPtr handle;

      public override HidDevice Device
      {
         get { return device; }
      }

      internal WinHidStream()
      {
         closeEventHandle = NativeMethods.CreateManualResetEventOrThrow();
      }

      ~WinHidStream()
      {
         if (!HandleClose())
         {
            return;
         }
         NativeMethods.SetEvent(closeEventHandle);
         HandleRelease();
      }

      internal void Init(string path, WinHidDevice winDevice)
      {
         var inHandle = NativeMethods.CreateFileFromDevice(path,
            NativeMethods.EFileAccess.Read | NativeMethods.EFileAccess.Write, NativeMethods.EFileShare.All);
         if (inHandle == (IntPtr) (-1))
         {
            throw new IOException("Unable to open HID class device.");
         }

         device = winDevice;
         handle = inHandle;
         HandleInitAndOpen();
      }

      protected override void HandleFree()
      {
         NativeMethods.CloseHandle(ref closeEventHandle);
         NativeMethods.CloseHandle(ref handle);
      }

      public override void GetFeature(byte[] buffer)
      {
         Throw.If.OutOfRange(buffer, 0, buffer.Length - 0);
         var reportSize = device.MaxFeatureReportLength - 1;
         var bufLen = buffer.Length;
         var rest = bufLen%reportSize == 0 ? 0 : reportSize - (bufLen%reportSize);
         var buf = new byte[bufLen + rest];

         var actLen = buf.Length;
         GetFeature(buf, 0, ref actLen);
         if (actLen > buffer.Length)
         {
            actLen = buffer.Length;
         }
         Array.Copy(buf, 0, buffer, 0, actLen);
      }

      public override void GetFeature(byte[] buffer, int offset, int count)
      {
         GetFeature(buffer, offset, ref count);
      }

      private void GetFeature(byte[] buffer, int offset, ref int count)
      {
         ReadData(HidReports.FeatureReport);
         count = CommonRead(buffer, offset, count, HidReports.FeatureReport);
      }

      /// <summary>
      /// Reads HID Input Reports.
      /// </summary>
      /// <param name="buffer">The buffer to place the reports into.</param>
      /// <returns>The number of bytes read.</returns>
      public override int Read(byte[] buffer)
      {
         Throw.If.OutOfRange(buffer, 0, buffer.Length - 0);
         var reportSize = device.MaxInputReportLength - 1;
         var bufLen = buffer.Length;
         var rest = bufLen%reportSize == 0 ? 0 : reportSize - (bufLen%reportSize);
         var buf = new byte[bufLen + rest];

         var actLen = Read(buf, 0, buf.Length);
         if (actLen > buffer.Length)
         {
            actLen = buffer.Length;
         }
         Array.Copy(buf, 0, buffer, 0, actLen);
         return actLen;
      }

      public override int Read(byte[] buffer, int offset, int count)
      {
         ReadData(HidReports.BasicReport);
         return CommonRead(buffer, offset, count, HidReports.BasicReport);
      }

      public override void SetFeature(byte[] buffer, int offset, int count)
      {
         CommonWrite(buffer, offset, count, HidReports.FeatureReport);
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         CommonWrite(buffer, offset, count, HidReports.BasicReport);
      }

      #region Private methods

      protected override void ReadThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }
         try
         {
            cancellationToken.WaitHandle.WaitOne();
         }
         finally
         {
            HandleRelease();
         }
      }

      protected override void WriteThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }

         try
         {
            lock (OutputQueue)
            {
               while (true)
               {
                  while (!cancellationToken.IsCancellationRequested && OutputQueue.Count == 0)
                  {
                     Monitor.Wait(OutputQueue);
                  }
                  if (cancellationToken.IsCancellationRequested)
                  {
                     break;
                  }

                  var output = OutputQueue.Dequeue();
                  try
                  {
                     Monitor.Exit(OutputQueue);
                     try
                     {
                        output.DoneOK = WriteData(output.Bytes, (HidReports) output.DataDescription);
                     }
                     finally
                     {
                        Monitor.Enter(OutputQueue);
                     }
                  }
                  finally
                  {
                     output.Done = true;
                     Monitor.PulseAll(OutputQueue);
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      /// <summary>
      /// Write multiple reports
      /// </summary>
      private bool WriteData(byte[] data, HidReports reports)
      {
         lock (device)
         {
            // First byte of each report/feature is allways 0, represents Report ID
            var resLen = data.Length;
            var maxLen = reports == HidReports.BasicReport
               ? device.MaxOutputReportLength - 1
               : device.MaxFeatureReportLength - 1;
            var offset = 0;
            while (resLen > 0)
            {
               var curLen = resLen > maxLen ? maxLen : resLen;
               var obuf = new byte[maxLen + 1];
               Array.Copy(data, offset, obuf, 1, curLen);
               switch (reports)
               {
                  case HidReports.BasicReport:
                     if (!WriteReport(obuf))
                     {
                        return false;
                     }
                     break;
                  case HidReports.FeatureReport:
                     if (!WriteFeature(obuf))
                     {
                        return false;
                     }
                     break;
                  default:
                     throw new ArgumentOutOfRangeException("reports");
               }
               offset += curLen;
               resLen -= curLen;
            }
            return true;
         }
      }

      private unsafe bool WriteFeature(byte[] obuf)
      {
         Debug.Assert(obuf.Length == device.MaxFeatureReportLength);
         var length = device.MaxFeatureReportLength;

         try
         {
            fixed (byte* ptr = obuf)
            {
               if (!NativeMethods.HidD_SetFeature(handle, ptr, length))
               {
                  length = 0;
               }
            }
         }
         catch
         {
            length = 0;
         }
         return length != 0;
      }

      /// <summary>
      /// Read single report
      /// </summary>
      private void ReadData(HidReports reports)
      {
         lock (device)
         {
            lock (InputQueue)
            {
               const uint bufLen = 255;
               var readLength = bufLen;
               while (readLength == bufLen)
               {
                  byte[] inputBuffer;
                  switch (reports)
                  {
                     case HidReports.BasicReport:
                        inputBuffer = ReadReport();
                        break;
                     case HidReports.FeatureReport:
                        inputBuffer = ReadFeature();
                        break;
                     default:
                        throw new ArgumentOutOfRangeException("reports");
                  }
                  if (inputBuffer == null)
                  {
                     readLength = 0;
                  }
                  else
                  {
                     readLength = (uint) inputBuffer.Length;
                  }
                  if (readLength == 0) break;
                  var input = new byte[readLength];
                  Array.Copy(inputBuffer, input, readLength);
                  InputQueue.Add(new CommonInput {Bytes = input, DataDescription = reports});
                  Monitor.PulseAll(InputQueue);
               }
            }
         }
      }

      private unsafe byte[] ReadFeature()
      {
         var length = device.MaxFeatureReportLength;
         var localBuffer = new byte[length];
         var ibuf = new byte[length - 1];
         fixed (byte* ptr = localBuffer)
         {
            if (!NativeMethods.HidD_GetFeature(handle, ptr, length))
            {
               return null;
            }

            Array.Copy(localBuffer, 1, ibuf, 0, ibuf.Length);
            return ibuf;
         }
      }

      private unsafe bool WriteReport(byte[] obuf)
      {
         Debug.Assert(obuf.Length == device.MaxOutputReportLength);
         var @event = NativeMethods.CreateManualResetEventOrThrow();
         var length = device.MaxOutputReportLength;

         HandleAcquireIfOpenOrFail();
         try
         {
            fixed (byte* ptr = obuf)
            {
               var overlapped = stackalloc NativeOverlapped[1];
               overlapped[0].EventHandle = @event;
               uint bytesTransferred;
               NativeMethods.OverlappedOperation(handle, @event, WriteTimeout, closeEventHandle,
                  NativeMethods.WriteFile(handle, ptr, length, IntPtr.Zero, overlapped),
                  overlapped, out bytesTransferred);
            }
         }
         catch
         {
            length = 0;
         }
         finally
         {
            HandleRelease();
            NativeMethods.CloseHandle(@event);
         }
         return length != 0;
      }

      private unsafe byte[] ReadReport()
      {
         var @event = NativeMethods.CreateManualResetEventOrThrow();

         HandleAcquireIfOpenOrFail();
         try
         {
            var length = device.MaxInputReportLength;
            var localBuffer = new byte[length];
            fixed (byte* ptr = localBuffer)
            {
               var overlapped = stackalloc NativeOverlapped[1];
               overlapped[0].EventHandle = @event;

               uint bytesTransferred;
               NativeMethods.OverlappedOperation(handle, @event, ReadTimeout, closeEventHandle,
                  NativeMethods.ReadFile(handle, ptr, length, IntPtr.Zero, overlapped),
                  overlapped, out bytesTransferred);
               if (bytesTransferred > 1)
               {
                  var ibuf = new byte[bytesTransferred - 1];
                  Array.Copy(localBuffer, 1, ibuf, 0, bytesTransferred - 1);
                  return ibuf;
               }
               else
               {
                  return null;
               }
            }
         }
         catch (Exception)
         {
            return null;
         }
         finally
         {
            HandleRelease();
            NativeMethods.CloseHandle(@event);
         }
      }

      #endregion
   }
}