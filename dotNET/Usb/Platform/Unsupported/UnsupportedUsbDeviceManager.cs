﻿using System;
using System.Collections.Generic;

namespace Usb.Platform.Unsupported
{
   /// <summary>
   ///    Watches system resource for available HID devices and informs
   ///    about them.
   /// </summary>
   internal class UnsupportedUsbDeviceManager : UsbDeviceManager, IPlatformSupport
   {
     
      public bool IsSupported
      {
         get { return true; }
      }

      #region Overrides of UsbDeviceManager

      /// <summary>
      ///    List of connected usb devices.
      /// </summary>
      public override IEnumerable<UsbDevice> ConnectedDevices
      {
         get { throw new NotImplementedException(); }
      }

      public override event EventHandler<UsbDevice> DeviceConnected
      {
         add { }
         remove { }
      }
      public override event EventHandler<UsbDevice> DeviceDisconnected
      {
         add { }
         remove { }
      }

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public override void Dispose()
      {
         throw new NotImplementedException();
      }

      #endregion
   }
}