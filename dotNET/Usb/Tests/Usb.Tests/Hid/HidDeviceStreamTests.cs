﻿using System;
using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.HID;
using Usb.HID.Platform.Windows;

namespace Usb.Tests.Hid
{
   [TestClass]
   public class HidDeviceStreamTests
   {
      private HidStream hidStream;
      private MemoryStream stream;

      [TestInitialize]
      public void Init()
      {
         var hidDevice = new WinHidDevice("");
         Assert.IsNotNull(hidDevice);
         stream = new MemoryStream();
         hidStream = hidDevice.Open(stream);
      }

      [TestCleanup]
      public void Clean()
      {
         hidStream.Close();
         stream.Close();
      }

      [TestMethod]
      public void StreamObject_Implemented()
      {
         Assert.IsInstanceOfType(hidStream, typeof(Stream));
      }

      [TestMethod]
      [ExpectedException(typeof(NotSupportedException))]
      public void Stream_Seek_NotSupported()
      {
         Assert.IsFalse(hidStream.CanSeek);
         hidStream.Seek(5, SeekOrigin.Begin);
      }

      [TestMethod]
      [ExpectedException(typeof(NotSupportedException))]
      public void Stream_Length_NotSupported()
      {
         var length = hidStream.Length;
      }

      [TestMethod]
      [ExpectedException(typeof(NotSupportedException))]
      public void Stream_Position_Get_NotSupported()
      {
         var length = hidStream.Position;
      }

      [TestMethod]
      [ExpectedException(typeof(NotSupportedException))]
      public void Stream_Position_Set_NotSupported()
      {
         hidStream.Position = 5;
      }

      [TestMethod]
      public void Stream_CanReadAndWriteData()
      {
         Assert.IsTrue(hidStream.CanRead);
         Assert.IsTrue(hidStream.CanWrite);
      }

      #region Write exceptions

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void Write_Throws_Exception_OnNullBuffer()
      {
         hidStream.Write(null, 0, 0);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Write_Throws_Exception_OnEmptyBufferNonEmptyCount()
      {
         hidStream.Write(new byte[] { }, 0, 1);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Write_Throws_Exception_WhenOffsetNonZero()
      {
         hidStream.Write(new byte[] { 1, 2, 3 }, 3, 1);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Write_Throws_Exception_WhenCountNegative()
      {
         hidStream.Write(new byte[] { 1, 2, 3 }, 2, -1);
      }

      [TestMethod]
      [ExpectedException(typeof(IOException))]
      public void Write_Throws_Exception_WhenNativeDeviceClosed()
      {
         hidStream.Close();
         hidStream.Write(new byte[] { 1, 2, 3 }, 0, 1);
      }

      #endregion

      #region Read exceptions

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void Read_Throws_Exception_OnNullBuffer()
      {
         hidStream.Read(null, 0, 0);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Read_Throws_Exception_OnEmptyBufferNonEmptyCount()
      {
         hidStream.Read(new byte[] { }, 0, 1);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Read_Throws_Exception_WhenOffsetNonZero()
      {
         hidStream.Read(new byte[] { 1, 2, 3 }, 3, 1);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void Read_Throws_Exception_WhenCountNegative()
      {
         hidStream.Read(new byte[] { 1, 2, 3 }, 2, -1);
      }

      [TestMethod]
      [ExpectedException(typeof(IOException))]
      public void Read_Throws_Exception_WhenNativeDeviceClosed()
      {
         hidStream.Close();
         hidStream.Read(new byte[] { 1, 2, 3 }, 0, 1);
      }

      #endregion

      [TestMethod]
      public void Write_IsOk_ValidParams()
      {
         hidStream.Write(TestConstants.SendMessage1, 0, TestConstants.SendMessage1.Length);
      }

      [TestMethod]
      public void WriteRead_IsOk()
      {
         var snd1 = TestConstants.SendMessage1;
         var rcvMsg = snd1;

         WriteReadIsOk(snd1, rcvMsg);
      }

      [TestMethod]
      public void WriteRead_long_IsOk()
      {
         byte[] snd1;
         while ((snd1 = TestConstants.SendMessage1).Length < 80) { }
         var rcvMsg = snd1;

         WriteReadIsOk(snd1, rcvMsg);
      }

      [TestMethod]
      public void WriteRead_NoAdditionalDataAvailable()
      {
         var snd1 = TestConstants.SendMessage1;
         var rcvMsg = snd1;
         var index = rcvMsg.Length - 1;
         var rcv = new byte[rcvMsg.Length];

         WriteRead_NoAdditionalDataAvailable(rcv, snd1, rcvMsg, index);
      }
      [TestMethod]
      public void WriteRead_long_NoAdditionalDataAvailable()
      {
         byte[] snd1;
         while ((snd1 = TestConstants.SendMessage1).Length < 80) { }
         var rcvMsg = snd1;
         var index = rcvMsg.Length - 1;
         var rcv = new byte[rcvMsg.Length];

         WriteRead_NoAdditionalDataAvailable(rcv, snd1, rcvMsg, index);
      }

      [TestMethod]
      public void MultipleWriteReadTests()
      {
         for (var i = 0; i < 20; i++)
         {
            Console.WriteLine(i);
            WriteRead_IsOk();
            Init();
            WriteRead_NoAdditionalDataAvailable();
            Init();
            WriteRead_long_IsOk();
            Init();
            WriteRead_long_NoAdditionalDataAvailable();
            Init();
         }
      }
      private void WriteReadIsOk(byte[] snd1, byte[] rcvMsg)
      {
         Assert.AreNotEqual(0, snd1.Length);
         hidStream.Write(snd1, 0, snd1.Length);

         var strCopy = new MemoryStream();
         strCopy.Write(stream.GetBuffer(), 0, (int)stream.Length);
         strCopy.Seek(0, SeekOrigin.Begin);
         var wBuf = new byte[snd1.Length];
         strCopy.Read(wBuf, 0, snd1.Length);
         Assert.AreEqual(snd1.Length, wBuf.Length);
         CollectionAssert.AreEqual(snd1, wBuf);
         stream.Seek(0, SeekOrigin.Begin);

         var rcv = new byte[rcvMsg.Length];
         Thread.Sleep(20);
         var data = hidStream.Read(rcv, 0, rcvMsg.Length);
         Assert.AreNotEqual(0, data);
         Assert.AreEqual(rcvMsg.Length, data);
         CollectionAssert.AreEqual(rcvMsg, rcv);
      }

      private void WriteRead_NoAdditionalDataAvailable(byte[] rcv, byte[] snd1, byte[] rcvMsg, int index)
      {
         while ((hidStream.Read(rcv, 0, 1)) != 0)
         {
         }
         WriteReadIsOk(snd1, rcvMsg);
         var data = hidStream.Read(rcv, 0, rcvMsg.Length);
         Assert.AreEqual(0, data);
      }
   }
}