﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.HID;

namespace Usb.Tests.Hid
{
   [TestClass]
   public class HidDeviceCreationAndPropertyTests
   {
      [TestMethod]
      public void ObjectCreated_Any_WithoutSpecification()
      {
         var hidDeviceLoader = new HidDeviceLoader();
         Assert.IsNotNull(hidDeviceLoader.GetDeviceOrDefault());
      }

      [TestMethod]
      public void ObjectCreated_Invalid_WithInvalidVidPid()
      {
         var hidDeviceLoader = new HidDeviceLoader();
         var hidDevice = hidDeviceLoader.GetDeviceOrDefault(0,0);
         Assert.IsNull(hidDevice);
      }

      [TestMethod]
      public void ObjectCreated_Valid_IsBat2VidPid()
      {
         var hidDeviceLoader = new HidDeviceLoader();
         var hidDevice = hidDeviceLoader.GetDeviceOrDefault(TestConstants.VID, TestConstants.PID);
         Assert.IsNotNull(hidDevice);
         Assert.AreEqual(TestConstants.VID, hidDevice.VendorID);
         Assert.AreEqual(TestConstants.PID, hidDevice.ProductID);
         Assert.IsNotNull(hidDevice.DevicePath);
      }

   }
}
