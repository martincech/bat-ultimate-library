﻿using System;
using System.IO;
using SerialPort;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.ELO;

namespace Usb.Tests.Elo
{
   [TestClass]
   public class WinEloDeviceTests
   {
      private const int READ_BUFFER_SIZE = 20;

      private static EloDevice _masterDevice;
      private static EloDevice _slaveDevice;
      private static Stream _masterStream;
      private static Stream _slaveStream;

      private readonly byte[] testData = { 53, 123, 15, 8, 9, 10 };

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         UsbDeviceManager manager = UsbDeviceManager.Instance;
         Thread.Sleep(1200);
         var eloDevs = manager.ConnectedDevices.Where(d => d is EloDevice).ToList();
         Assert.IsTrue(eloDevs.Count >= 2, "You have to connect at least 2 elo device to be able to run these tests.");
         _masterDevice = eloDevs.ElementAt(0) as EloDevice;
         _slaveDevice = eloDevs.ElementAt(1) as EloDevice;
         Assert.IsNotNull(_masterDevice);
         Assert.IsNotNull(_slaveDevice);
      }

      [TestInitialize]
      public void TestInit()
      {
         SetDefaultParameters();
         _masterStream = _masterDevice.Open();
         _slaveStream = _slaveDevice.Open();
      }

      [TestCleanup]
      public void Clean()
      {
         _masterStream.Close();
         _slaveStream.Close();
      }

      private static void SetDefaultParameters()
      {
         _masterDevice.DataBits = 8;
         _masterDevice.BaudRate = 19200;
         _masterDevice.Handshake = Handshake.None;
         _masterDevice.Parity = Parity.None;
         _slaveDevice.DataBits = 8;
         _slaveDevice.BaudRate = 19200;
         _slaveDevice.Handshake = Handshake.None;
         _slaveDevice.Parity = Parity.None;
      }

      [TestMethod]
      public void MasterReadFromSlave_ValidValues()
      {        
         Assert.IsTrue(CompareReadedData());
      }

      [TestMethod]
      public void ParityChangedToOddOnSingle_DifferentValue()
      {
         _slaveDevice.Parity = Parity.Odd;
         _slaveDevice.DataBits = 7;
         Assert.IsFalse(CompareReadedData());
      }

      [TestMethod]
      public void ParityChangedToEvenOnBoth_SameValues()
      {
         _masterDevice.Parity = _slaveDevice.Parity = Parity.Even;
         Assert.IsTrue(CompareReadedData());
      }

      [TestMethod]
      public void ParityChangedToOddOnBoth_SameValues()
      {
         _masterDevice.Parity = _slaveDevice.Parity = Parity.Odd;
         Assert.IsTrue(CompareReadedData());
      }

      [TestMethod]
      public void BaudRateChange_DifferentValue()
      {
         _slaveDevice.BaudRate = 9600;
         Assert.IsFalse(CompareReadedData());
      }

      [TestMethod]
      public void BaudRate_Change_SameValues()
      {
         _masterDevice.BaudRate = 9600;
         _slaveDevice.BaudRate = 9600;
         Assert.IsTrue(CompareReadedData());
      }

      [TestMethod]
      public void HandshakeChange_DifferentValue()
      {
         _slaveDevice.Handshake = Handshake.Hardware;
         Assert.IsFalse(CompareReadedData());
      }

      [TestMethod]
      public void Handshake_Change_SameValues()
      {
         _slaveDevice.Handshake = _masterDevice.Handshake = Handshake.XOnXOff;
         Assert.IsTrue(CompareReadedData());
      }

      [TestMethod]
      public void NoExcpetionWhenSettingParameters_OnClosedStream()
      {
         _masterStream.Close();
         var noException = true;
         try
         {
            _masterDevice.BaudRate = 2500;
            _masterDevice.Parity = Parity.Even;
            _masterDevice.Handshake = Handshake.Hardware;
            _masterDevice.DataBits = 7;
            _masterDevice.RxTimeout = 1200;
         }
         catch (Exception)
         {
            noException = false;
         }         
         Assert.IsTrue(noException);
      }

      [TestMethod]
      [ExpectedException(typeof(Exception))]
      public void ExceptionWhenWrite_OnClosedStream()
      {
         _masterStream.Close();
         _masterStream.Write(testData, 0, testData.Length);
      }

      [TestMethod]
      [ExpectedException(typeof (Exception))]
      public void ExceptionWhenRead_OnClosedStream()
      {
         _masterStream.Close();
         var readBuff = new byte[READ_BUFFER_SIZE];
         _masterStream.Read(readBuff, 0, readBuff.Length);
      }

      [TestMethod]
      public void ReadTimeout_Apply()
      {
         _masterDevice.RxTimeout = 1000;
         const double diffPercentReserve = 10.0;
         var startStamp = DateTime.Now;
         var readBuff = new byte[READ_BUFFER_SIZE];
         int readCount = 0;
         try
         {
            readCount = _masterStream.Read(readBuff, 0, readBuff.Length);
         }
         catch (Exception e)
         {
            Console.WriteLine(e);
         }
         var endStamp = DateTime.Now;
         var diff = new TimeSpan(endStamp.Ticks - startStamp.Ticks);
         
         Assert.AreEqual(0, readCount);
         Assert.IsTrue(diff.TotalMilliseconds >= (_masterDevice.RxTimeout - (_masterDevice.RxTimeout * diffPercentReserve/100)));
         Assert.IsTrue(diff.TotalMilliseconds <= (_masterDevice.RxTimeout + (_masterDevice.RxTimeout * diffPercentReserve/100)));
      }


      [TestMethod]
      public void DataBitsChange_DifferentValue()
      {
         _slaveDevice.DataBits = 5;
         Assert.IsFalse(CompareReadedData());
      }

      [TestMethod]
      public void DataBits_Change_SameValues()
      {
         _slaveDevice.DataBits = _masterDevice.DataBits = 7;
         Assert.IsTrue(CompareReadedData());
      }

      private bool CompareReadedData()
      {
         _slaveStream.Write(testData, 0, testData.Length);
         Thread.Sleep(50);
         var readBuff = new byte[READ_BUFFER_SIZE];
         int readCount = 0;
         try
         {
            readCount = _masterStream.Read(readBuff, 0, readBuff.Length);
         }
         catch (IOException)
         {
            return false;
         }

         if (testData.Length != readCount)
         {
            return false;
         }
         for (var i = 0; i < testData.Length; i++)
         {
            if (testData[i] != readBuff[i])
            {
               return false;
            }
         }
         return true;
      }
   }
}
