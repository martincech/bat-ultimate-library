﻿using System;

namespace Modbus {
    /// <summary>
    /// Reading from and writing to registers
    /// </summary>
    public partial class Modbus {
        
        public bool ReadRegisters(ushort Address, ushort Count) {
            // Cteni stavu
            if (Count > ModbusPdu.MAX_READ_REGISTERS_COUNT) {
                throw new ArgumentException("Number of registers too high");
            }
            modbusPacket.WriteAddress(slaveAddress);
            WriteFunction(ModbusPdu.Function.READ_HOLDING_REGISTERS);
            WriteItemAddress( Address);
            WriteCount( Count);
            modbusPacket.WriteCrc();
            return modbusCom.ComTxStart();
        }

        public void WriteSingleRegister( ushort Address, ushort Value) {
            // Vysle zapis
            modbusPacket.WriteAddress(slaveAddress);
            WriteFunction( ModbusPdu.Function.WRITE_SINGLE_REGISTER);
            WriteItemAddress( Address);
            modbusPacket.WriteWord( Value);
            modbusPacket.WriteCrc();
            modbusCom.ComTxStart();
        }

        public void WriteRegisters(ushort Address, ushort Count, ushort[] Data) {
            // Zapis pole hodnot
            byte i;

            if (Count > ModbusPdu.MAX_WRITE_REGISTERS_COUNT) {
                throw new ArgumentException("Number of registers too high");
            }
            modbusPacket.WriteAddress(slaveAddress);
            WriteFunction( ModbusPdu.Function.WRITE_MULTIPLE_REGISTERS);
            WriteItemAddress( Address);
            WriteCount( Count);
            modbusPacket.WriteByte((byte)(2 * Count));            // ByteCount
            for (i = 0; i < Count; i++) {
                modbusPacket.WriteWord(Data[i]);
            }
            modbusPacket.WriteCrc();
            modbusCom.ComTxStart();
        }

    }
}
