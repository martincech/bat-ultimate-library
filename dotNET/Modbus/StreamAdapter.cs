using System.IO;
using System.Threading;
using Modbus.IO;

namespace Modbus
{
   public class StreamAdapter : Stream, IStreamResource
   {
      private readonly Stream stream;

      public StreamAdapter(Stream stream)
      {
         this.stream = stream;
      }

      /// <summary>
      /// Writes a byte to the current position in the stream and advances the position within the stream by one byte.
      /// </summary>
      /// <param name="value">The byte to write to the stream. </param><exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override void WriteByte(byte value)
      {
         stream.WriteByte(value);
      }

      /// <summary>
      /// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
      /// </summary>
      /// <param name="buffer">An array of bytes. This method copies <paramref name="count"/> bytes from <paramref name="buffer"/> to the current stream. </param><param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin copying bytes to the current stream. </param><param name="count">The number of bytes to be written to the current stream. </param>
      public override void Write(byte[] buffer, int offset, int count)
      {
         stream.Write(buffer, offset, count);
      }

      /// <summary>
      /// Indicates that no timeout should occur.
      /// </summary>
      public int InfiniteTimeout
      {
         get { return Timeout.Infinite; }
      }

      /// <summary>
      /// Reads a byte from the stream and advances the position within the stream by one byte, or returns -1 if at the end of the stream.
      /// </summary>
      /// <returns>
      /// The unsigned byte cast to an Int32, or -1 if at the end of the stream.
      /// </returns>
      /// <exception cref="T:System.NotSupportedException">The stream does not support reading. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override int ReadByte()
      {
         return stream.ReadByte();
      }

      /// <summary>
      /// Purges the receive buffer.
      /// </summary>
      public void DiscardInBuffer()
      {
         stream.Flush();
      }

      /// <summary>
      /// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
      /// </summary>
      /// <returns>
      /// The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
      /// </returns>
      /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source. </param><param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream. </param><param name="count">The maximum number of bytes to be read from the current stream. </param><exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length. </exception><exception cref="T:System.ArgumentNullException"><paramref name="buffer"/> is null. </exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="offset"/> or <paramref name="count"/> is negative. </exception><exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support reading. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override int Read(byte[] buffer, int offset, int count)
      {
         return stream.Read(buffer, offset, count);
      }

      /// <summary>
      /// When overridden in a derived class, sets the length of the current stream.
      /// </summary>
      /// <param name="value">The desired length of the current stream in bytes. </param><exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support both writing and seeking, such as if the stream is constructed from a pipe or console output. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override void SetLength(long value)
      {
         stream.SetLength(value);
      }

      /// <summary>
      /// When overridden in a derived class, sets the position within the current stream.
      /// </summary>
      /// <returns>
      /// The new position within the current stream.
      /// </returns>
      /// <param name="offset">A byte offset relative to the <paramref name="origin"/> parameter. </param><param name="origin">A value of type <see cref="T:System.IO.SeekOrigin"/> indicating the reference point used to obtain the new position. </param><exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support seeking, such as if the stream is constructed from a pipe or console output. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override long Seek(long offset, SeekOrigin origin)
      {
         return stream.Seek(offset, origin);
      }

      /// <summary>
      /// When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device.
      /// </summary>
      /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
      public override void Flush()
      {
         stream.Flush();
      }

      /// <summary>
      /// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream. Instead of calling this method, ensure that the stream is properly disposed.
      /// </summary>
      public override void Close()
      {
         stream.Close();
      }

      /// <summary>
      /// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to write before timing out. 
      /// </summary>
      /// <returns>
      /// A value, in miliseconds, that determines how long the stream will attempt to write before timing out.
      /// </returns>
      /// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Stream.WriteTimeout"/> method always throws an <see cref="T:System.InvalidOperationException"/>. </exception>
      public override int WriteTimeout
      {
         get { return stream.WriteTimeout; }
         set { stream.WriteTimeout = value; }
      }

      /// <summary>
      /// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to read before timing out. 
      /// </summary>
      /// <returns>
      /// A value, in miliseconds, that determines how long the stream will attempt to read before timing out.
      /// </returns>
      /// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Stream.ReadTimeout"/> method always throws an <see cref="T:System.InvalidOperationException"/>. </exception>
      public override int ReadTimeout
      {
         get { return stream.ReadTimeout; }
         set { stream.ReadTimeout = value; }
      }

      /// <summary>
      /// When overridden in a derived class, gets or sets the position within the current stream.
      /// </summary>
      /// <returns>
      /// The current position within the stream.
      /// </returns>
      /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.NotSupportedException">The stream does not support seeking. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override long Position
      {
         get { return stream.Position; }
         set { stream.Position = value; }
      }

      /// <summary>
      /// When overridden in a derived class, gets the length in bytes of the stream.
      /// </summary>
      /// <returns>
      /// A long value representing the length of the stream in bytes.
      /// </returns>
      /// <exception cref="T:System.NotSupportedException">A class derived from Stream does not support seeking. </exception><exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
      public override long Length
      {
         get { return stream.Length; }
      }

      /// <summary>
      /// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
      /// </summary>
      /// <returns>
      /// true if the stream supports writing; otherwise, false.
      /// </returns>
      public override bool CanWrite
      {
         get { return stream.CanWrite; }
      }

      /// <summary>
      /// Gets a value that determines whether the current stream can time out.
      /// </summary>
      /// <returns>
      /// A value that determines whether the current stream can time out.
      /// </returns>
      public override bool CanTimeout
      {
         get { return stream.CanTimeout; }
      }

      /// <summary>
      /// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
      /// </summary>
      /// <returns>
      /// true if the stream supports seeking; otherwise, false.
      /// </returns>
      public override bool CanSeek
      {
         get { return stream.CanSeek; }
      }

      /// <summary>
      /// When overridden in a derived class, gets a value indicating whether the current stream supports reading.
      /// </summary>
      /// <returns>
      /// true if the stream supports reading; otherwise, false.
      /// </returns>
      public override bool CanRead
      {
         get { return stream.CanRead; }
      }
   }
}