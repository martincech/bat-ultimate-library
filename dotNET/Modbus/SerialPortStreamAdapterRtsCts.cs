using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using Modbus.IO;
using SerialPort;
using Handshake = SerialPort.Handshake;
using Parity = SerialPort.Parity;

namespace Modbus
{
   public class SerialPortStreamAdapterRtsCts : Stream, IStreamResource, ISerialPort
   {
      private readonly System.IO.Ports.SerialPort port;
      public SerialPortStreamAdapterRtsCts(System.IO.Ports.SerialPort port)
      {
         this.port = port;
         port.NewLine = GetPlatformNewLine();
      }

      private static string GetPlatformNewLine()
      {
         string newLine;
         var os = Environment.OSVersion;
         switch (os.Platform)
         {
            case PlatformID.Win32S:
            case PlatformID.Win32Windows:
            case PlatformID.Win32NT:
            case PlatformID.WinCE:
               newLine = "\r\n";
               break;
            case PlatformID.Unix:
               newLine = "\n";
               break;
            default:
               throw new InvalidOperationException();
         }
         return newLine;
      }

      public string PortName
      {
         get { return port.PortName; }
      }

      public int BaudRate
      {
         get { return port.BaudRate; }
         set { port.BaudRate = value; }
      }

      public Parity Parity
      {
         get { return port.GetParity(); }
         set { port.SetParity(value); }
      }

      public int RxTimeout
      {
         get { return port.ReadTimeout; }
         set { port.ReadTimeout = value; }
      }

      public Handshake Handshake
      {
         get { return port.GetHandshake(); }
         set { port.SetHandshake(value); }
      }

      public int DataBits { get  {return port.DataBits; } set { port.DataBits = value; } }
      public bool RtsEnable { get { return port.RtsEnable; } set { port.RtsEnable = value; } }
      public StopBit StopBits { get { return port.StopBits.Map(); } set { port.StopBits = value.Map(); } }

      public Stream Open()
      {
         port.Open();
         return this;
      }

      public override void WriteByte(byte value)
      {
         port.BaseStream.WriteByte(value);
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         port.RtsEnable = true;
         port.BaseStream.Write(buffer, offset, count);
         Thread.Sleep(count);
         port.DiscardInBuffer();
         port.RtsEnable = false;
      }

      public int InfiniteTimeout
      {
         get { return Timeout.Infinite; }
      }

      public override int ReadByte()
      {
         return port.BaseStream.ReadByte();
      }

      public void DiscardInBuffer()
      {
         port.BaseStream.Flush();
      }
      public override int Read(byte[] buffer, int offset, int count)
      {
         return port.BaseStream.Read(buffer, offset, count);
      }

      public override void SetLength(long value)
      {
         port.BaseStream.SetLength(value);
      }

      public override long Seek(long offset, SeekOrigin origin)
      {
         return port.BaseStream.Seek(offset, origin);
      }

      public override void Flush()
      {
         port.BaseStream.Flush();
      }

      public override void Close()
      {
         port.BaseStream.Close();
      }

      public override int WriteTimeout
      {
         get { return port.BaseStream.WriteTimeout; }
         set { port.BaseStream.WriteTimeout = value; }
      }

      public override int ReadTimeout
      {
         get { return port.BaseStream.ReadTimeout; }
         set { port.BaseStream.ReadTimeout = value; }
      }

      public override long Position
      {
         get { return port.BaseStream.Position; }
         set { port.BaseStream.Position = value; }
      }

      public override long Length
      {
         get { return port.BaseStream.Length; }
      }

      public override bool CanWrite
      {
         get { return port.BaseStream.CanWrite; }
      }

      public override bool CanTimeout
      {
         get { return port.BaseStream.CanTimeout; }
      }
      public override bool CanSeek
      {
         get { return port.BaseStream.CanSeek; }
      }

      public override bool CanRead
      {
         get { return port.BaseStream.CanRead; }
      }
   }

   public static class SerialPortExtension
   {
      public static StopBit Map(this StopBits stopBits)
      {
         switch (stopBits)
         {
            case StopBits.One:
               return StopBit.One;
            case StopBits.OnePointFive:
               return StopBit.OneHalf;
            case StopBits.Two:
               return StopBit.Two;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      public static StopBits Map(this StopBit stopBits)
      {
         switch (stopBits)
         {
            case StopBit.One:
               return StopBits.One;
            case StopBit.OneHalf:
               return StopBits.OnePointFive;
            case StopBit.Two:
               return StopBits.Two;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }
   }
}