﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace Windows
{
   /// <summary>
   /// Summary description for SingleApp.
   /// </summary>
   public class SingleApplication
   {
      /// <summary>
      /// Imports 
      /// </summary>

      [DllImport("user32.dll")]
      private static extern int ShowWindow(IntPtr hWnd, int nCmdShow);

      [DllImport("user32.dll")]
      private static extern int SetForegroundWindow(IntPtr hWnd);

      [DllImport("user32.dll")]
      private static extern int IsIconic(IntPtr hWnd);

      /// <summary>
      /// GetCurrentInstanceWindowHandle
      /// </summary>
      /// <returns></returns>
      private static IntPtr GetCurrentInstanceWindowHandle()
      {
         var hWnd = IntPtr.Zero;
         var process = Process.GetCurrentProcess();
         var processes = Process.GetProcessesByName(process.ProcessName);
         foreach (var _process in processes)
         {
            // Get the first instance that is not this instance, has the
            // same process name and was started from the same file name
            // and location. Also check that the process has a valid
            // window handle in this session to filter out other user's
            // processes.
            if (_process.Id != process.Id &&
               _process.MainModule.FileName == process.MainModule.FileName &&
               _process.MainWindowHandle != IntPtr.Zero)
            {
               hWnd = _process.MainWindowHandle;
               break;
            }
         }
         return hWnd;
      }
      /// <summary>
      /// SwitchToCurrentInstance
      /// </summary>
      public static void SwitchToCurrentInstance()
      {
         var hWnd = GetCurrentInstanceWindowHandle();
         if (hWnd != IntPtr.Zero)
         {
            // Restore window if minimised. Do not restore if already in
            // normal or maximised window state, since we don't want to
            // change the current state of the window.
            if (IsIconic(hWnd) != 0)
            {
               ShowWindow(hWnd, SW_RESTORE);
            }

            // Set foreground window.
            SetForegroundWindow(hWnd);
         }
      }


      /// <summary>
      /// for console base application
      /// </summary>
      /// <returns></returns>
      public static bool Run()
      {
         if (IsAlreadyRunning())
         {
            return false;
         }
         return true;
      }

      /// <summary>
      /// check if given exe alread running or not
      /// </summary>
      /// <returns>returns true if already running</returns>
      public static bool IsAlreadyRunning()
      {
          var strLoc = Assembly.GetEntryAssembly().Location;
          FileSystemInfo fileInfo = new FileInfo(strLoc);
          var sExeName = fileInfo.Name;
          bool bCreatedNew;

          _mutex = new Mutex(true, "Global\\" + sExeName, out bCreatedNew);
          if (bCreatedNew)
              _mutex.ReleaseMutex();

          return !bCreatedNew;
      }

     

      static Mutex _mutex;
      const int SW_RESTORE = 9;
   }
}
