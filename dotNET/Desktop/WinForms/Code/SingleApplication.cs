﻿using System.Windows.Forms;

namespace Desktop.WinForms
{
   /// <summary>
   /// Summary description for SingleApp.
   /// </summary>
   public class SingleApplication : Windows.SingleApplication
   {
      /// <summary>
      /// Execute a form base application if another instance already running on
      /// the system activate previous one
      /// </summary>
      /// <param name="frmMain">main form</param>
      /// <returns>true if no previous instance is running</returns>
      public static bool Run(Form frmMain)
      {
         if (IsAlreadyRunning())
         {
            //set focus on previously running app
            SwitchToCurrentInstance();
            return false;
         }
         Application.Run(frmMain);
         return true;
      }
   }
}
