﻿using System;
using System.Threading;
using GUI;

namespace Desktop.WinForms
{
   public class ProgressBar : System.Windows.Forms.ProgressBar, IProgressBar
   {
      #region Implementation of IProgressBar

      public new double Value
      {
         get
         {
            if (!InvokeRequired) return base.Value;

            var s = 0;
            var waitEvent = new ManualResetEvent(false);
            Invoke(new Action(() =>
            {
               s = base.Value;
               waitEvent.Set();
            }));
            waitEvent.WaitOne();
            return s;
         }
         set
         {
            if (InvokeRequired)
            {
               Invoke(new Action<int>(v => base.Value = v), new object[] {(int)value});
            }
            else
            {
               base.Value = (int) value;
            }
         }
      }

      #endregion
   }
}
