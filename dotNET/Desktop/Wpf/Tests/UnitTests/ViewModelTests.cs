﻿using System.Collections.Generic;
using System.Linq;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class ViewModelTests
   {

      [TestMethod]
      public void ViewsAddedToViewModel()
      {
         var vm = new StubViewModel(new []{new StubView(),new StubView(),new StubView()});
         Assert.IsNotNull(vm.Views);
         Assert.IsTrue(vm.Views.Any());
         Assert.IsTrue(vm.Views.All(view => view.DataContext == vm));
         Assert.AreEqual(3, vm.Views.Count());
      }

      [TestMethod]
      public void NoViews()
      {
         var vm = new StubViewModel();
         Assert.IsNotNull(vm.Views);
         Assert.IsFalse(vm.Views.Any());
         Assert.IsNull(vm.View);
      }

      [TestMethod]
      public void ViewsAdded()
      {
         var vm = new StubViewModel();
         vm.View = new StubView();
         Assert.IsTrue(vm.Views.Any());
         Assert.IsTrue(vm.Views.All(view => view.DataContext == vm));
         Assert.IsTrue(vm.View.DataContext == vm);
         var sview = new StubView();
         vm.View = sview;
         Assert.IsTrue(vm.Views.Any());
         Assert.AreEqual(2, vm.Views.Count());
         Assert.IsTrue(vm.Views.All(view => view.DataContext == vm));
         Assert.IsTrue(vm.View.DataContext == vm);
         Assert.AreSame(vm.View, sview);
      }

   }

   internal class StubViewModel : ViewModel
   {
      public StubViewModel(IEnumerable<IView>  views)
         : base(views)
      {
      }
      public StubViewModel(IView view = null)
         : base(view)
      {
      }
   }

   internal class StubView : IView
   {
      public object DataContext { get; set; }


      public void Show()
      {
          throw new System.NotImplementedException();
      }

      public void Hide()
      {
          throw new System.NotImplementedException();
      }
   }
}