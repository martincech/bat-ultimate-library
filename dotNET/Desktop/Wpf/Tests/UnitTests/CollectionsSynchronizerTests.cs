﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using Desktop.Wpf.AttachedProperties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class CollectionsSynchronizerTests
   {
      private ObservableCollection<object> col1;
      private ObservableCollection<object> col2;
      private Mock<IValueConverter> conv;

      [TestInitialize]
      public void Init()
      {
         col1 = new ObservableCollection<object>();
         col2 = new ObservableCollection<object>();
         conv = new Mock<IValueConverter>();
         conv.Setup(
            converter =>
               converter.Convert(It.IsAny<object>(), It.IsAny<Type>(), It.IsAny<object>(), It.IsAny<CultureInfo>()))
            .Returns<object>(o => o);
         conv.Setup(
            converter =>
               converter.ConvertBack(It.IsAny<object>(), It.IsAny<Type>(), It.IsAny<object>(), It.IsAny<CultureInfo>()))
            .Returns<object>(o => o);
      }

      [TestMethod]
      public void Synchronizer_CanCreate()
      {
         var synchro = new CollectionsSynchronizer(list: col2, dependentList: col1, converter: conv.Object);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer(list: null, dependentList: col1, converter: conv.Object);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer(list: null, dependentList: null, converter: conv.Object);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer(list: col2, dependentList: null, converter: conv.Object);
         Assert.IsNotNull(synchro);
      }
   }
}
