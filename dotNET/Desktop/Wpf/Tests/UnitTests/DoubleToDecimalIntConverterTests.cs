﻿using System;
using System.Globalization;
using Desktop.Wpf.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class DoubleToDecimalIntConverterTests
   {
      private IntToDecimalDoubleConverter converter;

      [TestInitialize]
      public void TestInit()
      {
         converter = new IntToDecimalDoubleConverter();
      }

      [TestMethod]
      public void Converter_ConvertToDouble()
      {
         const int value = 525;
         var convertedValue = converter.Convert(value, typeof(double), 2, CultureInfo.InvariantCulture);
         Assert.IsTrue(Math.Abs((double)convertedValue - 5.25) <= 0.01, "Conversion failed");
      }

      [TestMethod]
      public void Converter_ConvertToNullableDouble()
      {
         const int value = 525;
         var convertedValue = converter.Convert(value, typeof(double?), 2, CultureInfo.InvariantCulture);
         Assert.IsTrue(Math.Abs((double)convertedValue - 5.25) <= 0.01, "Conversion failed");
      }

      [TestMethod]
      public void Converter_ConvertFromDouble()
      {
         const double value = 5.25;
         var convertedValue = (int)converter.ConvertBack(value, typeof(int), 2, CultureInfo.InvariantCulture);
         Assert.IsTrue(convertedValue == 525, "Conversion failed");
      }

      [TestMethod]
      public void Converter_ConvertFromNullableDouble()
      {
         double? value = 5.25;
         var convertedValue = converter.ConvertBack(value, typeof(int), 2, CultureInfo.InvariantCulture);
         Assert.IsTrue((int)convertedValue == 525, "Conversion failed");
      }

      [TestMethod]
      public void MultiConverter_ConvertToDouble()
      {
         var values = new object[] {525, 2};
         var convertedValue = converter.Convert(values, typeof(double), null, CultureInfo.InvariantCulture);
         Assert.IsTrue(Math.Abs((double)convertedValue - 5.25) <= 0.01, "Conversion failed");
      }

      [TestMethod]
      public void MultiConverter_ConvertToNullableDouble()
      {
         var values = new object[] { 525, 2 };
         var convertedValue = converter.Convert(values, typeof(double?), null, CultureInfo.InvariantCulture);
         Assert.IsTrue(Math.Abs((double)convertedValue - 5.25) <= 0.01, "Conversion failed");
      }

      [TestMethod]
      public void MultiConverter_ConvertFromDouble()
      {
         const double value = 5.25;
         var types = new[] {typeof (int), typeof (int)};
         var convertedValue = converter.ConvertBack(value, types, null, CultureInfo.InvariantCulture);
         Assert.IsTrue((int)convertedValue[0] == 525, "Conversion failed");
         Assert.IsTrue((int)convertedValue[1] == 2, "Conversion failed");
      }

      [TestMethod]
      public void MultiConverter_ConvertFromNullableDouble()
      {
         const double value = 5.25;
         var types = new[] { typeof(int), typeof(int) };
         var convertedValue = converter.ConvertBack(value, types, null, CultureInfo.InvariantCulture);
         Assert.IsTrue((int)convertedValue[0] == 525, "Conversion failed");
         Assert.IsTrue((int)convertedValue[1] == 2, "Conversion failed");
      }

   }
}
