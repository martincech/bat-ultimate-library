﻿using System.Collections.ObjectModel;
using System.Windows.Data;
using Desktop.Wpf.Applications;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class AttachedPropertyCollectionsSynchronizerTests
   {
      private ObservableCollection<object> col1;
      private ObservableCollection<object> col2;
      private ObservableCollection<object> col3;
      private Mock<IValueConverter> conv;

      [TestInitialize]
      public void Init()
      {
         col1 = new ObservableCollection<object>();
         col2 = new ObservableCollection<object>();
         col3 = new ObservableCollection<object>();
      }

      [TestMethod]
      public void Synchronizer_CanCreate()
      {
         var synchro = new CollectionsSynchronizer(col1, col2);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer(col1, null);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer(null, col2);
         Assert.IsNotNull(synchro);

         synchro = new CollectionsSynchronizer();
         Assert.IsNotNull(synchro);

      }

      [TestMethod]
      public void Synchronizer_SynchroWithNew()
      {
         var synchro = new CollectionsSynchronizer(col1);
         col1.Add(new object());
         col1.Add(new object());

         synchro.DependentList = col2;
         Assert.AreEqual(col1.Count, col2.Count);
         synchro.DependentList = null;
         col1.Add(new object());
         Assert.AreEqual(2, col2.Count);
         Assert.AreEqual(3, col1.Count);
         col2.Add(new object());
         col2.Add(new object());
         synchro.DependentList = col2;
         Assert.AreEqual(col1.Count, col2.Count);
         Assert.AreEqual(5, col1.Count);
         synchro.List = col3;
         Assert.AreEqual(col3.Count, col2.Count);
      }
   }
}
