﻿using System;
using System.Diagnostics;

namespace Desktop.Wpf
{
   public static class Logger
   {
      static Logger()
      {
         Write("=== " + DateTime.Now + " ==========================", false);
      }

      public static ILogger Instance { get; set; }

      /// <summary>
      /// Write string to log file.
      /// </summary>
      /// <param name="message">string to write</param>
      /// <param name="timeFlag">if true - add time to message</param>
      public static void Write(string message, bool timeFlag = true)
      {
         if (timeFlag)
         {
            WriteWithTime(message);
         }
         else
         {
            WriteWithoutTime(message);
         }
      }

      private static void WriteWithoutTime(string message)
      {
         if (Instance != null)
         {
            Instance.Log(message);
         }
         else
         {
            Trace.WriteLine(message);
         }
      }

      private static void WriteWithTime(string message)
      {
         if (Instance != null)
         {
               Instance.Log(DateTime.Now, message);
         }
         else
         {
            var time = DateTime.Now.ToString("hh:mm:ss:fff tt") + " - ";
            Trace.WriteLine(time + message);
         }
      }
   }
}
