using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Resources;
using System.Windows.Data;

namespace Desktop.Wpf.Localization
{
   /// <summary>
   /// Defines a type converter for string values to support nested strings
   /// </summary>
   /// <remarks>
   /// <code lang="C#" escaped="true" >
   /// class LocalizedConverter : NestedResourceConverter
   /// {
   ///    public LocalizedConverter(Type type)
   ///        : base(Path.To.ResourceManager)
   ///    {
   ///    }
   /// }    
   /// </code>
   /// </remarks>
   public class NestedResourceConverter : IValueConverter
    {
       #region Private variables
       private class LookupTable : Dictionary<string, object> { }
       private Dictionary<CultureInfo, LookupTable> _lookupTables = new Dictionary<CultureInfo, LookupTable>();
       private ResourceManager _resourceManager;
       #endregion

       #region Private helpers

       private LookupTable GetLookupTable(CultureInfo culture)
       {
          LookupTable result = null;
          if (culture == null)
             culture = CultureInfo.CurrentCulture;

          if (!_lookupTables.TryGetValue(culture, out result))
          {
             result = new LookupTable();
             var tcv = new TypeConverter();
             foreach (var value in tcv.GetStandardValues())
             {
                var text = GetValueText(culture, value);
                if (text != null)
                {
                   result.Add(text, value);
                }
             }
             _lookupTables.Add(culture, result);
          }
          return result;
       }
       private string GetValueText(CultureInfo culture, object value)
       {
          var resourceName = GetResourceName(value);
          var result = _resourceManager.GetString(resourceName, culture);
          if (result == null)
             result = _resourceManager.GetString(resourceName);
          if (result == null)
             result = resourceName;
          return result;
       }
       private object GetValue(CultureInfo culture, string text)
       {
          var lookupTable = GetLookupTable(culture);
          object result = null;
          lookupTable.TryGetValue(text, out result);
          return result;
       }

       private string GetResourceName(object value)
       {
          return value.ToString();
       }
       #endregion

       #region Public interface

      /// <summary>
       /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      /// <param name="resourceManager"></param>
       public NestedResourceConverter(ResourceManager resourceManager)
       {
          _resourceManager = resourceManager;
       }

       /// <summary>
       /// Handle XAML Conversion from this type to other types
       /// </summary>
       /// <param name="value">The value to convert</param>
       /// <param name="targetType">The target type</param>
       /// <param name="parameter">not used</param>
       /// <param name="culture">The culture to convert</param>
       /// <returns>The converted value</returns>
       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
          if (culture == null)
             culture = CultureInfo.CurrentCulture;

          if (value == null) return null;
          if (targetType == typeof(string) || targetType == typeof(object))
          {
             return GetValueText(culture, value);
          }
          else
          {
             return null;
          }
       }

       /// <summary>
       /// Handle XAML Conversion from other types back to this type
       /// </summary>
       /// <param name="value">The value to convert</param>
       /// <param name="targetType">The target type</param>
       /// <param name="parameter">not used</param>
       /// <param name="culture">The culture to convert</param>
       /// <returns>The converted value</returns>
       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
          if (culture == null)
             culture = CultureInfo.CurrentCulture;
          if (value is string)
          {
             return GetValue(culture, (string)value);
          }
          else
          {
             return null;
          }
       }
       #endregion
    }
}
