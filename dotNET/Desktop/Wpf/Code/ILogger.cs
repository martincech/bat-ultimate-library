﻿using System;

namespace Desktop.Wpf
{
   public interface ILogger
   {
      void Log(string message);
      void Log(DateTime time, string message);
   }
}