﻿namespace Desktop.Wpf.LocalizationExtension
{
   public enum Language
   {
      English,
      Czech,
      French,
      German,
      Austria
   }
}
