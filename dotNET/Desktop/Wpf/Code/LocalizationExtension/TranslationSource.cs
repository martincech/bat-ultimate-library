﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Resources;

namespace Desktop.Wpf.LocalizationExtension
{
   /// <summary>
   /// Based on :
   /// https://codinginfinity.me/post/2015-05-10/localization_of_a_wpf_app_the_simple_approach#
   /// https://gist.github.com/jakubfijalkowski/0771bfbd26ce68456d3e
   /// </summary>
   public class TranslationSource : INotifyPropertyChanged
   {
      #region Static

      private static readonly TranslationSource INSTANCE = new TranslationSource();

      public static TranslationSource Instance
      {
         get { return INSTANCE; }
      }

      public static ResourceManager ResManager;

      #endregion

      #region Private fields

      private CultureInfo currentCulture;
      private Language language;
      private readonly Dictionary<Language, string> languageToCultureMapper;
      private const Language DEFAULT_LANGUAGE = Language.English;
      private readonly CultureInfo defaultCulture;

      #endregion

      #region Public interfaces

      #region Constructor

      public TranslationSource()
      {
         languageToCultureMapper = new Dictionary<Language, string>
         {
            //source: https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx
            {Language.English, "en-US"},
            {Language.Czech, "cs-CZ"},
            {Language.French, "fr-FR"},
            {Language.German, "de-DE"},
            {Language.Austria, "de-AT"},
         };
         Language = DEFAULT_LANGUAGE;
         defaultCulture = GetCulture(DEFAULT_LANGUAGE);
      }

      #endregion

      public string this[string key]
      {
         get
         {
            if (ResManager == null || currentCulture == null)
            {
               return DefaultFormat(key);
            }

            var localizedString = ResManager.GetString(key, currentCulture);
            if (localizedString == null || localizedString.Equals(string.Empty))
            {  // string is missing in current culture -> search in default culture
               localizedString = ResManager.GetString(key, defaultCulture);
            }
            if (localizedString == null || localizedString.Equals(string.Empty))
            {  // string not exists in resources
               localizedString = DefaultFormat(key);
            }
            return localizedString;
         }
      }

      public CultureInfo CurrentCulture
      {
         get { return currentCulture; }
         set
         {
            currentCulture = value;
            Invoke();
         }
      }

      public event PropertyChangedEventHandler PropertyChanged;

      public Language Language
      {
         get { return language; }
         set
         {
            language = value;
            CurrentCulture = GetCulture(language);
         }
      }

      #endregion

      #region Private helpers

      private CultureInfo GetCulture(Language lang)
      {
         return CultureInfo.GetCultureInfo(languageToCultureMapper[lang]);
      }

      private void Invoke()
      {
         var @event = PropertyChanged;
         if (@event != null)
         {
            @event.Invoke(this, new PropertyChangedEventArgs(string.Empty));
         }
      }

      private string DefaultFormat(string str)
      {
         return "#" + str;
      }

      #endregion
   }
}
