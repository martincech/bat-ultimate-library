﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Desktop.Wpf.LocalizationExtension
{
   public class EnumToLocalizableStringConvertor : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var collection = value as IEnumerable;
         if (collection != null)
         {
            return (from object item in collection select LocalizeElement((Enum)item)).ToList();
         }
         var val = value as Enum;
         return val == null ? value : LocalizeElement(val);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      private string LocalizeElement(Enum element)
      {
         return element.GetLocalizableName();
      }
   }
}
