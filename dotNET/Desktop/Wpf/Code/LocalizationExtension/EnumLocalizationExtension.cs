﻿using System;

namespace Desktop.Wpf.LocalizationExtension
{
   public static class EnumLocalizationExtension
   {
      public static string GetLocalizableName(this Enum e)
      {
         return TranslationSource.Instance[e.ToString()];
      }
   }
}
