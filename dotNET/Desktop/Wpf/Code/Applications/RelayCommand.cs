﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Desktop.Wpf.Applications
{
   /// <summary>
   /// Base class for relay commands, adds <see cref="RaiseCanExecuteChanged"/>
   /// method to <see cref="ICommand"/> interface to be able
   /// raise <see cref="CanExecuteChanged"/> event easy.
   /// </summary>
   public abstract class Command : ICommand
   {
      public abstract void RaiseCanExecuteChanged();
      public abstract bool CanExecute(object parameter);
      public abstract void Execute(object parameter);
      public abstract event EventHandler CanExecuteChanged;
   }
   /// <summary>
   /// Provides an <see cref="ICommand"/> implementation which relays the <see cref="Execute"/> and <see cref="CanExecute"/> 
   /// method to the specified delegates.
   /// </summary>
   public class RelayCommand<T> : Command
   {
      private readonly Action<T> execute;
      private readonly Func<T, bool> canExecute;


      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action execute) : this(execute, null) { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action<T> execute) : this(execute, null) { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <param name="canExecute">Delegate to execute when CanExecute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action execute, Func<bool> canExecute)
         : this(execute != null ? p => execute() : (Action<T>)null, canExecute != null ? p => canExecute() : (Func<T, bool>)null)
      { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <param name="canExecute">Delegate to execute when CanExecute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action<T> execute, Func<T, bool> canExecute)
      {
         if (execute == null) { throw new ArgumentNullException("execute"); }

         this.execute = execute;
         this.canExecute = canExecute;
      }


      /// <summary>
      /// Occurs when changes occur that affect whether or not the command should execute.
      /// </summary>
      public override event EventHandler CanExecuteChanged;


      /// <summary>
      /// Defines the method that determines whether the command can execute in its current state.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this T can be set to null.</param>
      /// <returns>
      /// true if this command can be executed; otherwise, false.
      /// </returns>
      /// <exception cref="ArgumentException">The parameter is not of generic type.</exception>
      public override bool CanExecute(object parameter)
      {
         if (parameter != null && (!(parameter is T) && !(parameter is IEnumerable)))
         {
            throw new ArgumentException("This is not valid command parameter.");
         }
         return canExecute == null || canExecute(parameter != null ? (T) parameter : default(T));
      }

      /// <summary>
      /// Defines the method to be called when the command is invoked.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
      /// <exception cref="InvalidOperationException">The <see cref="CanExecute"/> method returns <c>false.</c></exception>
      public override void Execute(object parameter)
      {
         if (!CanExecute(parameter))
         {
            throw new InvalidOperationException("The command cannot be executed because the canExecute action returned false.");
         }

         execute(parameter != null ? (T)parameter : default(T));
      }

      /// <summary>
      /// Raises the <see cref="E:CanExecuteChanged"/> event.
      /// </summary>
      public override void RaiseCanExecuteChanged()
      {
         OnCanExecuteChanged(EventArgs.Empty);
      }

      /// <summary>
      /// Raises the <see cref="E:CanExecuteChanged"/> event.
      /// </summary>
      /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
      protected virtual void OnCanExecuteChanged(EventArgs e)
      {
         EventHandler handler = CanExecuteChanged;
         if (handler != null)
         {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action) (() =>
            {
               handler(this, e);
            }));

         }
      }
   }

   public class RelayCommand : RelayCommand<object>
   {
            #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action execute) : this(execute, null) { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action<object> execute) : this(execute, null) { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <param name="canExecute">Delegate to execute when CanExecute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action execute, Func<bool> canExecute)
         : this(execute != null ? p => execute() : (Action<object>)null, canExecute != null ? p => canExecute() : (Func<object, bool>)null)
      { }

      /// <summary>
      /// Initializes a new instance of the <see cref="RelayCommand"/> class.
      /// </summary>
      /// <param name="execute">Delegate to execute when Execute is called on the command.</param>
      /// <param name="canExecute">Delegate to execute when CanExecute is called on the command.</param>
      /// <exception cref="ArgumentNullException">The execute argument must not be null.</exception>
      public RelayCommand(Action<object> execute, Func<object, bool> canExecute)
         : base(execute, canExecute)
      {}

      #region Overrides of RelayCommand<object>

      /// <summary>
      /// Overload method for <see cref="ICommand.CanExecute"/> with null parameter
      /// </summary>
      /// <returns>same as <see cref="ICommand.CanExecute"/></returns>
      public bool CanExecute()
      {
         return CanExecute(null);
      }

      /// <summary>
      /// Overload method for <see cref="ICommand.Execute"/> with null parameter
      /// </summary>
      /// <returns>same as <see cref="ICommand.Execute"/></returns>
      public void Execute()
      {
         Execute(null);
      }

      #endregion

      #endregion
   }
}
