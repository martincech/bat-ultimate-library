using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Desktop.Wpf.AttachedProperties
{
   /// <summary>
   /// Add converter posibility to general collections synchronizer.
   /// </summary>
   public class CollectionsSynchronizer : Applications.CollectionsSynchronizer
   {
      private IValueConverter converter;

      /// <summary>
      /// Constructor to easily set properties.
      /// </summary>
      /// <param name="list">Main list (master), both will have this list values at begining</param>
      /// <param name="dependentList">Dependent list (slave)</param>
      /// <param name="converter">Converter to be used to assing values from one list to the other</param>
      public CollectionsSynchronizer(
         INotifyCollectionChanged list = null,
         INotifyCollectionChanged dependentList = null,
         IValueConverter converter = null)
      {
         Converter = converter;
         List = list;
         DependentList = dependentList;
      }

      /// <summary>
      /// Converter to use
      /// </summary>
      public IValueConverter Converter
      {
         get { return converter; }
         set
         {
            if (converter == value)
            {
               return;
            }
            StopSynchronize();
            converter = value;
            StartSynchronize();
         }
      }

      /// <summary>
      /// Use converter to convert value, if converter exists
      /// </summary>
      /// <param name="l">listToListen to which value will be converted in</param>
      /// <param name="value">value to be converted</param>
      /// <returns>converted value or original value when conversion not posible</returns>
      protected override object ConvertValue(IEnumerable l, object value)
      {
         if (converter == null) return base.ConvertValue(l,value);
         object v;
         if (l == DependentList)
         {
            v = converter.ConvertBack(value, DependentList.GetType().GetGenericArguments().First(), null,
               CultureInfo.CurrentCulture);
         }
         else
         {
            var t = List.GetType();
            while (t != null && !t.IsGenericType)
            {
               t = t.BaseType;
            }
            if (t == null)
            {
               v = converter.Convert(value, typeof(string), null, CultureInfo.CurrentCulture);
            }
            else
            {
               var type = t.GetGenericArguments().First();
               if (type == typeof (object))
               {
                  type = typeof (string);
               }
               v = converter.Convert(value, type, null,
                  CultureInfo.CurrentCulture);
            }
         }
         return v ?? (value);
      }
   }
}