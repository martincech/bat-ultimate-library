﻿namespace Desktop.Wpf.Presentation
{
   /// <summary>
   /// Represents a view
   /// 
   /// </summary>
   public interface IView
   {
      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      object DataContext { get; set; }

      /// <summary>
      /// Show this view
      /// </summary>
      void Show();

      /// <summary>
      /// Hide this view
      /// </summary>
      void Hide();
   }
}
