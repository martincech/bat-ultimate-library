﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
    public class NavigationToNextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[1] == null) 
            {
                return null;
            }
           IEnumerable<dynamic> list = (values[0] as IEnumerable<dynamic>);
           int index = list.ToList().IndexOf(values.LastOrDefault());
           object item = null;
           if (index + 1 < list.ToList().Count) 
           {
               item = list.ToList()[index+1];
           }
           return item;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NavigationToPreviousConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
            {
                return null;
            }
            IEnumerable<dynamic> list = (values[0] as IEnumerable<dynamic>);
            int index = list.ToList().IndexOf(values.LastOrDefault());
            object item = null;
            if (index > 0)
            {
                item = list.ElementAt(index - 1);
            }
            return item;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NavigationToNextVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
            {
                return null;
            }
            IEnumerable<dynamic> list = (values[0] as IEnumerable<dynamic>);
            int index = list.ToList().IndexOf(values.LastOrDefault());
            Visibility item = Visibility.Collapsed;
            if (index + 1 < list.ToList().Count)
            {
                item = Visibility.Visible;
            }
            return item;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NavigationToPreviousVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
            {
                return null;
            }
            IEnumerable<dynamic> list = (values[0] as IEnumerable<dynamic>);
            int index = list.ToList().IndexOf(values.LastOrDefault());
            Visibility item = Visibility.Collapsed;
            if (index > 0)
            {
                item = Visibility.Visible;
            }
            return item;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }



}
