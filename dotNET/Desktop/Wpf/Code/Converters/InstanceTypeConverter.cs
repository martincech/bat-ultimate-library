using System;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   public sealed class InstanceTypeConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (value == null)
         {
            return null;
         }
         return value.GetType();
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }

}