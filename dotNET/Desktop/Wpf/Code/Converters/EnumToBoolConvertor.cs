﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   /// <summary>
   /// Check if value is the same value as a parameter. Usage for radiobuttons for example.
   /// </summary>
   public class EnumToBoolConvertor : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return value != null && value.Equals(parameter);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return value != null && value.Equals(true) ? parameter : Binding.DoNothing;
      }
   }
}
