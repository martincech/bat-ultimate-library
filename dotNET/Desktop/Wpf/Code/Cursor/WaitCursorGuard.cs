﻿using System.Windows.Input;

namespace Desktop.Wpf.Cursor
{
   public sealed class WaitCursorGuard : CursorGuard
   {
      public WaitCursorGuard()
         : base(CursorType.Wait)
      {
      }
   }
}
