﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Desktop.Wpf.Cursor
{
   public class CursorGuard : IDisposable
   {
      public CursorGuard(CursorType cursorType)
      {
         Application.Current.Dispatcher.Invoke(() =>
         {
            Mouse.OverrideCursor = cursorType.Map();
         });
      }

      public void Dispose()
      {
         ResetCursor();
         GC.SuppressFinalize(this);
      }

#pragma warning disable CC0091 // Use static method
      public void ResetCursor()
#pragma warning restore CC0091 // Use static method
      {
         Application.Current.Dispatcher.Invoke(() =>
         {
            Mouse.OverrideCursor = null;
         });
      }
   }
}
