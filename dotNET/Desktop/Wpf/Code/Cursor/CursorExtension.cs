﻿using System.Windows.Input;

namespace Desktop.Wpf.Cursor
{
   public static class CursorExtension
   {
      //https://referencesource.microsoft.com/#PresentationCore/Core/CSharp/System/Windows/Input/CursorType.cs
      public static System.Windows.Input.Cursor Map(this CursorType type)
      {
         switch(type)
         {
            case CursorType.None:
               return Cursors.None;
            case CursorType.No:
               return Cursors.No;
            case CursorType.Arrow:
               return Cursors.Arrow;
            case CursorType.AppStarting:
               return Cursors.AppStarting;
            case CursorType.Cross:
               return Cursors.Cross;
            case CursorType.Help:
               return Cursors.Help;
            case CursorType.IBeam:
               return Cursors.IBeam;
            case CursorType.SizeAll:
               return Cursors.SizeAll;
            case CursorType.SizeNESW:
               return Cursors.SizeNESW;
            case CursorType.SizeNS:
               return Cursors.SizeNS;
            case CursorType.SizeNWSE:
               return Cursors.SizeNWSE;
            case CursorType.SizeWE:
               return Cursors.SizeWE;
            case CursorType.UpArrow:
               return Cursors.UpArrow;
            case CursorType.Wait:
               return Cursors.Wait;
            case CursorType.Hand:
               return Cursors.Hand;
            case CursorType.Pen:
               return Cursors.Pen;
            case CursorType.ScrollNS:
               return Cursors.ScrollNS;
            case CursorType.ScrollWE:
               return Cursors.ScrollWE;
            case CursorType.ScrollAll:
               return Cursors.ScrollAll;
            case CursorType.ScrollN:
               return Cursors.ScrollN;
            case CursorType.ScrollS:
               return Cursors.ScrollS;
            case CursorType.ScrollW:
               return Cursors.ScrollW;
            case CursorType.ScrollE:
               return Cursors.ScrollE;
            case CursorType.ScrollNW:
               return Cursors.ScrollNW;
            case CursorType.ScrollNE:
               return Cursors.ScrollNE;
            case CursorType.ScrollSW:
               return Cursors.ScrollSW;
            case CursorType.ScrollSE:
               return Cursors.ScrollSE;
            case CursorType.ArrowCD:
               return Cursors.ArrowCD;
            default:
               return null;
         }
      }
   }
}
